import Vue from 'vue'
import './plugins/vuetify'
import App from './App.vue'
import router from './router'
import store from './store'
import Vuetify from 'vuetify'
import 'vuetify/dist/vuetify.min.css'
import axios from 'axios'
import VueAnalytics from 'vue-analytics'
Vue.prototype.$http = axios

import VueLazyload from 'vue-lazyload'
import SocialSharing from "vue-social-sharing";
 
Vue.use(SocialSharing);
// or with options
Vue.use(VueLazyload, {
    preLoad: 1.3,
    error: '/logo.png',
    loading: '/logo.png',
    attempt: 1
})
Vue.use(VueAnalytics, {
    id: 'UA-159545108-2',
    router
  });
  
import Autocomplete from '@trevoreyre/autocomplete-vue'
import '@trevoreyre/autocomplete-vue/dist/style.css'

import vuesax from 'vuesax'
import 'vuesax/dist/vuesax.css'

import BootstrapVue from 'bootstrap-vue'

import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'

import 'material-icons/iconfont/material-icons.css';

import i18n from './i18n'

import 'vuetify/src/stylus/app.styl'

Vue.use(Vuetify)
Vue.use(vuesax)
Vue.use(BootstrapVue)
Vue.use(Autocomplete)

Vue.config.productionTip = false
window.moment = require('moment')

// use beforeEach route guard to set the language
router.beforeEach((to, from, next) => {

    // use the language from the routing param or default language
    let language = to.params.lang;
    if (!language) {
        language = 'en'
    }

    // set the current language for i18n.
    i18n.locale = language
    next()
})

new Vue({
    router,
    store,
    i18n,
    Vuetify,
    render: h => h(App)
}).$mount('#app')