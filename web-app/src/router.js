import Vue from 'vue'
import Router from 'vue-router'
import i18n from './i18n'

Vue.use(Router)

export default new Router({
    // mode: 'history',
    routes: [{
        path: '/',
        redirect: `/${i18n.locale}`
    },
    {
        path: '/:lang',
        component: {
            render(c) { return c('router-view') }
        },
        children: [{
            path: '/',
            name: 'home',
            component: () =>
                import('./views/Home.vue'),
            children: [{
                path: 'about',
                name: 'about',
                component: () =>
                    import('./views/About.vue')

            }]
        },
        {
            path: 'news-detail/:id',
            name: 'news-detail',
            component: () =>
                import('./views/NewsDetail.vue')
        },
        {
            path: 'archive',
            name: 'archive',
            component: () =>
                import('./components/Archive.vue')
        },
        {
            path: 'opinion',
            name: 'opinion',
            component: () =>
                import('./components/Opinion.vue')
        },
        {
            path: 'opinion-ur',
            name: 'opinion-ur',
            component: () =>
                import('./components/Opinion-ur.vue')
        }
            ,
        {
            path: 'opinion-pu',
            name: 'opinion-pu',
            component: () =>
                import('./components/Opinion-pu.vue')
        },
        {
            path: 'opinion-ar',
            name: 'opinion-ar',
            component: () =>
                import('./components/OpinionAR.vue')
        }
            ,
        {
            path: 'opinion-detail/:id',
            name: 'opinion-detail',
            component: () =>
                import('./views/OpinionDetail.vue')
        },
        {
            path: 'opinion-detail-ur/:id',
            name: 'opinion-detail-ur',
            component: () =>
                import('./views/OpinionDetailUR.vue')
        },
        {
            path: 'opinion-detail-pu/:id',
            name: 'opinion-detail-pu',
            component: () =>
                import('./views/OpinionDetailPU.vue')
        },
        {
            path: 'opinion-detail-ar/:id',
            name: 'opinion-detail-ar',
            component: () =>
                import('./views/OpinionDetailAR.vue')
        },
        {
            path: 'news-detail-ur/:id',
            name: 'news-detail-ur',
            component: () =>
                import('./views/NewsDetailUR.vue')
        },
        {
            path: 'news-detail-ar/:id',
            name: 'news-detail-ar',
            component: () =>
                import('./views/NewsDetailAR.vue')
        },

        {
            path: 'news-detail-pu/:id',
            name: 'news-detail-pu',
            component: () =>
                import('./views/NewsDetailPU.vue')
        },

        {
            path: 'header',
            name: 'header',
            component: () =>
                import('./components/header.vue')
        },
        {
            path: 'customization',
            name: 'customization',
            component: () =>
                import('./components/Customization.vue')
        }
        ]
    }
    ],
})