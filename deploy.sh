cd web-app;
echo "Building Web App...";
npm run build;
echo "Building Web App Completed...";
cd dist;
echo "Copying Dist...";
cp -rf * ../../app/public/;
echo "Copying Dist Completed...";