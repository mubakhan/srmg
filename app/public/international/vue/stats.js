var app = new Vue({
  el: "#app",
  data: {
    initialLoading: true,
    isLoading: true,
    isLoadingMore: false,
    isSourceLoading: false,
    allowScroll: false,
    base_url: "/international/",
    lang: "english",
    search: "",
    searchSort: "phrase",
    country: {
      data: [],
      selected: [],
      search: "",
      all: false,
    },
    countryAll: true,
    source: {
      data: [],
      selected: [],
      search: "",
      all: false,
    },
    sourceAll: true,
    startDate: "",
    endDate: "",
    customDate: "",
    dateAll: true,
    data: [],
    newsCount: "",
    lastId: "",
    dateFilter: "",
    tab: 1,
    sort: "desc",
    trans: {},
    archiveIdArr: [],
    opinionIdArr: [],
    pieChartData: [],
    calendarChartData: [],
    countryChartData: [],
    filterType: "news",
    totalNews: 0,
    totalSources: 0,
    totalCountries: 0,
    searchTerm: ""
  },
  mounted() {
    this.getSearchQuery();
    this.fetch();
    this.getChatData();
    if (localStorage.getItem("lang")) {
      this.lang = localStorage.getItem("lang");
    } else {
      this.lang = "english";
      localStorage.setItem("lang", this.lang);
    }
  },
  computed: {
    filteredCountry() {
      if (this.country.search)
        return this.country.data
          .filter((val) =>
            val.name.toLowerCase().includes(this.country.search.toLowerCase())
          )
          .sort((a, b) => a.name.localeCompare(b.name));
      if (this.country.all)
        return this.country.data.sort((a, b) => a.name.localeCompare(b.name));
      return this.country.data
        .sort((a, b) => a.name.localeCompare(b.name))
        .slice(0, 6);
    },
    filteredSource() {
      if (this.source.search)
        return this.source.data
          .filter((val) =>
            val.source.toLowerCase().includes(this.source.search.toLowerCase())
          )
          .sort((a, b) =>
            a.source.replace("The ").localeCompare(b.source.replace("The "))
          );
      if (this.source.all)
        return this.source.data.sort((a, b) =>
          a.source
            .replace("The ", "")
            .localeCompare(b.source.replace("The ", ""))
        );
      return this.source.data
        .sort((a, b) =>
          a.source
            .replace("The ", "")
            .localeCompare(b.source.replace("The ", ""))
        )
        .slice(0, 6);
    },
    hasCountry() {
      return this.country.selected.length > 0;
    },
    hasSource() {
      return this.source.selected.length > 0;
    },
  },
  watch: {
    lang(val) {
      val === "english"
        ? (document.getElementById("html-lang").dir = "ltr")
        : (document.getElementById("html-lang").dir = "rtl");
      this.resetFilter();
      this.initialLoading = true;
      this.getLanguage();
      this.getCountries();
    },
    sort() {
      this.newsCount = "";
      this.getLatest();
    },
    tab() {
      this.resetFilter();
    },
    "country.selected"(val) {
      this.countryAll = !val.length ? true : false;
      this.source = {
        data: [],
        selected: [],
        search: "",
        all: false,
      };

      this.newsCount = "";
      this.getSources();
    },
    "source.selected"(val) {
      this.sourceAll = !val.length ? true : false;
      this.newsCount = "";
      this.getChatData();
    },
    dateFilter(val) {
      this.dateAll = !val.length ? true : false;
      if (val) {
        this.customDate = "";
      }
      const todayDate = moment(new Date())
        .subtract(5, "hours")
        .format("YYYY:MM:DD HH:mm:ss");
      if (val == "24h") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(1, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getChatData();
      } else if (val == "48h") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(2, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getChatData();
      } else if (val == "7d") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(7, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getChatData();
      } else if (val == "1m") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(30, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getChatData();
      } else if (val == "1y") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(365, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getChatData();
      } else if (val == "") {
        this.endDate = "";
        this.startDate = "";
        this.getChatData();
      }
    },
    customDate(val) {
      if (val.includes("to")) {
        this.dateFilter = "";
        this.startDate = `${val.split(" to")[0]} ${new Date()
          .toTimeString()
          .substring(0, 8)}`;
        this.endDate = `${val.split("to ")[1]} ${new Date()
          .toTimeString()
          .substring(0, 8)}`;
        this.getLatest();
      }
    },
  },
  filters: {
    formatDate(val) {
      return `${moment(val).fromNow()} - ${moment(val).format(
        "DD:MM:YYYY, hh:mm Z"
      )}`;
    },
  },
  methods: {
    setLanguage() {
      localStorage.setItem("lang", this.lang);
    },
    monthNames(month) {
      return [
        "Jan",
        "Feb",
        "Mar",
        "Apr",
        "May",
        "Jun",
        "Jul",
        "Aug",
        "Sep",
        "Oct",
        "Nov",
        "Dec",
      ][month - 1];
    },
    getWeekName(week) {
      return `Week ${week}`;
    },
    getDateName(day) {
      return `Date ${day}`;
    },
    initCharts() {
      const arr = this.pieChartData.map((data) => {
        return [`${data.name}`, data.value];
      });
      let bgColor = [];
      let country = [];
      let count = [];

      this.countryChartData.map((data) => {
        country.push(data.country);
        bgColor.push("#" + (((1 << 24) * Math.random()) | 0).toString(16));
        count.push(data.count);
      });
      if (window.HrChart != undefined) {
        window.HrChart.destroy();
      }

      window.HrChart = new Chart(
        document.getElementById("bar-chart-horizontal"),
        {
          type: "horizontalBar",
          data: {
            labels: country,
            datasets: [
              {
                label: "News",
                backgroundColor: bgColor,
                data: count,
              },
            ],
          },
          options: {
            legend: { display: false },
            title: {
              display: true,
              text: "Total news country basied",
            },
          },
        }
      );
      c3.generate({
        bindto: "#campaign-v2",
        data: {
          columns: arr,
          type: "donut",
          tooltip: {
            show: true,
          },
        },
        donut: {
          label: {
            show: false,
          },
          title: "Sales",
          width: 18,
        },
        legend: {
          hide: true,
        },
        color: {
          pattern: ["#edf2f6", "#5f76e8", "#ff4f70", "#01caf1", "#e83e8c"],
        },
      });

      d3.select("#campaign-v2 .c3-chart-arcs-title").style(
        "font-family",
        "Rubik"
      );
      let labels = [];
      let series = [];

      this.calendarChartData.map((data) => {
        let value;
        if (data.week) {
          value = this.getWeekName(data.week);
        } else if (data.day) {
          value = this.getDateName(data.day);
        } else {
          value = this.monthNames(data.month);
        }
        labels.push(value);
        series.push(data.count);
      });
      // ==============================================================
      // income
      // ==============================================================
      var data = {
        labels,
        series: [series],
      };

      var options = {
        axisX: {
          showGrid: false,
        },
        seriesBarDistance: 1,
        chartPadding: {
          top: 15,
          right: 15,
          bottom: 5,
          left: 0,
        },
        plugins: [window.Chartist.plugins.tooltip()],
        width: "100%",
      };

      var responsiveOptions = [
        [
          "screen and (max-width: 640px)",
          {
            seriesBarDistance: 5,
            axisX: {
              labelInterpolationFnc: function (value) {
                return value[0];
              },
            },
          },
        ],
      ];
      let chart;
      if (chart) {
        chart.destroy();
      }
      chart = new Chartist.Bar(".net-income", data, options, responsiveOptions);
    },
    async fetch() {
      await this.getCountries();
      await this.getSources();
      this.getLanguage();
    },
    countryCheck(val) {
      if (val.target.value === "on") {
        this.country.selected = [];
      }
    },
    sourceCheck(val) {
      if (val.target.value === "on") {
        this.source.selected = [];
      }
    },
    datesCheck(val) {
      if (val.target.value === "true") {
        this.dateFilter = "";
        this.startDate = "";
        this.endDate = "";
        this.getLatest();
      }
    },
    async getLanguage() {
      this.trans = await (await fetch(`/locale/${this.lang}.json`)).json();
    },
    sourceType(type) {
      if (type === "opinion") return "get-opinion-sources";
      if (type === "news") return "get-sources";
    },
    latestType(type) {
      if (type === 1) return "get-opinions";
      if (type === 2) return "get-opinion-archive";
    },
    latestMoreType(type) {
      if (type === 1) return "get-opinions-more";
    },
    getSearchQuery() {
      this.searchTerm = window.location.search;
      const params = new URL(window.location.href).searchParams;
      this.search = params.get("q");
    },
    searchInput() {
        window.location.href = `${window.location.pathname}?q=${this.search}`;
    },
    async getChatData() {
      const response = await axios.post(`${this.base_url}getChatData`, {
        startDate: this.startDate,
        endDate: this.endDate,
        sources: this.source.selected,
        filterType: this.filterType,
        search: this.search,
        lang: this.lang,
        dateFilter: this.dateFilter,
        countries: !this.source.selected.length
          ? this.country.data
              .filter((val) => this.country.selected.includes(val.id))
              .map((val) => val.name)
          : [],
      });
      await this.getSources();
      this.pieChartData = response.data.pieCount;
      this.calendarChartData = response.data.yearCount;
      this.countryChartData = response.data.countryCount;
      this.totalNews = response.data.totalNews;
      this.totalSources = response.data.totalSources;
      this.totalCountries = response.data.totalCountries[0].count;
      setTimeout(() => {
        this.initCharts();
      }, 100);
    },
    async getCountries() {
      const response = await axios.get(
        `${this.base_url}get-countries/${this.lang}`
      );
      this.country.data = await response.data;
      this.initialLoading = false;
    },
    async getSources() {
      this.isSourceLoading = true;
      const url = this.sourceType(this.filterType);
      let countries = this.country.data;
      let selectCount = [];
      countries.filter(function (e) {
        selectCount.push(e.id);
      });
      const response = await axios.post(this.base_url + url, {
        countries: !this.country.selected.length
          ? selectCount
          : this.country.selected,
        lang: this.lang,
      });

      this.source.data = await response.data;
      this.isSourceLoading = false;
    },
    resetFilter() {
      this.search = "";
      this.country.selected = [];
      this.dateFilter = "";
      this.customDate = "";
      this.startDate = "";
      this.endDate = "";
    },
    media(image) {
      if (!image) return "/international/assets/images/logo.png";
      if (image.includes("png") || image.includes("jpg")) return image;
      return "/international/assets/images/logo.png";
    },
    scroll() {
      let self = this;
      window.onscroll = function () {
        var d = document.documentElement;
        var offset = d.scrollTop + window.innerHeight;
        var height = d.offsetHeight;

        if (
          offset >= height - 1 &&
          (self.tab === 1 || self.tab === 2) &&
          self.allowScroll
        ) {
          self.allowScroll = false;
          self.getLatestMore();
        }
      };
    },
  },
});
