// import Multiselect from 'vue-multiselect'
let multi = window.VueMultiselect.default
var app = new Vue({
    el: '#app',
    components: { 
        Multiselect: multi
     },
    data: {
        isLoading: true,
        username: '',
        password: '',
        error: false,
        phone:'',
        regions:[],
        selectedRegions:[],
        selectedCountries:[],
        countries:[],
        sources:[],
        selectedSources:[],
    },
    mounted() {
        this.isLoading = false
        this.getRegions()
        if (localStorage.getItem("lang")) {
            this.lang= localStorage.getItem("lang");
        }else{
            this.lang = "english"
            localStorage.setItem("lang", this.lang);
        }
    },
    methods: {
          customLabel (region) {
            return `${region.name}`
          },
          customLabel1 (source) {
            return `${source.source}`
          },
          setLanguage() {
            localStorage.setItem("lang", this.lang);
            },
          register() {
            this.isLoading = true;

            let sources = this.selectedSources.map(function(ele) {
                return ele.id;
              });
            
            let regions = this.selectedRegions.map(function(ele) {
                return ele.id;
              });
            let countries = this.selectedCountries.map(function(ele) {
                return ele.id;
              });                 
            axios
                .post('/international/register', {
                    email: this.username,
                    password: this.password,
                    phone:this.phone,
                    regions,
                    countries,
                    sources,
                })
                .then(response => {
                    this.isLoading = false;
                    window.location.href = "/international/login";
                    // this.error = true;
                })
        },
        getRegions(){
            this.isLoading = true
            axios
                .get('/international/getRegions')
                .then(response => {
                    if(response.data){
                       this.regions=response.data.rows;
                        this.isLoading = false;
                    }
                
                    
              
                })
        },
        getCountries(value,id){
            this.isLoading = true
           
            axios
                .post('/international/getcountries',{
                    regionId:value.id
                })
                .then(response => {
                    if(response.data){
                    
                       this.countries = this.countries.concat(response.data.rows);
                        this.isLoading = false;
                    }
                
                    
              
                })
        },
        getSources(value,id){
            this.isLoading = true
           
            axios
                .post('/international/getSourcesReg',{
                    countryId:value.id
                })
                .then(response => {
                    if(response.data){
                    
                       this.sources = this.sources.concat(response.data.rows);
                        this.isLoading = false;
                    }
                
                    
              
                })
        }

    }
})