var app = new Vue({
  el: "#app",
  data: {
    initialLoading: true,
    isLoading: true,
    isLoadingMore: false,
    isSourceLoading: false,
    allowScroll: false,
    base_url: "/international/",
    lang: "english",
    search: "",
    searchSort: "phrase",
    country: {
      data: [],
      selected: [],
      search: "",
      all: false,
    },
    countryAll: true,
    source: {
      data: [],
      selected: [],
      search: "",
      all: false,
    },
    sourceAll: true,
    startDate: "",
    endDate: "",
    customDate: "",
    dateAll: true,
    data: [],
    newsCount: "",
    lastId: "",
    dateFilter: "",
    tab: 1,
    tab2: 1,
    sort: "desc",
    trans: {},
    archiveIdArr: [],
    opinionIdArr: [],
    searchTerm: "",
  },
  mounted() {
    if (localStorage.getItem("lang")) {
        this.lang = localStorage.getItem("lang");
      } else {
        this.lang = "english";
        localStorage.setItem("lang", this.lang);
      }
    this.getSearchQuery();
    this.fetch();
 
  },
  computed: {
    filteredCountry() {
      if (this.country.search)
        return this.country.data
          .filter((val) =>
            val.name.toLowerCase().includes(this.country.search.toLowerCase())
          )
          .sort((a, b) => a.name.localeCompare(b.name));
      if (this.country.all)
        return this.country.data.sort((a, b) => a.name.localeCompare(b.name));
      return this.country.data
        .sort((a, b) => a.name.localeCompare(b.name))
        .slice(0, 6);
    },
    filteredSource() {
      if (this.source.search)
        return this.source.data
          .filter((val) =>
            val.source.toLowerCase().includes(this.source.search.toLowerCase())
          )
          .sort((a, b) =>
            a.source.replace("The ").localeCompare(b.source.replace("The "))
          );
      if (this.source.all)
        return this.source.data.sort((a, b) =>
          a.source
            .replace("The ", "")
            .localeCompare(b.source.replace("The ", ""))
        );
      return this.source.data
        .sort((a, b) =>
          a.source
            .replace("The ", "")
            .localeCompare(b.source.replace("The ", ""))
        )
        .slice(0, 6);
    },
    hasCountry() {
      return this.country.selected.length > 0;
    },
    hasSource() {
      return this.source.selected.length > 0;
    },
  },
  watch: {
    lang(val) {
      val === "english"
        ? (document.getElementById("html-lang").dir = "ltr")
        : (document.getElementById("html-lang").dir = "rtl");
      this.resetFilter();
      this.initialLoading = true;
      this.getLanguage();
      this.getCountries();
    },
    sort() {
      this.newsCount = "";
      this.getLatest();
    },
    tab() {
      this.resetFilter();
    },
    "country.selected"(val) {
      this.countryAll = !val.length ? true : false;
      this.source = {
        data: [],
        selected: [],
        search: "",
        all: false,
      };

      this.newsCount = "";
      this.getSources();
    },
    "source.selected"(val) {
      this.sourceAll = !val.length ? true : false;
      this.newsCount = "";
      this.getLatest();
    },
    dateFilter(val) {
      this.dateAll = !val.length ? true : false;
      if (val) {
        this.customDate = "";
      }
      const todayDate = moment(new Date())
        .subtract(5, "hours")
        .format("YYYY:MM:DD HH:mm:ss");
      if (val == "24h") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(1, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getLatest();
      } else if (val == "48h") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(2, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getLatest();
      } else if (val == "7d") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(7, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getLatest();
      } else if (val == "1m") {
        this.endDate = todayDate;
        this.startDate = moment(new Date())
          .subtract(30, "days")
          .subtract(5, "hours")
          .format("YYYY:MM:DD HH:mm:ss");
        this.getLatest();
      }
    },
    customDate(val) {
      if (val.includes("to")) {
        this.dateFilter = "";
        this.startDate = `${val.split(" to")[0]} ${new Date()
          .toTimeString()
          .substring(0, 8)}`;
        this.endDate = `${val.split("to ")[1]} ${new Date()
          .toTimeString()
          .substring(0, 8)}`;
        this.getLatest();
      }
    },
  },
  filters: {
    formatDate(val) {
      return `${moment(val).fromNow()} - ${moment(val).format(
        "DD:MM:YYYY, hh:mm Z"
      )}`;
    },
  },
  methods: {
    async fetch() {
      await this.getCountries();
      await this.getSources();
      this.getLatest();
      this.getLanguage();
      this.getNewsArchive();
      this.getOpinionArchive();
    },
    setLanguage() {
      localStorage.setItem("lang", this.lang);
    },
    countryCheck(val) {
      if (val.target.value === "on") {
        this.country.selected = [];
      }
    },
    sourceCheck(val) {
      if (val.target.value === "on") {
        this.source.selected = [];
      }
    },
    datesCheck(val) {
      if (val.target.value === "true") {
        this.dateFilter = "";
        this.startDate = "";
        this.endDate = "";
        this.getLatest();
      }
    },
    async getLanguage() {
      this.trans = await (await fetch(`/locale/${this.lang}.json`)).json();
    },
    sourceType(type) {
      if (type === 1 || type === 3) return "get-sources";
      if (type === 2 || type === 4) return "get-opinion-sources";
    },
    latestType(type) {
      if (type === 1) return "get-news";
      if (type === 2) return "get-opinions";
      if (type === 3) return "get-news-archive";
      if (type === 4) return "get-opinion-archive";
    },
    latestMoreType(type) {
      if (type === 1) return "get-news-more";
      if (type === 2) return "get-opinions-more";
    },
    getSearchQuery() {
      this.searchTerm = window.location.search;
      const params = new URL(window.location.href).searchParams;
      this.search = params.get("q");
    },
    searchInput() {
        window.location.href = `${window.location.pathname}?q=${this.search}`;
    },
    async getCountries() {
      const response = await axios.get(
        `${this.base_url}get-countries/${this.lang}`
      );
      this.country.data = await response.data;
      this.initialLoading = false;
    },
    async getSources() {
      this.isSourceLoading = true;
      const url = `get-sources`;
      let countries = this.country.data;
      let selectCount = [];
      countries.filter(function (e) {
        selectCount.push(e.id);
      });
      const response = await axios.post(this.base_url + url, {
        countries: !this.country.selected.length
          ? selectCount
          : this.country.selected,
        lang: this.lang,
      });

      this.source.data = await response.data;
      this.isSourceLoading = false;
    },
    async getLatest() {
      this.data = [];
      this.isLoading = true;
      const url = await this.latestType(this.tab);
      const response = await axios.post(this.base_url + url, {
        startDate: this.startDate,
        endDate: this.endDate,
        sources: this.source.selected,
        lang: this.lang,
        searchKeywords:
          this.searchSort === "keyword" ? this.search.split(" ") : [],
        searchPhrase: this.searchSort !== "keyword" ? this.search : "",
        sort: this.sort,
        countries: !this.source.selected.length
          ? this.country.data
              .filter((val) => this.country.selected.includes(val.id))
              .map((val) => val.name)
          : [],
      });

      this.lastId = (await response.data.rows.length)
        ? response.data.rows[response.data.rows.length - 1].id
        : "";
      this.data = await response.data.rows;
      this.newsCount = (await response.data.count.length)
        ? response.data.count[0].newsCount
        : "";
      this.isLoading = false;
      this.allowScroll = true;
      this.scroll();
    },
    async getLatestMore() {
      this.isLoadingMore = true;
      const url = await this.latestMoreType(this.tab);
      const response = await axios.post(this.base_url + url, {
        startDate: this.startDate,
        endDate: this.endDate,
        sources: this.source.selected,
        lang: this.lang,
        searchKeywords:
          this.searchSort === "keyword" ? this.search.split(" ") : [],
        searchPhrase: this.searchSort !== "keyword" ? this.search : "",
        sort: this.sort,
        lastId: this.lastId,
        countries: !this.source.selected.length
          ? this.country.data
              .filter((val) => this.country.selected.includes(val.id))
              .map((val) => val.name)
          : [],
      });

      this.allowScroll = (await response.data.length) < 10 ? false : true;
      this.lastId = (await response.data.length)
        ? response.data[response.data.length - 1].id
        : "";
      this.data = await this.data.concat(response.data);
      this.isLoadingMore = false;
      this.isLoading = false;
    },
    async newsArchive(id) {
      const response = await axios.post(this.base_url + "add-news-archive", {
        newsId: id,
      });

      await this.getNewsArchive();
    },
    async getNewsArchive() {
      const response = await axios.get(this.base_url + "get-news-archive");

      this.archiveIdArr = await response.data.rows.map((val) => val.news_id);
    },
    async opinionArchive(id) {
      const response = await axios.post(this.base_url + "add-opinion-archive", {
        opinionId: id,
      });

      await this.getOpinionArchive();
    },
    async getOpinionArchive() {
      const response = await axios.get(this.base_url + "get-opinion-archive");

      this.opinionIdArr = await response.data.rows.map((val) => val.news_id);
    },
    async removeNewsArchive(id) {
      this.isLoading = true;
      const response = await axios.post(this.base_url + "delete-news-archive", {
        newsId: id,
      });

      await this.getNewsArchive();
      await this.getLatest();
    },
    async removeOpinionArchive(id) {
      this.isLoading = true;
      const response = await axios.post(
        this.base_url + "delete-opinion-archive",
        {
          opinionId: id,
        }
      );

      await this.getOpinionArchive();
      await this.getLatest();
    },
    resetFilter() {
      this.search = "";
      this.country.selected = [];
      this.dateFilter = "";
      this.customDate = "";
      this.startDate = "";
      this.endDate = "";
    },
    media(image) {
      if (!image) return "/international/assets/images/logo.png";
      if (image.includes("png") || image.includes("jpg")) return image;
      return "/international/assets/images/logo.png";
    },
    scroll() {
      let self = this;
      window.onscroll = function () {
        var d = document.documentElement;
        var offset = d.scrollTop + window.innerHeight;
        var height = d.offsetHeight;

        if (
          offset >= height - 1 &&
          (self.tab === 1 || self.tab === 2) &&
          self.allowScroll
        ) {
          self.allowScroll = false;
          self.getLatestMore();
        }
      };
    },
  },
});
