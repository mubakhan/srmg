var app = new Vue({
    el: '#app',
    data: {
        isLoading: true,
        username: '',
        password: '',
        error: false
    },
    mounted() {
        this.isLoading = false
    },
    methods: {
        login() {
            this.isLoading = true
            axios
                .post('/international/login', {
                    email: this.username,
                    password: this.password,

                })
                .then(response => {
                    if (response.data == true) return window.location.href = "/international/dashboard";
                    this.isLoading = false;
                    this.error = true;
                })
        }

    }
})