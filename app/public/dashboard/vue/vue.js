var vue = new Vue({
    el: '#app',
    data: {
        baseUrl: '/final',
        dataToBeFetch: [],
        regions: [],
        getNews: [],
        getSource: [],
        sources: [],
        sourcesData: [],
        country: [],
        countryData: [],
        countrySources: [],
        countrySourcesCount: [],
        colorSchema: {},
        countryArr: [],
        regionArr: [],
        regionSelect: '',
        countrySelect: '',
        regionSelectArr: [],
        countNewsRegion: [],
        count: {
            totalNews: "",
            totalSources: "",
            totalCountry: "",
            totalRegions: ""
        },
    },
    beforeMount() {
        this.getCount();
        this.regionWiseNewsGraph();
        this.regionWiseSourceGraph();
        this.countryWiseNewsGraph();
        this.countryWiseSourceGraph();
    },
    mounted() {
        
        axios({
            method: "get",
            url: this.baseUrl + `/get-region-country`,
            headers: {
                "Content-Type": "application/json",
                "Access-Control-Allow-Origin": true
            }
        }).then(async response => {
            this.regionArr = response.data;
        });
    },
    methods: {
        
        async getSourceWiseNewsCount(regionName, newsCount) {
            // $('#startLoader').show()
                this.regionSelectArr = [];
                this.countNewsRegion =[];
            axios({
                method: "get",
                url: this.baseUrl + `/get-news-source-by-filter/${regionName}/${newsCount}`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                // $('#startLoader').hide()
                response.data.forEach(async val=>{
                    await this.regionSelectArr.push(val.source_name);
                    await this.countNewsRegion.push(val.count)
                })
                    var region_country_select = await new Chart('region_country_select', {
                    type: 'horizontalBar',
                    data: {
                        labels: await this.regionSelectArr,
                        datasets: [{
                            label: 'Total Sources',
                            data: await this.countNewsRegion,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });
            });
            
        },
        onChangeRegion(value) {
            axios({
                method: "get",
                url: this.baseUrl + `/get-country/${value}`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                this.countryArr = response.data;
            });
        },
        getCount() {
            axios({
                method: "get",
                url: this.baseUrl + "/getCount",
                headers: {
                    'Content-Type': "application/json",
                    'Access-Control-Allow-Origin': true
                }
            }).then(response => {
                $('#startLoader').hide()
                this.count.totalNews = response.data.totalNews[0].newsCount
                this.count.totalSources = response.data.totalSources[0].sourceCount
                this.count.totalCountry = response.data.totalCountry[0].countryCount
                this.count.totalRegions = response.data.totalRegions[0].reginCount
            })
        },
        async regionWiseNewsGraph() {
            axios({
                method: "get",
                url: this.baseUrl + `/get-news-by-region`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                this.getNews = response.data;
                await this.getNews.forEach(async value => {
                    await this.regions.push(value.region_name);
                    await this.dataToBeFetch.push(value.count);
                });

                var newsChart = await new Chart('newsChart', {
                    type: 'bar',
                    data: {
                        labels:await this.regions,
                        datasets: [{
                            label: 'Total News',
                            data: await this.dataToBeFetch,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });

            });
        },
        async regionWiseSourceGraph() {
            axios({
                method: "get",
                url: this.baseUrl + `/get-sources-by-region`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                this.getSource = response.data;
                await this.getSource.forEach(async value => {
                    await this.sources.push(value.region_name);
                    await this.sourcesData.push(value.count);
                });

                var sourceChart = await new Chart('sourceChart', {
                    type: 'bar',
                    data: {
                        labels: await this.sources,
                        datasets: [{
                            label: 'Total Sources',
                            data: await this.sourcesData,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });


            });
        },
        async countryWiseNewsGraph() {
            axios({
                method: "get",
                url: this.baseUrl + `/get-news-by-country`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                this.getSource = response.data;
                await this.getSource.forEach(async value => {
                    await this.country.push(value.country_name);
                    await this.countryData.push(value.count);
                });
                var countrynewsChart = await new Chart('countrynewsChart', {
                    type: 'horizontalBar',
                    data: {
                        labels: await this.country,
                        datasets: [{
                            label: 'Country Wise News',
                            data: await this.countryData,
                            backgroundColor: [
                                'rgba(255, 99, 132, 0.2)',
                                'rgba(54, 162, 235, 0.2)',
                                'rgba(255, 206, 86, 0.2)',
                                'rgba(75, 192, 192, 0.2)',
                                'rgba(153, 102, 255, 0.2)',
                                'rgba(255, 159, 64, 0.2)'
                            ],
                            borderColor: [
                                'rgba(255, 99, 132, 1)',
                                'rgba(54, 162, 235, 1)',
                                'rgba(255, 206, 86, 1)',
                                'rgba(75, 192, 192, 1)',
                                'rgba(153, 102, 255, 1)',
                                'rgba(255, 159, 64, 1)'
                            ],
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });

            });
        },
        async countryWiseSourceGraph() {
            axios({
                method: "get",
                url: this.baseUrl + `/get-sources-by-country`,
                headers: {
                    "Content-Type": "application/json",
                    "Access-Control-Allow-Origin": true
                }
            }).then(async response => {
                this.getSource = response.data;
                await this.getSource.forEach(async value => {
                    await this.countrySources.push(value.country_name);
                    await this.countrySourcesCount.push(value.count);
                });
                var contrysourceChart = await new Chart('contrysourceChart', {
                    type: 'horizontalBar',
                    data: {
                        labels: await this.countrySources,
                        datasets: [{
                            label: 'Country Wise Sources',
                            data: await this.countrySourcesCount,
                            // backgroundColor: this.colorSchema,
                            // borderColor:this.colorSchema,
                            borderWidth: 1
                        }]
                    },
                    options: {
                        scales: {
                            yAxes: [{
                                ticks: {
                                    beginAtZero: true
                                }
                            }]
                        }
                    }
                });

            });
        }
    },
    watch: {
        regionSelect(val) {
            this.onChangeRegion(val)
        },
        countrySelect(val) {
            
            this.getSourceWiseNewsCount(this.regionSelect, this.countrySelect);
        }

    }
});