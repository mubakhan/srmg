const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const HotNews = sequelize.define('hot_news', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    news_id: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    ip: {
        type: DataTypes.STRING(500),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'hot_news',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});
module.exports = HotNews;
