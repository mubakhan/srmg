const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const SourceDataPath = require('./source_data_path');
const SourceUrl =  sequelize.define('source_url', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    url: {
        type: DataTypes.STRING(500),
        allowNull: false
    },
    encoding: {
        type: DataTypes.ENUM('html', 'xml'),
        allowNull: false,
        defaultValue: 'html'
    },
    category_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    source_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'source_url',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});

SourceUrl.hasMany(SourceDataPath, {
    foreignKey: 'sourceUrl_id'
});
SourceDataPath.belongsTo(SourceUrl, {
    foreignKey: 'sourceUrl_id'
});
module.exports = SourceUrl;
