const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const User =  sequelize.define('user', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    oauth_provider: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    email: {
        type: DataTypes.STRING(255),
        allowNull: false,
        unique: 'email'
    },
    name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    phone: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    password: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    picture: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    created: {
        type: DataTypes.DATE(6),
        allowNull: true
    },
    modified: {
        type: DataTypes.STRING(20),
        allowNull: true
    }
}, {
    sequelize,
    tableName: 'user',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'email',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'email' }
            ]
        }
    ]
});
module.exports = User;
