const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const Keywords = sequelize.define('keywords', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    word: {
        type: DataTypes.STRING(222),
        allowNull: false
    },
    keyword_catId: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'keywords',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});
module.exports = Keywords;
