const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const HotOpnions = sequelize.define('hot_opinions', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    opinion_id: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    ip: {
        type: DataTypes.STRING(500),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'hot_opinions',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});
module.exports = HotOpnions;
