const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const Poetries = sequelize.define('poetries', {
    poet_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    poet_title: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    poet_src: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    poet_desc: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    published_by: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    publish_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 1
    }
}, {
    sequelize,
    tableName: 'poetries',
    timestamps: false
});
module.exports = Poetries;
