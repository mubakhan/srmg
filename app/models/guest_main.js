const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');
const { DataTypes } = Sequelize;
const GuestMain = sequelize.define('guest_main', {
    drama_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    drama_title: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    drama_cover_photo: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    drama_total_episodes: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    publish_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    create: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    is_active: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1
    }
}, {
    sequelize,
    tableName: 'guest_main',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'drama_id' }
            ]
        }
    ]
});
module.exports = GuestMain;
