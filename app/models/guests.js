const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');
const { DataTypes } = Sequelize;
const Guests = sequelize.define('guests', {
    episode_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    episode_no: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    episode_title: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    episode_shortcode: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    publish_date: {
        type: DataTypes.DATEONLY,
        allowNull: false
    },
    drama_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    create: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    is_active: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1
    }
}, {
    sequelize,
    tableName: 'guests',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'episode_id' }
            ]
        }
    ]
});
module.exports = Guests;
