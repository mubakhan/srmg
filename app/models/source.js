const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');

const { DataTypes } = Sequelize;
const SourceUrl = require('./source_url');

const Source =  sequelize.define('source', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    source: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    slug: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: 'slug'
    },
    website: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    image: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    lang: {
        type: DataTypes.ENUM('english', 'urdu', 'arabic', 'pushto'),
        allowNull: false,
        defaultValue: 'english'
    },
    color: {
        type: DataTypes.STRING(10),
        allowNull: false,
        defaultValue: '#F7921C'
    },
    default: {
        type: DataTypes.ENUM('true', 'false'),
        allowNull: false,
        defaultValue: 'true'
    },
    country_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1
    },
    type: {
        type: DataTypes.STRING(22),
        allowNull: false,
        defaultValue: 'simple'
    },
    updated_at: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
}, {
    sequelize,
    tableName: 'source',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'slug',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'slug' }
            ]
        },
        {
            name: 'lang',
            using: 'BTREE',
            fields: [
                { name: 'lang' }
            ]
        },
        {
            name: 'id',
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'country_id',
            using: 'BTREE',
            fields: [
                { name: 'country_id' }
            ]
        },
        {
            name: 'id_2',
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});

Source.hasMany(SourceUrl, {
    foreignKey: 'source_id'
});
SourceUrl.belongsTo(Source, {
    foreignKey: 'source_id'
});
module.exports = Source;
