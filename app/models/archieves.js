const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');

const Archives = sequelize.define('archieves', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    news_id: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    oauth_uid: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    oauth_provider: {
        type: DataTypes.STRING(1000),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'archieves',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});

module.exports = Archives;
