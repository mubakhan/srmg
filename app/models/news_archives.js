const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const NewsArchives = sequelize.define('news_archives', {
    achive_id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    news_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'news_archives',
    timestamps: true,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'achive_id' }
            ]
        }
    ]
});
module.exports = NewsArchives;
