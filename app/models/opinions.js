const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const Opinions = sequelize.define('opinions', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING(222),
        allowNull: true
    },
    slug: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    link: {
        type: DataTypes.STRING(222),
        allowNull: true,
        unique: 'link'
    },
    description: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    body: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    media: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    views: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    source_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    category_id: {
        type: DataTypes.STRING(22),
        allowNull: false,
        defaultValue: '1'
    },
    author_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    author: {
        type: DataTypes.STRING(222),
        allowNull: true
    },
    hot: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    cold: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    pub_date: {
        type: DataTypes.DATE,
        allowNull: false
    },
    source: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    lang: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    source_slug: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    country: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    col_stat: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
}, {
    sequelize,
    tableName: 'opinions',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'link',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'link' }
            ]
        }
    ]
});
module.exports = Opinions;
