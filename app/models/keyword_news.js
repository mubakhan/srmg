const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const KeywordNews =  sequelize.define('keyword_news', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    news_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    keyword_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'keyword_news',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'news_id',
            using: 'BTREE',
            fields: [
                { name: 'news_id' }
            ]
        },
        {
            name: 'keyword_id',
            using: 'BTREE',
            fields: [
                { name: 'keyword_id' }
            ]
        }
    ]
});
module.exports = KeywordNews;
