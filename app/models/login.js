const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');
const { DataTypes } = Sequelize;
const Login =  sequelize.define('login', {
    login_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    login_username: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    login_email: {
        type: DataTypes.STRING(30),
        allowNull: false
    },
    login_password: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    create: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    is_active: {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: 1
    }
}, {
    sequelize,
    tableName: 'login',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'login_id' }
            ]
        }
    ]
});
module.exports = Login;
