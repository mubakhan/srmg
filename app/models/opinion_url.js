const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const OpinionUrl =  sequelize.define('opinion_url', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    url: {
        type: DataTypes.STRING(222),
        allowNull: false
    },
    opinion_author_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    source_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    title: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
}, {
    sequelize,
    tableName: 'opinion_url',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});


module.exports = OpinionUrl;
