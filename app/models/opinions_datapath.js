const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const OpinionDataPath =  sequelize.define('opinions_datapath', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    data_name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    data_path: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    data_param: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    data_filter: {
        type: DataTypes.STRING(1000),
        allowNull: true
    },
    data_type: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'string'
    },
    opinionUrl_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'opinions_datapath',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});

module.exports = OpinionDataPath;
