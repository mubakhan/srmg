const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const KeywordCategory = sequelize.define('keyword_category', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    name: {
        type: DataTypes.STRING(22),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'keyword_category',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});
module.exports = KeywordCategory;
