const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');
const { DataTypes } = Sequelize;
const News = sequelize.define('news', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    title: {
        type: DataTypes.STRING(300),
        allowNull: false,
        unique: 'title_2'
    },
    slug: {
        type: DataTypes.STRING(500),
        allowNull: true
    },
    description: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    body: {
        type: DataTypes.TEXT,
        allowNull: false
    },
    summary: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    media: {
        type: DataTypes.STRING(500),
        allowNull: true
    },
    url: {
        type: DataTypes.STRING(300),
        allowNull: false,
        unique: 'url_2'
    },
    pub_date: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    },
    tags: {
        type: DataTypes.STRING(1000),
        allowNull: true
    },
    source_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    category_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    sub_category: {
        type: DataTypes.STRING(255),
        allowNull: true
    },
    hot: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    cold: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    keyStatus: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    source: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    lang: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    source_slug: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    country: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    col_stat: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    },
    seen: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 0
    }
}, {
    sequelize,
    tableName: 'news',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'title_2',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'title' }
            ]
        },
        {
            name: 'url_2',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'url' }
            ]
        },
        {
            name: 'title',
            using: 'BTREE',
            fields: [
                { name: 'title' }
            ]
        },
        {
            name: 'category',
            using: 'BTREE',
            fields: [
                { name: 'category_id' }
            ]
        },
        {
            name: 'pub_date',
            using: 'BTREE',
            fields: [
                { name: 'pub_date' }
            ]
        },
        {
            name: 'source',
            using: 'BTREE',
            fields: [
                { name: 'source_id' }
            ]
        },
        {
            name: 'id',
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'url',
            using: 'BTREE',
            fields: [
                { name: 'url' }
            ]
        },
        {
            name: 'id_2',
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'lang',
            using: 'BTREE',
            fields: [
                { name: 'lang' }
            ]
        },
        {
            name: 'country',
            using: 'BTREE',
            fields: [
                { name: 'country' }
            ]
        },
        {
            name: 'source_id_category_id_index',
            using: 'BTREE',
            fields: [
                { name: 'source_id' },
                { name: 'category_id' }
            ]
        }
    ]
});
module.exports = News;
