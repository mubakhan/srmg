const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');

const Author = sequelize.define('author', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    author: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    news_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'author',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'author',
            using: 'BTREE',
            fields: [
                { name: 'author' }
            ]
        }
    ]
});


module.exports = Author;
