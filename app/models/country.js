const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const Source = require('./source');
const OpinionSource = require('./opinion_source');
const Country = sequelize.define('country', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    region_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
            model: 'regions',
            key: 'id'
        }
    },
    name: {
        type: DataTypes.STRING(222),
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'country',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'id',
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'region_id',
            using: 'BTREE',
            fields: [
                { name: 'region_id' }
            ]
        }
    ]
});
Country.hasMany(Source, {
    foreignKey: 'country_id'
});
Source.belongsTo(Country, {
    foreignKey: 'country_id'
});
Country.hasMany(OpinionSource, {
    foreignKey: 'country_id'
});
module.exports = Country;
