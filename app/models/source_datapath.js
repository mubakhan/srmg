const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const SourceDataPath =  sequelize.define('source_datapath', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    data_name: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    data_path: {
        type: DataTypes.STRING(1000),
        allowNull: false
    },
    data_param: {
        type: DataTypes.STRING(50),
        allowNull: true
    },
    data_filter: {
        type: DataTypes.STRING(1000),
        allowNull: true
    },
    data_type: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: 'string'
    }
}, {
    sequelize,
    tableName: 'source_datapath',
    timestamps: false
});
module.exports = SourceDataPath;
