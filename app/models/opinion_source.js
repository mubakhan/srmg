const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const OpinionSource =  sequelize.define('opinion_source', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    source: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    slug: {
        type: DataTypes.STRING(255),
        allowNull: true,
        unique: 'slug'
    },
    website: {
        type: DataTypes.STRING(255),
        allowNull: false
    },
    image: {
        type: DataTypes.TEXT,
        allowNull: true
    },
    lang: {
        type: DataTypes.ENUM('english', 'urdu', 'arabic', 'pushto'),
        allowNull: false,
        defaultValue: 'english'
    },
    color: {
        type: DataTypes.STRING(10),
        allowNull: false,
        defaultValue: '#F7921C'
    },
    default: {
        type: DataTypes.ENUM('true', 'false'),
        allowNull: false,
        defaultValue: 'true'
    },
    country_id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        defaultValue: 1
    }
}, {
    sequelize,
    tableName: 'opinion_source',
    timestamps: true,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        },
        {
            name: 'slug',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'slug' }
            ]
        },
        {
            name: 'lang',
            using: 'BTREE',
            fields: [
                { name: 'lang' }
            ]
        }
    ]
});

module.exports = OpinionSource;
