const sequelize = require('../factory/sequelize');
const Sequelize = require('sequelize');
const { DataTypes } = Sequelize;

const Playlist =  sequelize.define('playlist', {
    playlist_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    playlist_title: {
        type: DataTypes.STRING(300),
        allowNull: false
    },
    playlist_link: {
        type: DataTypes.STRING(300),
        allowNull: false
    },
    playlist_desc: {
        type: DataTypes.STRING(300),
        allowNull: false
    },
    playlist_code: {
        type: DataTypes.STRING(100),
        allowNull: false
    },
    is_active: {
        type: DataTypes.TINYINT,
        allowNull: false,
        defaultValue: 1
    },
    create_on: {
        type: DataTypes.DATE,
        allowNull: false,
        defaultValue: Sequelize.literal('CURRENT_TIMESTAMP')
    }
}, {
    sequelize,
    tableName: 'playlist',
    timestamps: false
});
module.exports = Playlist;
