const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const UserCategories =  sequelize.define('user_categories', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    category_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'user_categories',
    timestamps: false
});

module.exports = UserCategories;
