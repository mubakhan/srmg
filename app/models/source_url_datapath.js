const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const SourceUrlDataPath = sequelize.define('source_url_datapath', {
    id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    source_url_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    source_datapath_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'source_url_datapath',
    timestamps: false
});
module.exports = SourceUrlDataPath;
