const sequelize = require('../factory/sequelize');
const { DataTypes } = require('sequelize');
const UserSource =  sequelize.define('user_source', {
    id: {
        autoIncrement: true,
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true
    },
    user_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    },
    source_id: {
        type: DataTypes.INTEGER,
        allowNull: false
    }
}, {
    sequelize,
    tableName: 'user_source',
    timestamps: false,
    indexes: [
        {
            name: 'PRIMARY',
            unique: true,
            using: 'BTREE',
            fields: [
                { name: 'id' }
            ]
        }
    ]
});
module.exports = UserSource;
