const Opinion = require('../models/opinions');
const HotOpinion = require('../models/hot_opinions');
const ColdOpinion = require('../models/cold_opinions');
const { Op } = require('sequelize');


const getOpinion = async ({ body }, res)=>{
    const { search = null } = body;
    const Opinion = search ? await getOpinionFromES(body) : await getOpinionFromDB(body);
    res.json(Opinion);
};

const getOpinionFromES = async (body)=>{
    const { search = null, sourceId = null, sourceIds = [], categoryId = null, categoryIds = [], lang = 'english', limit = 20, lastId = null, order = { type: 'DESC', field: 'id' } } = body;
};


const getOpinionFromDB = async (body)=>{
    const { authors = [], author = null, opinionId = null, opinionIds = [], sourceId = null, sourceIds = [], categoryId = null, categoryIds = [], lang = 'english', limit = 20, lastId = null, order = { type: 'DESC', field: 'id' }, pubDate = null } = body;
    const opinion = await Opinion.findAll({
        where: {
            ...(author && { author }),
            ...(authors.lenght && {
                authors: {
                    [Op.in]: authors
                }
            }),
            ...(opinionId && {
                id:opinionId
            }),
            ...(opinionIds.length && {
                id: {
                    [Op.in]: opinionIds
                }
            }),
            ...(lastId && {
                id: {
                    [Op.lt]: lastId
                }
            }),
            ...(sourceId && {
                source_id: sourceId
            }),
            ...(sourceIds.length && {
                source_id: {
                    [Op.in]: sourceIds
                }
            }),
            ...(categoryId && {
                category_id: categoryId
            }),
            ...(categoryIds.length && {
                category_id: {
                    [Op.in]: categoryIds
                }
            }),
            ...(lang && {
                lang
            }),
            ...(pubDate && {
                pub_date: {
                    [Op.between]: [pubDate.from, pubDate.to]
                }
            })
        },
        order: [
            [order.field, order.type]
        ],
        limit,
        raw: true
    });

    return opinion;
};

const addHot = async ({ body :{ opinionId }, ip }, res)=>{
    ip = ip.substr(0, 7) === '::ffff:' ? ip.substr(7) : ip;
    try {
        const hotOpinion = await HotOpinion.findOne({ where:{ opinion_id:opinionId, ip } });
        if (hotOpinion) return res.send({ success:false, message:'you have already added hot to this opinion' });

        const coldOpinion = await ColdOpinion.findOne({ where:{ opinion:opinionId, ip } });
        if (coldOpinion) return res.send({ success:false, message:'you have already added cold to this opinion' });

        await HotOpinion.create({ opinion_id:opinionId, ip });
        await Opinion.increment({ hot:+1 }, { where:{ id:opinionId } });
        return res.send({ success:true, message:'you have added hot to this opinion' });
    } catch (err) {
        res.send(err);
    }
};

const addCold = async ({ body :{ opinionId }, ip }, res)=>{
    ip = ip.substr(0, 7) === '::ffff:' ? ip.substr(7) : ip;
    try {
        const hotOpinion = await HotOpinion.findOne({ where:{ opinion_id:opinionId, ip } });
        if (hotOpinion) return res.send({ success:false, message:'you have already added hot to this opinion' });

        const coldOpinion = await ColdOpinion.findOne({ where:{ opinion_id:opinionId, ip } });
        if (coldOpinion) return res.send({ success:false, message:'you have already added cold to this opinion' });

        await ColdOpinion.create({ opinion_id:opinionId, ip });
        await Opinion.increment({ cold:+1 }, { where:{ id:opinionId } });
        return res.send({ success:true, message:'you have added cold to this opinion' });
    } catch (err) {
        res.send(err);
    }
};

module.exports = {
    getOpinion,
    addHot,
    addCold
};
