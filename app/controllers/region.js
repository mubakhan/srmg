const Regions = require('../models/regions');
const { Op } = require('sequelize');


const getRegions = async (body)=>{
    const {  regionId = null, regionIds = [] } = body;

    const sources = await Regions.findAll({
        where: {
            ...(regionId && {
                id: regionId
            }),
            ...(regionIds.length && {
                id: {
                    [Op.in]: regionIds
                }
            })
        },
        raw: true
    });

    return sources;
};

module.exports = {
    getRegions
};
