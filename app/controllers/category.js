const Category = require('../models/category');

const getCategories = async ()=>{
    const categories = await Category.findAll({ raw: true });
    return categories;
};

module.exports = {
    getCategories
};
