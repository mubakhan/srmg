const News = require('../models/news');
const HotNews = require('../models/hot_news');
const ColdNews = require('../models/cold_news');
const { Op } = require('sequelize');
const client = require('../factory/elasticsearch');

const getNews = async ({ body }, res)=>{
    const { search = null } = body;
    const news = search ? await getNewsFromES(body) : await getNewsFromDB(body);
    res.json(news);
};

const getNewsFromES = async (body)=>{
    const { search = null, sourceId = null, sourceIds = [], categoryId = null, categoryIds = [], lang = null, limit = 20, lastId = null, order = { type: 'DESC', field: 'id' }, pubDate = null } = body;
    const query = {
        from: 0,
        size: limit,
        query: {
            bool: {
                must: []
            }
        },
        sort: [
            {
                [order.field]: {
                    order: order.type
                }
            }
        ],
        _source: false
    };
    if (search) {
        query.query.bool.must.push({
            query_string: {
                query: `*${search.query.replace(/ /g, '\\ ')}*`,
                fields: search.fields && search.fields.length
                    ? search.fields.map(field => field === 'title' ? `${field}.keyword` : field)
                    : [
                        'title.keyword',
                        'body'
                    ]
            }
        });
    }
    if (sourceId) {
        query.query.bool.must.push(
            {
                match: {
                    source_id: sourceId
                }
            }
        );
    }
    if (sourceIds.length) {
        query.query.bool.must.push(
            {
                terms: {
                    source_id: sourceIds
                }
            }
        );
    }
    if (categoryId) {
        query.query.bool.must.push(
            {
                match: {
                    category_id: categoryId
                }
            }
        );
    }
    if (categoryIds.length) {
        query.query.bool.must.push(
            {
                terms: {
                    category_id: categoryIds
                }
            }
        );
    }
    if (lang) {
        query.query.bool.must.push(
            {
                match: {
                    lang
                }
            }
        );
    }
    if (lastId) {
        query.query.bool.must.push(
            {
                range: {
                    id: {
                        lt: lastId
                    }
                }
            }
        );
    }
    if (pubDate) {
        query.query.bool.must.push(
            {
                range: {
                    pub_date: {
                        gte: pubDate.from,
                        lte: pubDate.to
                    }
                }
            }
        );
    }
    console.log(JSON.stringify(query, null, 2));
    const result = await client.search({
        index: 'news',
        body: query
    });
    const newsIds = result.body.hits.hits.map(item=>Number(item._id));
    return newsIds.length ? await getNewsFromDB({ newsIds, limit, lastId, order, pubDate }) : [];
};


const getNewsFromDB = async (body)=>{
    const { newsId = null, newsIds = [], sourceId = null, sourceIds = [], categoryId = null, categoryIds = [], lang = null, limit = 21, lastId = null, order = { type: 'DESC', field: 'id' }, pubDate = null } = body;
    const news = await News.findAll({
        where: {
            ...(newsId && {
                id: newsId
            }),
            ...(newsIds.length && {
                id: {
                    [Op.in]: newsIds
                }
            }),
            ...(lastId && {
                id: {
                    [Op.lt]: lastId
                }
            }),
            ...(sourceId && {
                source_id: sourceId
            }),
            ...(sourceIds.length && {
                source_id: {
                    [Op.in]: sourceIds
                }
            }),
            ...(categoryId && {
                category_id: categoryId
            }),
            ...(categoryIds.length && {
                category_id: {
                    [Op.in]: categoryIds
                }
            }),
            ...(lang && {
                lang
            }),
            ...(pubDate && {
                pub_date: {
                    [Op.between]: [pubDate.from, pubDate.to]
                }
            })
        },
        order: [
            [order.field, order.type]
        ],
        limit,
        raw: true
    });

    return news;
};

const addHot = async ({ body :{ newsId }, ip }, res)=>{
    ip = ip.substr(0, 7) === '::ffff:' ? ip.substr(7) : ip;
    try {
        const hotNews = await HotNews.findOne({ where:{ news_id:newsId, ip } });
        if (hotNews) return res.send({ success:false, message:'you have already added hot to this news' });

        const coldNews = await ColdNews.findOne({ where:{ news_id:newsId, ip } });
        if (coldNews) return res.send({ success:false, message:'you have already added cold to this news' });

        await HotNews.create({ news_id:newsId, ip });
        await News.increment({ hot:+1 }, { where:{ id:newsId } });
        return res.send({ success:true, message:'you have added hot to this news' });
    } catch (err) {
        res.send(err);
    }
};

const addCold = async ({ body :{ newsId }, ip }, res)=>{
    ip = ip.substr(0, 7) === '::ffff:' ? ip.substr(7) : ip;
    try {
        const hotNews = await HotNews.findOne({ where:{ news_id:newsId, ip } });
        if (hotNews) return res.send({ success:false, message:'you have already added hot to this news' });

        const coldNews = await ColdNews.findOne({ where:{ news_id:newsId, ip } });
        if (coldNews) return res.send({ success:false, message:'you have already added cold to this news' });

        await ColdNews.create({ news_id:newsId, ip });
        await News.increment({ cold:+1 }, { where:{ id:newsId } });
        return res.send({ success:true, message:'you have added cold to this news' });
    } catch (err) {
        res.send(err);
    }
};

module.exports = {
    getNews,
    addHot,
    addCold
};
