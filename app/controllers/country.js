const Country = require('../models/country');
const Source = require('../models/source');
const OpinionSource = require('../models/opinion_source');
const { Op } = require('sequelize');


const getCountries = async (body)=>{
    const { name = null, regionId = null, regionIds = [], lang = null } = body;

    const country = await Country.findAll({
        where: {
            ...(regionId && {
                region_id: regionId
            }),
            ...(name && {
                name
            }),
            ...(regionIds.length && {
                region_id: {
                    [Op.in]: regionIds
                }
            })
        },
        raw: true,
        ...( lang && {
            include: [
                {
                    model: Source,
                    where: {
                        lang
                    }
                }
            ]
        } )

    });

    return country;
};
const getOpinionCountries = async (body)=>{
    const { name = null, regionId = null, regionIds = [], lang = null } = body;

    const country = await Country.findAll({
        where: {
            ...(regionId && {
                region_id: regionId
            }),
            ...(name && {
                name
            }),
            ...(regionIds.length && {
                region_id: {
                    [Op.in]: regionIds
                }
            })
        },
        raw: true,
        include: [
            {
                model: OpinionSource,
                where: {
                    lang
                },
                required: true
            }
        ]


    });

    return country;
};


module.exports = {
    getCountries,
    getOpinionCountries
};
