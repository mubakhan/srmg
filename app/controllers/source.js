const Source = require('../models/source');
const { Op } = require('sequelize');


const getSources = async (body)=>{
    const {  sourceId = null, sourceIds = [], countryId = null, countryIds = [], lang = null } = body;

    const sources = await Source.findAll({
        where: {
            ...(sourceId && {
                id: sourceId
            }),
            ...(countryId && {
                country_id: countryId
            }),
            ...(countryIds.length && {
                country_id: {
                    [Op.in]: countryIds
                }
            }),
            ...(sourceIds.length && {
                id: {
                    [Op.in]: sourceIds
                }
            }),
            ...(lang && { lang })
        },
        order: [
            ['updated_at', 'DESC']
        ],
        raw: true
    });

    return sources;
};

module.exports = {
    getSources
};
