const News = require('./news');
const Category = require('./category');
const Source = require('./source');
const Country = require('./country');
const Region = require('./region');

module.exports = {
    News,
    Category,
    Source,
    Country,
    Region
};
