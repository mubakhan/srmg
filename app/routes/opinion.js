const express = require('express'),
    router = express.Router();
const query = require('../factory/query');
const { json } = require('body-parser');
const Opinion = require('../controllers/opinion');

router.post('/opinion/fetch', async (req, res) => {
    return Opinion.getOpinion(req, res);
});


router.post('/getOpinions/', async (req, res) => {
    console.log('opinion page');
    const { sources, lang } = req.body;
    const temp = [];
    for (const element of sources) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.lang="${lang}" And opinions.source_id in ( ${temp} ) ORDER BY opinions.id DESC LIMIT 50`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/getOpinionsMore/', async (req, res) => {
    const { sources, lang, lastId } = req.body;
    const temp = [];
    for (const element of sources) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source,opinions.lang,opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.lang='${lang}' And opinions.source_id in ( ${temp} ) AND  opinions.id < ${lastId} ORDER BY opinions.id DESC LIMIT 50`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.get('/opnionSourcesAll/', async (req, res) => {

    try {

        const sources = await query('SELECT * FROM `opinion_source` where lang="english"');
        res.send(sources);
    } catch (err) {
        console.log(err);
    }
});

router.post('/get_sourcesOnIdsOpinion/', async (req, res) => {
    const sourcesIds = req.body.sourcesId;
    const temp = [];
    for (const element of sourcesIds) {
        temp.push(Number(element));
    }

    try {
        const sources = await query(`SELECT * FROM opinion_source  where id in (${temp}) ORDER BY opinion_source.updated_at DESC`);
        //     let newSources=[];
        //     for (const iterator of sources) {
        //         const newTempSources = await query(`SELECT * FROM source WHERE id = ${iterator.source_id}`);
        //         newSources.push(newTempSources[0]);

        //     }
        //   let temNewSources = []
        //     for (const source of newSources) {
        //             for (const iterator of sourcesIds) {

        //                 // console.log(iterator,source.id)
        //                 if(source.id ==iterator ){
        //                     temNewSources.push(source)
        //                 }
        //                     }
        //     }
        res.send(sources);

    } catch (err) {
        console.log(err);
    }
});

router.get('/getCountrySourcesOpinion/:countryName', async (req, res) => {
    const name = req.params.countryName;
    try {
        const country = await query(`SELECT * FROM \`country\` WHERE \`name\` = '${name}'`);
        if (country.length > 0) {
            for (const element of country) {
                const sources = await query(`SELECT s.* FROM \`opinion_source\` as s INNER JOIN country as c ON s.\`country_id\` = c.\`id\` WHERE s.\`lang\`="english" AND c.\`id\`=${element.id}`);
                res.send({ Success: true, sources });
            }

        } else {
            const sources = await query('SELECT * FROM `opinion_source` WHERE `country_id` = 1 AND`lang`="english"');
            res.send({ Success: true, sources });
        }
        // res.send(rows);
    } catch (err) {
        console.log(err);
    }

});

router.post('/get_catOpinions', async (req, res) => {
    try {
        const { cateId, sources } = req.body;
        const temp = [];
        for (const element of sources) {
            temp.push(Number(element));
        }
        const newsCount = await query(`SELECT COUNT(n.id) as count FROM opinions as n where n.category_id= ${cateId}  AND n.source_id in ( ${temp} )`);
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.source_id in ( ${temp} ) AND category_id = ${cateId} ORDER BY opinions.id DESC LIMIT 50`);
        res.send({ rows, newsCount });
    } catch (err) {
        console.log(err);
    }
});

router.post('/get_catOpinionMore', async (req, res) => {
    try {
        const { cateId, sources, lastId } = req.body;
        const temp = [];
        for (const element of sources) {
            temp.push(Number(element));
        }
        if (cateId == '0') {

        } else {
            const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.source_id in ( ${temp} ) AND category_id = ${cateId} AND opinions.id < ${lastId}  ORDER BY opinions.id DESC LIMIT 50`);
            res.send(rows);
        }

    } catch (err) {
        console.log(err);
    }
});

router.post('/opinionSources/', async (req, res) => {
    const { countryIds } = req.body;
    const temp = [];
    for (const element of countryIds) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT * FROM \`opinion_source\` WHERE \`country_id\` in (${temp}) And lang="english"`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/getOpinionSources/', async (req, res) => {
    const sourcesIds = req.body.sourcesId;
    const temp = [];
    for (const element of sourcesIds) {
        temp.push(Number(element));
    }

    try {
        const sources = await query(`SELECT * FROM opinion_source  where id in (${temp}) AND lang="english" ORDER BY opinion_source.updated_at DESC`);

        // let sources = await query(`SELECT source_id, MAX(id) FROM opinions GROUP BY source_id ORDER BY MAX( id ) DESC`);
        // let newSources = [];
        // for (const iterator of sources) {
        //     const newTempSources = await query(`SELECT * FROM opinion_source WHERE id = ${iterator.source_id}`);
        //     newSources.push(newTempSources[0]);

        // }
        // let temNewSources = []
        // for (const source of newSources) {
        //     for (const iterator of sourcesIds) {

        //         // console.log(iterator,source.id)
        //         if (source.id == iterator) {
        //             temNewSources.push(source)
        //         }
        //     }
        // }
        res.send(sources);

    } catch (err) {
        console.log(err);
    }
});

router.post('/getOpinionSources/urdu/', async (req, res) => {
    const sourcesIds = req.body.sourcesId;
    const temp = [];
    for (const element of sourcesIds) {
        temp.push(Number(element));
    }

    try {
        const sources = await query(`SELECT * FROM opinion_source  where id in (${temp}) AND lang="urdu" ORDER BY opinion_source.updated_at DESC`);

        // let newSources = [];
        // for (const iterator of sources) {
        //     const newTempSources = await query(`SELECT * FROM opinion_source WHERE id = ${iterator.source_id}`);
        //     newSources.push(newTempSources[0]);

        // }
        // let temNewSources = []
        // for (const source of newSources) {
        //     for (const iterator of sourcesIds) {

        //         // console.log(iterator,source.id)
        //         if (source.id == iterator) {
        //             temNewSources.push(source)
        //         }
        //     }
        // }
        res.send(sources);

    } catch (err) {
        console.log(err);
    }
});


router.get('/opinionDetail/:id', async (req, res) => {
    try {
        const opinion = await query(`SELECT opinions.id, opinions.category_id,opinions.title,opinions.link , opinions.hot, opinions.cold,opinions.slug,opinions.description,opinions.category_id, opinions.body, opinions.media, opinions.views, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source,opinions.lang,opinions.source_slug as sources_slug, opinions.country as country from opinions  WHERE opinions.id =${req.params.id}`);
        const realTimeOpinions = await query(`SELECT opinions.id, opinions.title,opinions.link, opinions.slug,opinions.description, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source,opinions.lang,opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.lang = "${opinion[0].lang}" And opinions.category_id = ${opinion[0].category_id} ORDER BY opinions.id DESC LIMIT 10`);

        const views = opinion[0].views + 1;

        const updateOpinion = await query(`UPDATE opinions SET views = ${views} WHERE id = ${req.params.id} `);

        res.send({ opinion, realTimeOpinions });
    } catch (err) {
        console.log(err);
    }
});

router.post('/singleSourceOpnions', async (req, res) => {
    try {
        const { sourcesId } = req.body;

        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.source_id  =  ${sourcesId}  ORDER BY opinions.id DESC LIMIT 50`);

        res.send({ rows });


    } catch (err) {
        console.log(err);
    }
});

router.post('/singleSourceOpnions/more', async (req, res) => {
    try {
        const { sourcesId, lastId } = req.body;
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.source_id  =  ${sourcesId} AND opinions.id < ${lastId} ORDER BY opinions.id DESC LIMIT 50`);
        res.send({ rows });


    } catch (err) {
        console.log(err);
    }
});


router.post('/archiveOpinions/', async (req, res) => {
    const { opinionId } = req.body;
    const temp = [];
    for (const element of opinionId) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.hot, opinions.cold, opinions.media, opinions.views, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,source.source,source.lang,source.slug as sources_slug, country.name as country from opinions inner join source  on opinions.source_id = source.id inner join country on source.country_id = country.id  And opinions.id  IN (${temp}) ORDER BY opinions.id DESC LIMIT 50`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/get_countriesOpinion', async (req, res) => {
    try {
        const { regionId } = req.body;
        const temp = [];
        for (const element of regionId) {
            temp.push(Number(element));
        }
        const countryArr = [];
        const selectCoutryOpnion = await query('SELECT DISTINCT(country_id) FROM opinion_source');
        for (const iterator of selectCoutryOpnion) {
            countryArr.push(iterator.country_id);
        }
        const rows = await query(`SELECT * FROM \`country\` WHERE \`region_id\` in (${temp}) AND id in (${countryArr})`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/get_countries/all-opinion', async (req, res) => {
    try {
        let countryArr = [];
        const selectCoutryOpnion = await query(`SELECT DISTINCT(country_id) FROM opinion_source`);
        for (const iterator of selectCoutryOpnion) {
            countryArr.push(iterator.country_id);
        }
        const rows = await query('SELECT * FROM `country` where id in (' + countryArr + ')');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/getTrending_sourcesOpinions', async (req, res) => {
    const { sources } = req.body;
    const temp = [];
    for (const element of sources) {
        temp.push(Number(element));
    }
    try {
        const row = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions where opinions.source_id in (${temp}) ORDER BY views DESC LIMIT 50`);
        res.send(row);
    } catch (error) {
        console.log(error);

    }

});

router.get('/getOpinionSources/urdu/', async (req, res) => {
    const name = req.params.countryName;
    try {
        const sources = await query('SELECT * FROM `opinion_source` WHERE `country_id` = 1 AND`lang`="urdu"');
        res.send({ Success: true, sources });

    } catch (err) {
        console.log(err);
    }

});
router.get('/getOpinionSources/pushto', async (req, res) => {
    try {
        const sources = await query('SELECT * FROM `opinion_source` WHERE lang="pushto"');
        res.send({ Success: true, sources });

    } catch (err) {
        console.log(err);
    }

});
router.post('/get_sourcesOpinionPU/', async (req, res) => {
    const { countryIds } = req.body;
    const temp = [];
    for (const element of countryIds) {
        temp.push(Number(element));
    }
    try {
        const rows = await query('SELECT * FROM `opinion_source` WHERE lang="pushto"');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/get_opinionCountries/pu', async (req, res) => {
    try {
        const rows = await query('SELECT country.* FROM `country` INNER JOIN opinion_source on opinion_source.country_id=country.id And opinion_source.lang="pushto"');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/get_countriesOpinion/ar', async (req, res) => {
    try {
        const rows = await query('SELECT country.* FROM `country` INNER JOIN opinion_source on opinion_source.country_id=country.id And opinion_source.lang="arabic"');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.post('/get_countriesOpinion/ar', async (req, res) => {
    try {
        const { regionId } = req.body;
        const temp = [];
        for (const element of regionId) {
            temp.push(Number(element));
        }

        const rows = await query(`SELECT country.* FROM \`country\` INNER JOIN opinion_source on opinion_source.country_id=country.id And opinion_source.lang="arabic" And country.region_id in (${temp})`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/get_sourcesOpinionAR/', async (req, res) => {
    const { countryIds } = req.body;
    const temp = [];
    for (const element of countryIds) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT * FROM \`opinion_source\` WHERE \`country_id\` in (${temp}) And lang="arabic"`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.get('/getOpinionSources/arabic', async (req, res) => {
    const name = req.params.countryName;
    try {
        const sources = await query('SELECT * FROM `opinion_source` WHERE `lang`="arabic"');
        res.send(sources);

    } catch (err) {
        console.log(err);
    }

});
//////////////////////////////////////
/////////Date Opinions///////////////
////////////////////////////////////


router.post('/getSourcesOpinion_Dated/', async (req, res) => {
    const { sources, startDate, endDate } = req.body;
    const temp = [];
    for (const element of sources) {
        temp.push(Number(element));
    }
    try {
        const newsCount = await query(`SELECT COUNT(id) as count FROM opinions  WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp})`);
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp}) ORDER BY id DESC LIMIT 10`);
        res.send({ rows, newsCount });
    } catch (err) {
        console.log(err);
    }
});

router.post('/getSourcesOpinion_Dated_more/', async (req, res) => {
    const { sources, startDate, endDate, lastId } = req.body;
    const temp = [];
    for (const element of sources) {
        temp.push(Number(element));
    }
    try {
        const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp}) AND id < ${lastId} ORDER BY id DESC LIMIT 10`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/get_catOpinionDated', async (req, res) => {

    try {
        const { cateId, sources, startDate, endDate } = req.body;
        const temp = [];
        for (const element of sources) {
            temp.push(Number(element));
        }
        if (cateId == '0') {
            const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions  where pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND \`source_id\` in (${temp})  ORDER BY id DESC LIMIT 10`);
            res.send(rows);
        } else {
            const newsCount = await query(`SELECT COUNT(id) as count FROM opinions  WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp}) AND \`category_id\`=${cateId}`);
            const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions  WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp}) AND \`category_id\`=${cateId}  ORDER BY id DESC LIMIT 10`);
            res.send({ rows, newsCount });

        }

    } catch (err) {
        console.log(err);
    }
});

router.post('/get_catOpinionDatedMore', async (req, res) => {
    try {
        const { cateId, sources, lastId, startDate, endDate } = req.body;
        const temp = [];
        for (const element of sources) {
            temp.push(Number(element));
        }
        if (cateId == '0') {
            const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions  where pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND \`source_id\` in (${temp}) AND id < ${lastId}  ORDER BY id DESC LIMIT 10`);
            res.send(rows);
        } else {
            const rows = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions  WHERE pub_date BETWEEN "${startDate} 00:00:00" AND "${endDate} 00:00:00" AND source_id in (${temp}) AND \`category_id\`=${cateId} AND id < ${lastId}  ORDER BY id DESC LIMIT 10`);
            res.send(rows);
        }

    } catch (err) {
        console.log(err);
    }
});

router.get('/searchOpinions/:word', async (req, res) => {

    try {
        const news = await query(`SELECT id,title FROM opinions WHERE title LIKE '%${req.params.word}%' ORDER BY opinions.id  DESC LIMIT 20`);
        res.send({ Success: true, news });


    } catch (err) {
        console.log(err);
    }
});

router.get('/detail-searchOpinions/:word', async (req, res) => {
    console.log('called');
    try {
        const opinions = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions WHERE title LIKE '%${req.params.word}%' ORDER BY opinions.id  DESC LIMIT 20`);
        res.send(opinions);

    } catch (err) {
        console.log(err);
    }
});
router.get('/get_opinionAuthors/:lang', async(req, res) => {
    try {

        const authors = await query(`SELECT DISTINCT(author) as name FROM opinions WHERE lang='${req.params.lang}'`);
        res.send(authors);

    } catch (error) {
        res.send(error);
    }

});
router.post('/opinionAuthor', async (req, res) => {
    try {
        const { authors } = req.body;
        const authorsArr = [];
        for (const author of authors) {
            authorsArr.push(JSON.stringify(author.name));
        }

        const opinions = await query(`SELECT opinions.id, opinions.title, opinions.slug,opinions.description, opinions.media, opinions.views, opinions.hot, opinions.cold, opinions.source_id, opinions.author_id, opinions.author, opinions.pub_date ,opinions.source, opinions.lang, opinions.source_slug as sources_slug, opinions.country as country from opinions WHERE author in (${authorsArr})`);
        res.send(opinions);
    } catch (error) {
        console.log(error);
    }
});

module.exports = router;
