const express = require('express'),
    router = express.Router();
const { Source } = require('../controllers');


router.post('/fetch', async (req, res) => {
    const sources  = await Source.getSources(req.body);
    res.json(sources);
});

module.exports = router;
