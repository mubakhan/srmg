const express = require('express'),
    cheerio = require('cheerio'),
    router = express.Router();
const query = require('../factory/query');
const { concat } = require('lodash');


const sum = require('sum');
const { TextAnalyticsClient, AzureKeyCredential } = require("@azure/ai-text-analytics");
const key = '759b4f4c9d9645c6acb8369227e6ced9';
const endpoint = 'https://mubashir-text.cognitiveservices.azure.com/';
const textAnalyticsClient = new TextAnalyticsClient(endpoint, new AzureKeyCredential(key));

router.get('/', isLoggedIn, (req, res) => {
    res.render('./focus/index.html')
});
router.get('/login', (req, res) => {
    res.render('./focus/login')
});
router.get('/getCount/:start/News/:end', async(req, res) => {
    let { start, end } = req.params;
    try {

        let enUrNewsCount = await query(`SELECT SUM(CASE WHEN source.lang='english' And source.id=news.source_id THEN 1 else 0 END) as eng_news,
                                        SUM(CASE WHEN source.lang='urdu' And source.id=news.source_id THEN 1 else 0 END) as urdu_news
                                        FROM news,source WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        let enUrSourcesCount = await query(`SELECT SUM(CASE WHEN source.lang='english'THEN 1 else 0 END) as eng_source, SUM(CASE WHEN source.lang='urdu' THEN 1 else 0 END) as urdu_source FROM source`);

        let cateCount = await query(`SELECT SUM(CASE WHEN news.category_id = 1 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_national,SUM(CASE WHEN news.category_id = 2 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_world,SUM(CASE WHEN news.category_id = 3 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_sports,SUM(CASE WHEN news.category_id = 4 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_business,SUM(CASE WHEN news.category_id = 5 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_lifeandstyle,SUM(CASE WHEN news.category_id = 6 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_health,SUM(CASE WHEN news.category_id = 7 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_technology,SUM(CASE WHEN news.category_id = 8 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_other,SUM(CASE WHEN news.category_id = 1 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_national,SUM(CASE WHEN news.category_id = 2 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_world,SUM(CASE WHEN news.category_id = 3 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_sports,SUM(CASE WHEN news.category_id = 4 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_business,SUM(CASE WHEN news.category_id = 5 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_lifeandstyle,SUM(CASE WHEN news.category_id = 6 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_health, SUM(CASE WHEN news.category_id = 7 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_technology,SUM(CASE WHEN news.category_id = 8 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_other FROM  news JOIN source WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        let sentiCount = await query(`SELECT SUM(CASE WHEN news.sentiment_state='positive'THEN 1 else 0 END) as sent_pos,
                                    SUM(CASE WHEN news.sentiment_state='neutral'THEN 1 else 0 END) as sent_neu,
                                    SUM(CASE WHEN news.sentiment_state='negative'THEN 1 else 0 END) as sent_neg
                                    FROM news WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        let deepCount = await query(`SELECT SUM(CASE WHEN news.deep_analyze_state='toxicity'THEN 1 else 0 END) as toxicity, 
                                        SUM(CASE WHEN news.deep_analyze_state='threat'THEN 1 else 0 END) as threat, 
                                        SUM(CASE WHEN news.deep_analyze_state='severe toxicity'THEN 1 else 0 END) as severe_toxicity, 
                                        SUM(CASE WHEN news.deep_analyze_state='insult'THEN 1 else 0 END) as insult, 
                                        SUM(CASE WHEN news.deep_analyze_state='sexual'THEN 1 else 0 END) as sexual, 
                                        SUM(CASE WHEN news.deep_analyze_state='flirt'THEN 1 else 0 END) as flirt, 
                                        SUM(CASE WHEN news.deep_analyze_state='insult'THEN 1 else 0 END) as profanity, 
                                        SUM(CASE WHEN news.deep_analyze_state='identity'THEN 1 else 0 END) as identity 
                                        FROM news WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`)

        res.send({ enUrNewsCount, enUrSourcesCount, cateCount, sentiCount, deepCount });
    } catch (error) {
        res.send({ error });
    }

});
router.get("/getKeywordCat", async(req, res) => {
    try {

        let categories = await query(`SELECT * FROM keyword_category`);
        res.send({ categories });
    } catch (error) {
        res.send({ error });

    }

})
router.get("/getKeyword/all", async(req, res) => {
    try {
        let keywords = await query(`SELECT * FROM keywords`);
        res.send({ keywords });
    } catch (error) {
        res.send({ error });

    }

})
// get keyword related to selected sources
router.post("/getKeywordByCat", async(req, res) => {
    let {country, souces} = req.body;
    try {
        let temp = [];
        for (let element of souces) {
            temp.push(Number(element));
        }
        let regionKeywordCount = [];
            for (const region of temp) {
               let obj = {};
                let keycount = await query(`SELECT keywords.id AS keyid, keywords.word AS keyword , (SELECT COUNT(keyword_news.keyword_id)) AS Count FROM keyword_news  INNER JOIN keywords ON keywords.id = keyword_news.keyword_id INNER JOIN news ON news.id = keyword_news.news_id WHERE news.source_id IN (${region}) AND keywords.keyword_catId = 21 GROUP BY keyid, keyword`)
               
                if(keycount.length > 0){
                   for (const element of keycount) {
                    
                        // console.log(element);
                        // console.log(element.keyid)
                        obj.id = element.keyid;
                        obj.word = element.keyword;
                        obj.count = element.Count;
                        regionKeywordCount.push(obj);
                        // 
                   }
                       
                }
               
                
                // console.log(obj);
            }
            
var outputArray = [];

 
regionKeywordCount.forEach(function(e) {
 if(!this[e.id]) {
   this[e.id] = { id: e.id, count:  0, word: e.word }
    outputArray.push(this[e.id]);
  }
 this[e.id].count += Number(e.count);
 this[e.id].word = e.word;
}, {});

console.log(outputArray)
// console.log(regionKeywordCount);

        res.send({ outputArray });
    } catch (error) {
        res.send({ error });

    }

})
router.get("/getNew/:start/:end/byLang/:lang", async(req, res) => {
    let { start, end, lang } = req.params;
    try {

        let news = await query(`SELECT news.id, news.slug ,news.title, news.description, news.media, news.pub_date,source.source FROM news,source WHERE source.lang='${lang}' And source.id=news.source_id AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});

router.get("/getSourcesByLang/:lang", async(req, res) => {
    let { lang } = req.params;
    try {

        let sources = await query(`SELECT * FROM source where lang = '${lang}'`);
        res.send({ sources });
    } catch (error) {
        res.send({ error });

    }

})

router.get("/getSourcesByLang/:lang", async(req, res) => {
    let { lang } = req.params;
    try {

        let sources = await query(`SELECT * FROM source where lang = '${lang}'`);
        res.send({ sources });
    } catch (error) {
        res.send({ error });

    }

})

router.get("/getcatNews/:start/:end/byLang/:lang/:cat", async(req, res) => {
    let { start, end, lang, cat } = req.params;
    try {

        let news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,
    news.pub_date, news.tags, s.source AS source, c.category
    FROM news
    INNER JOIN source s ON news.source_id = s.id
    INNER JOIN category c ON news.category_id = c.id
    WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')
    AND c.slug = '${cat}' AND s.lang='${lang}'`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});

router.get("/getSentiNews/:start/:end/byState/:state", async(req, res) => {
    let { start, end, state } = req.params;
    try {

        let news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,news.pub_date ,news.sentiment_state
    FROM news WHERE news.sentiment_state='${state}' AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});
router.get("/getDeepNews/:start/:end/byState/:state", async(req, res) => {
    let { start, end, state } = req.params;
    try {

        let news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,news.pub_date ,news.deep_analyze_state FROM news WHERE news.deep_analyze_state='${state}' AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }
});
router.post('/get_regions', async(req, res) => {


    let keywords = req.body.keywords;
    if (keywords.length > 0) {
        let temp = [];
        for (let element of keywords) {
            temp.push(Number(element));
        }
        try {
            const regions = await query('SELECT * FROM `regions`');
            let regionKeywordCount = [];
            for (const region of regions) {
                let obj = {};
                const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.region_id =${region.id} and kn.keyword_id in (${temp})`);
                obj.id = region.id;
                obj.name = region.name;
                obj.description = region.description;
                obj.count = Count[0].cunt;
                regionKeywordCount.push(obj);
            }
            // const asiaCount = await query(`SELECT COUNT(*) as asiaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.region_id =1 and kn.keyword_id in (${temp}))`);
            res.send(regionKeywordCount);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query('SELECT * FROM `regions`');
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }


})
router.post('/get_countries', async(req, res) => {


    let keywords = req.body.keywords;
    if (keywords.length > 0) {
        let temp = [];
        for (let element of keywords) {
            temp.push(Number(element));
        }
        try {
            const countries = await query('SELECT * FROM `country` WHERE `region_id` = ' + req.body.regionId + '');

            let countryKeywordCount = [];
            for (const country of countries) {
                let obj = {};
                const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.id =${country.id} and kn.keyword_id in (${temp})`);
                obj.id = country.id;
                obj.name = country.name;
                obj.count = Count[0].cunt;
                countryKeywordCount.push(obj);
            }
            res.send(countryKeywordCount);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query('SELECT * FROM `country` WHERE `region_id` = ' + req.body.regionId + '');
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }



});
router.post('/get_multiCountries', async(req, res) => {


    // let regions = req.body.regions;

    try {
        // let temp = [];
        // for (let element of regions) {
        //     temp.push(Number(element));
        // }
        const rows = await query('SELECT * FROM `country` WHERE `sub_reg` in ("m_east")');
       
        // const rows = await query('SELECT * FROM `country` WHERE `region_id` in (1)');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }

// search news
router.get('/searchNews/:word',async (req,res)=>{
    
    try {
        const news = await query(`SELECT id,title FROM news WHERE title LIKE '%${req.params.word}%'  ORDER BY news.id  DESC LIMIT 20`);
        res.send({ "Success": true, news });


    } catch (err) {
        res.send(err);
    }
});

// get detail news on search
router.get('/detail-searchNews/:word',async (req,res)=>{
    let pageNumber = Number(1);
    pageNumber = pageNumber * 10;
    // let {sourceIds,source} = req.body;
    // let sourcesarr = [];
    // if(sourceIds.length > 0){
    //     for (let element of sourceIds) {
    //         sourcesarr.push(Number(element));
    //     }
    // }
    // if(source.length > 0){
    //     for (let element of source) {
    //         sourcesarr.push(Number(element));
    //     }
    // }
    try {
        let rows = [];
        console.log("search");
            const news = await query(`SELECT * FROM news WHERE title like '%${req.params.word}%'  order by id desc LIMIT 500`)
             // const news = await query(`select n.id,n.title,n.slug,n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n  WHERE title LIKE '%${req.params.word}%' ORDER BY n.id  DESC`);
            const newscount = await query(`select COUNT(news.id) AS newscount from news where title LIKE '%${req.params.word}%'`)
            
        news.forEach(val => {
                
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source
                    });
                    
            });
        
        var Elements = rows;
        var lastPages = Elements.slice(0, pageNumber);
        var newElem = lastPages;

        var lastRecords = Elements;
        let numberOfNews = newscount
       
        
        res.send({ lastRecords, numberOfNews });

    } catch (error) {
        res.send({ error });

    }
    // try {
    //     const news = await query(`select n.id,n.title,n.slug,n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n  WHERE title LIKE '%${req.params.word}%' ORDER BY n.id  DESC`);
    //     const newscount = await query(`select sum(news.id) AS newscount from news where title LIKE '%${req.params.word}%'`)
    //     console.log(newscount)
    //     res.send( {news, newscount} );


    // } catch (err) {
    //     res.send(err);
    // }
});

// get detail news on search
router.get('/detail-GsearchNews/:word',async (req,res)=>{
    
    try {
        const news = await query(`select n.id,n.title,n.slug,n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n  WHERE title LIKE '%${req.params.word}%' ORDER BY n.id  DESC`);
        // console.log(news);
        res.send( news );


    } catch (err) {
        res.send(err);
    }
});


});
router.post('/get_multisources', async(req, res) => {
    let countries = req.body.countries;

    let temp = [];
    for (let element of countries) {
        temp.push(Number(element));
    }
    try {
        const rows = await query('SELECT source.id AS sourceID, source.source AS sourcename FROM source INNER JOIN news ON news.source_id = source.id INNER JOIN keyword_news ON keyword_news.news_id = news.id WHERE source.lang= "english" AND source.country_id in ('+temp+')  GROUP BY source.id');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }



});
router.post('/get_sources', async(req, res) => {
    let keywords = req.body.keywords;
    if (keywords.length > 0) {
        let temp = [];
        for (let element of keywords) {
            temp.push(Number(element));
        }
        try {
            const sources = await query('SELECT * FROM `source` WHERE `country_id` = ' + req.body.countryId + '');

            let sourceKeywordCount = [];
            for (const source of sources) {
                let obj = {};
                const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE  source.id =${source.id} and kn.keyword_id in (${temp})`);
                obj.id = source.id;
                obj.source = source.source;
                obj.slug = source.slug;
                obj.website = source.website;
                obj.lang = source.lang;
                obj.count = Count[0].cunt;
                sourceKeywordCount.push(obj);
            }
            res.send(sourceKeywordCount);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query('SELECT * FROM `source` WHERE `country_id` = ' + req.body.countryId + '');
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }


});

// count all news
router.get('/getCount', async(req, res) => {
    try {
            //
        let enUrNewsCount = await query(`SELECT SUM(CASE WHEN news.lang='english' THEN 1 else 0 END) as engCount, SUM(CASE WHEN news.lang='urdu' THEN 1 else 0 END) as urduCount, SUM(CASE WHEN news.lang='pushto' THEN 1 else 0 END) as pushtoCount, SUM(CASE WHEN news.lang='arabic' THEN 1 else 0 END) as arabicCount  FROM news`);

        let asiaNewsCount = await query(`SELECT COUNT(*) as asiaCount FROM news INNER JOIN country ON news.country=country.name WHERE country.sub_reg ='m_east'`);
        let europeNewsCount = await query(`SELECT COUNT(*) as europeCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =2`);
        let africaNewsCount = await query(`SELECT COUNT(*) as africaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =3`);
        let americaNewsCount = await query(`SELECT COUNT(*) as americaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =4`);

        let totalNewsCount = await query(`SELECT COUNT(*) as totalCount FROM news INNER JOIN country ON news.country=country.name INNER JOIN source ON news.source_id=source.id WHERE source.lang = 'english' AND country.sub_reg ='m_east'`)
        let sourcesCount = await query(`SELECT count(*) as sourceCount FROM source INNER JOIN country ON source.country_id = country.id where country.sub_reg = 'm_east' AND lang='english'`);
        let countriesCount = await query(`SELECT count(*) as countryCount FROM country where country.sub_reg='m_east'`);

        res.send({ enUrNewsCount, asiaNewsCount, europeNewsCount, africaNewsCount, americaNewsCount, totalNewsCount, sourcesCount, countriesCount });
    } catch (error) {
        res.send({ error });
    }

});

// date select in general tab without key
router.post("/getNewsBySource", async(req, res) => {


    let { start, end, sourceId } = req.body;
    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    let sources = [];
    for (let element of sourceId) {
        sources.push(Number(element));
    }
    // console.log(sources)
        try {
            let rows = [];
            var lastRecords;
            let count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') `);
           
           let news = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id asc `);
           if(news.length > 0){
            
            
            news.forEach(val => {
                console.log(val.id);
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source
                    });
                }
            });
        
            lastRecords = rows;

        // var lastPages = Elements.slice(0, pageNumber);

        // var newElem = lastPages;

        //  lastRecords = newElem.slice(-10);
           }else{
                lastRecords = 0
           }
        let numberOfNews;
        // if(count.length > 0){
            numberOfNews = count
        // }
        
        
        res.send({ lastRecords, numberOfNews });

            // res.send({ news });
        } catch (error) {
            res.send({ error });

        }
    




});

router.post("/getNewsBySourcekey", async(req, res) => {


    let { start, end, sourceId ,keyword} = req.body;
    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    let sources = [];
    let temp = [];
    for (let element of keyword) {
        if(element != '0'){
            
            temp.push(Number(element));
        }
    }
    for (let element of sourceId) {
        sources.push(Number(element));
    }
    console.log(sources)
        try {
            let rows = [];
            // let news = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id desc LIMIT 100 OFFSET 0`);
           
            let count;
            //  = await query(`SELECT DISTINCT(news.id) FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE source.id in (${sources}) order by id desc`);
            if(temp.length > 0){
                console.log("news");
            let dayIndex = 0;
            
            console.log("with keyword")
                
                let news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id desc `);
                 count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
            
                
                news.forEach(val => {
                    if (rows.some(person => person.id === val.id)) {
                        dayIndex = rows.map(e => e.id).indexOf(val.id);
                        rows[dayIndex].keyword.push(val.keyword);
                    } else {
                        rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            keyword: [val.keyword]
                        });
                    }
                });
            }else{   
                console.log("news n ");
                let newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id   WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id desc`);   
                count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.sub_reg = 'm_east' AND source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
                if(newsn.length > 0){
                newsn.forEach(val => {
                    if (rows.some(person => person.id === val.id)) {
                        dayIndex = rows.map(e => e.id).indexOf(val.id);
                        rows[dayIndex].keyword.push(val.keyword);
                    } else {
                        rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            source: val.source,
                            keyword: [val.keyword]
                        });
                    }
                });
            }
            }
            // var Elements = rows;
    
            // var lastPages = Elements.slice(0, pageNumber);
    
            // var newElem = lastPages;
    
            // var lastRecords = newElem.slice(-10);
            var lastRecords = rows;
            let numberOfNews;
            // if(count.length > 0){
                numberOfNews = count
            // }
            
            
            res.send({ lastRecords, numberOfNews });

            // res.send({ news });
        } catch (error) {
            res.send({ error });

        }
    
});

router.post("/getGeneralNewsMulti/bySource/", async(req, res) => {
    let { start, end, sourceIds, keywords,lastId } = req.body;
    
    let temp = [];
    for (let element of keywords) {
        if(element != '0'){
            temp.push(Number(element));
            
        }
    }
    let sources = [];
    for (let element of sourceIds) {
        sources.push(Number(element));
    }

    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    
            if(lastId != null){
                console.log(lastId)
                try {
                    let rows = [];
                    let pageID = Number(req.body.pageId);
                    pageID = (pageID-1);
                    pageID = pageID * 10;
                    console.log("Inside last page id");
                    console.log(sources);
                    let count
                    
                    if(temp.length > 0){
                        let elemntCateogery = [];
                        let dayIndex = 0;
                        
                        // if(empty(news)){
                            
                            let news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) AND news.id > ${lastId} order by id asc LIMIT 10`);  
                            count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) `);

                            news.forEach(val => {
                                if (rows.some(person => person.id === val.id)) {
                                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                                    rows[dayIndex].keyword.push(val.keyword);
                                } else {
                                    rows.push({
                                        id: val.id,
                                        title: val.title,
                                        media: val.media,
                                        link: val.url,
                                        body: val.body,
                                        description: val.description,
                                        pub_date: val.pub_date,
                                        source: val.source,
                                        keyword: [val.keyword]
                                    });
                                }
                            });
                        }else{   
                            let newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE source.id in (${sources})  order by id asc  LIMIT 100 OFFSET ${pageID}`);  
                            count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.sub_reg = 'm_east' AND source.id in (${sources})`);
                            newsn.forEach(val => {
                                console.log(val.id)
                                if (rows.some(person => person.id === val.id)) {
                                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                                    rows[dayIndex].keyword.push(val.keyword);
                                } else {
                                    
                                    rows.push({
                                        id: val.id,
                                        title: val.title,
                                        media: val.media,
                                        link: val.url,
                                        body: val.body,
                                        description: val.description,
                                        pub_date: val.pub_date,
                                        source: val.source
                                    });
                                }
                            });
                        }
                        var Elements = rows;
                        var lastPages = Elements.slice(0, pageNumber);
                
                        var newElem = lastPages;
                      
                        var lastRecords = newElem.slice(-10);
                        let numberOfNews;
                        // if(count.length > 0){
                        // }
                        numberOfNews = count
                          console.log(lastRecords)
                        
                        res.send({ lastRecords, numberOfNews });
            
                } catch (error) {
                    res.send({ error });
            
                }
    }else{
        try {
            let rows = [];
            // let countnotkey;
            let count;
            if(temp.length > 0){
            let elemntCateogery = [];
            let dayIndex = 0;
            
            // if(empty(news)){
                
                let news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) order by id asc LIMIT 100 OFFSET 0`);
                 count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id  INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id WHERE source.id in (${sources}) and kn.keyword_id IN (${temp})`);
            
                news.forEach(val => {
                    if (rows.some(person => person.id === val.id)) {
                        dayIndex = rows.map(e => e.id).indexOf(val.id);
                        rows[dayIndex].keyword.push(val.keyword);
                    } else {
                        rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            source: val.source,
                            keyword: [val.keyword]
                        });
                    }
                });
            }else{   
                let newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id   WHERE source.id in (${sources}) order by id asc  LIMIT 100 OFFSET 0`);   
                count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.sub_reg = 'm_east' AND source.id in (${sources})`);
            
                newsn.forEach(val => {
                    if (rows.some(person => person.id === val.id)) {
                        dayIndex = rows.map(e => e.id).indexOf(val.id);
                        rows[dayIndex].keyword.push(val.keyword);
                    } else {
                        rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            source: val.source
                        });
                    }
                });
            }
            var Elements = rows;
    
            var lastPages = Elements.slice(0, pageNumber);
    
            var newElem = lastPages;
    
            var lastRecords = newElem.slice(-10);
            let numberOfNews;
            // if(count.length > 0){
                numberOfNews = count
            // }
            
            
            res.send({ lastRecords, numberOfNews });
    
        } catch (error) {
            res.send({ error });
    
        }
    }
});

router.post("/getDashNews/byKeywordCate", async(req, res) => {
    let { start, end, sourceId, catId, keywords } = req.body;
    if (keywords.length > 0) {
        console.log(sourceId + ' hi ' + keywords + " sdsf " + catId)
        let temp = [];
        for (let element of keywords) {
            temp.push(Number(element));
        }

        try {
            let rows = [];
            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id =${sourceId} and kn.keyword_id in (${temp}) and kc.id=${catId} AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

            let elemntCateogery = [];
            let dayIndex = 0;
            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        keyword: [val.keyword]
                    });
                }
            });
            console.log(rows)
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }

    } else {
        try {
            const rows = await query('select n.id, n.title, n.slug, n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n WHERE n.pub_date BETWEEN "' + start + ' 00:00:00" AND "' + end + ' 00:00:00" AND n.source_id =' + sourceId + ' AND n.category_id =' + catId + ' ORDER BY n.id DESC');
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }
    }


});
router.get('/test', async(req, res) => {
    let newArr = [];
    const news = await query(`SELECT news.*,kc.name as keywordCat FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE kn.keyword_id in (1,2)`);
    let elemntCateogery = [];
    let dayIndex = 0;
    news.forEach(val => {
        if (newArr.some(person => person.id === val.id)) {
            dayIndex = newArr.map(e => e.id).indexOf(val.id);
            newArr[dayIndex].cate.push(val.keywordCat);
        } else {
            newArr.push({
                id: val.id,
                title: val.title,
                media: val.media,
                link: val.link,
                body: val.body,
                cate: [val.keywordCat]
            });
        }
    });


    res.send(newArr);
});


router.get("/pagenew/:pageId", async(req, res) => {
    let pageNumber = Number(req.params.pageId);
    pageNumber = pageNumber * 10;
    let getNews = await query(`select * from news order by id desc limit ${pageNumber}`);
    var Elements = getNews;

    var last4 = Elements.slice(-10)

    res.send(last4)

});

// news summary
router.get('/textSummary/:id', async (req, res) => {
    const news = await query(`select * from news where id = ${req.params.id}`);
    let body = news[0].body;
    let bodyHtml = cheerio.load(body);
    bodyHtml('img').remove();
    let desc = bodyHtml.text();
    desc = desc.replace(/<[^>]*>?/gm, '');
 
    try {
        let abstract = sum({ 'corpus': desc });
        let summary = abstract.summary;
        res.send({summary})
    } catch (error) {
        console.log(error);
    }
})

// Sentiment analysis
router.get('/textAna/:id', async (req, res) => {

    try {

        const news = await query(`select * from news where id = ${req.params.id}`);
        let body = news[0].body;
        let bodyHtml = cheerio.load(body);
        bodyHtml('img').remove();
        let desc = bodyHtml.text();
        desc = desc.replace(/<[^>]*>?/gm, '');
        descTemp = desc.substr(0, 5100)
        let temp = [];
        temp.push(descTemp)
        const sentimentInput = temp;
        const sentimentResult = await textAnalyticsClient.analyzeSentiment(sentimentInput);
        console.log(sentimentResult)
        res.send({ sentimentResult });

    } catch (error) {
        console.log(error);
    }

});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/focus/login');
}