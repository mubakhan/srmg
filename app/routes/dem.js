const express = require('express'),
    cheerio = require('cheerio'),
    router = express.Router();
const query = require('../factory/query');
const { concat } = require('lodash');

const sum = require('sum');
const { TextAnalyticsClient, AzureKeyCredential } = require('@azure/ai-text-analytics');
const key = '759b4f4c9d9645c6acb8369227e6ced9';
const endpoint = 'https://mubashir-text.cognitiveservices.azure.com/';
const textAnalyticsClient = new TextAnalyticsClient(endpoint, new AzureKeyCredential(key));

// index page of dem
router.get('/', isLoggedIn, (req, res) => {
    res.render('./dem/index.html');
});

// login page
router.get('/login', (req, res) => {
    res.render('./dem/login');
});

// count all news
router.get('/getCount', async(req, res) => {
    try {
        const totalNewsCount = await query('SELECT COUNT(*) as totalCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.id =1');
        const sourcesEngCount = await query('SELECT count(*) as engSourceCount FROM source where country_id=1');
        res.send({ totalNewsCount, sourcesEngCount });
    } catch (error) {
        res.send({ error });
    }

});


// sources form country pakistan
router.post('/get_sources', async(req, res) => {
    try {
        const rows = await query('SELECT * FROM `source` WHERE `country_id` = 1');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

// news search by source ids and lastid
router.post('/getdamNews/bySource/', async(req, res) => {
    const {  start, end,  sourceIds, lastId } = req.body;


    const sources = [];
    for (const element of sourceIds) {
        sources.push(Number(element));
    }

    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    if (lastId != null) {
        console.log(lastId);
        try {
            const rows = [];
            const pageID = Number(req.body.pageId);

            const news = await query(`SELECT news.* FROM news INNER JOIN source AS s ON news.source_id=s.id WHERE s.id in (${sources}) AND news.id < ${lastId} order by news.id DESC LIMIT 10`);
            const count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources})`);

            const elemntCateogery = [];
            const dayIndex = 0;
            news.forEach(val => {
                rows.push({
                    id: val.id,
                    title: val.title,
                    media: val.media,
                    link: val.url,
                    body: val.body,
                    description: val.description,
                    pub_date: val.pub_date,
                    source: val.source
                });
            });

            var Elements = rows;

            var lastPages = await  Elements.slice(0, pageNumber);

            var newElem = lastPages;

            var lastRecords = await newElem.slice(-10);
            console.log(count.length);
            const numberOfNews = count;
            res.send({ lastRecords, numberOfNews });

        } catch (error) {
            res.send({ error });

        }
    } else {

        try {
            const rows = [];

            const news = await query(`SELECT news.* FROM news INNER JOIN source AS s ON news.source_id=s.id WHERE s.id in (${sources}) AND  (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by news.id DESC LIMIT 10 OFFSET 0`);
            const count = await query(`SELECT count(news.id) as countNews  FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND  (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by news.id asc`);


            const elemntCateogery = [];
            const dayIndex = 0;
            news.forEach(val => {

                rows.push({
                    id: val.id,
                    title: val.title,
                    media: val.media,
                    link: val.url,
                    body: val.body,
                    description: val.description,
                    pub_date: val.pub_date,
                    source: val.source
                });
            });

            var Elements = rows;

            var lastPages = await  Elements.slice(0, pageNumber);

            var newElem = lastPages;

            var lastRecords = await newElem.slice(-10);

            const numberOfNews = count;
            res.send({ lastRecords, numberOfNews });

        } catch (error) {
            res.send({ error });

        }
    }
});

// search news
router.post('/searchNews/', async (req, res)=>{
    const { sourceIds, word } = req.body;
    console.log(sourceIds);
    console.log(word);
    const sources = [];
    for (const element of sourceIds) {
        sources.push(Number(element));
    }
    try {
        const news = await query(`SELECT id,title FROM news WHERE title LIKE '%${word}%' AND news.source_id IN (${sources})  ORDER BY news.id  DESC LIMIT 20`);
        res.send({ Success: true, news });


    } catch (err) {
        res.send(err);
    }
});

// get detail news on search
router.post('/detail-searchNews/', async (req, res)=>{
    const { word, sourceIds } = req.body;
    const sources = [];
    for (const element of sourceIds) {
        sources.push(Number(element));
    }
    let pageNumber = Number(1);
    pageNumber = pageNumber * 10;
    try {
        const rows = [];
        console.log('search');
        const news = await query(`SELECT * FROM news WHERE title like '%${word}%' AND news.source_id IN (${sources})  order by id desc LIMIT 500`);
        const newscount = await query(`select COUNT(news.id) AS newscount from news where title LIKE '%${word}%' AND news.source_id IN (${sources})`);

        news.forEach(val => {

            rows.push({
                id: val.id,
                title: val.title,
                media: val.media,
                link: val.url,
                body: val.body,
                description: val.description,
                pub_date: val.pub_date,
                source: val.source
            });

        });

        const Elements = rows;
        const lastPages = Elements.slice(0, pageNumber);
        const newElem = lastPages;

        const lastRecords = Elements;
        const numberOfNews = newscount;


        res.send({ lastRecords, numberOfNews });

    } catch (error) {
        res.send({ error });

    }
});


// news summary
router.get('/textSummary/:id', async (req, res) => {
    const news = await query(`select * from news where id = ${req.params.id}`);
    const { body } = news[0];
    const bodyHtml = cheerio.load(body);
    bodyHtml('img').remove();
    let desc = bodyHtml.text();
    desc = desc.replace(/<[^>]*>?/gm, '');

    try {
        const abstract = sum({ corpus: desc });
        const { summary } = abstract;
        res.send({ summary });
    } catch (error) {
        console.log(error);
    }
});

// Sentiment analysis
router.get('/textAna/:id', async (req, res) => {

    try {

        const news = await query(`select * from news where id = ${req.params.id}`);
        const { body } = news[0];
        const bodyHtml = cheerio.load(body);
        bodyHtml('img').remove();
        let desc = bodyHtml.text();
        desc = desc.replace(/<[^>]*>?/gm, '');
        descTemp = desc.substr(0, 5100);
        const temp = [];
        temp.push(descTemp);
        const sentimentInput = temp;
        const sentimentResult = await textAnalyticsClient.analyzeSentiment(sentimentInput);

        res.send({ sentimentResult });

    } catch (error) {
        console.log(error);
    }

});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/dem/login');
}
