const express = require('express'),
    adminRouter = require('./admin/scraper'),
    opnionScraperRouter = require('./admin/opnionScraper'),
    newsRouter = require('./news'),
    hotColdRouter = require('./hotcold'),
    dashboardRouter = require('./dashboard'),
    focusRouter = require('./focus'),
    finalRouter = require('./final'),
    demrouter = require('./dem'),
    introuter = require('./int'),
    opinionRouter = require('./opinion'),
    irfanRouter = require('./irfan'),
    userRouter = require('./user'),
    testRouter = require('./admin/test'),
    mobileRouter = require('./mobile/mobile'),
    textAnalysisRouter = require('./textAnalysis'),
    INTrouter = require('./dashboards/international'),
    DOMrouter = require('./dashboards/domestic'),
    sourceRouter = require('./source'),
    categoryRouter = require('./category'),
    countryRouter = require('./country'),
    regionRouter = require('./region'),
    router = express.Router();

router.get('/', (req, res) => {
    res.render('./newshunt/index.html');
});
router.get('/admin', isLoggedIn, (req, res) => {
    const pageData = {
        title: 'Sources',
        user: req.user
    };
    res.render('index', {pageData});
});

router.get('/admin/login', (req, res) => {
    res.render('admin/login');
});

router.use('/international', INTrouter);
router.use('/domestic', DOMrouter);

router.post('/contact_us', async(req, res) => {
    const sgMail = require('@sendgrid/mail');
    const sendgridAPIKey = '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';
    sgMail.setApiKey(sendgridAPIKey);
    const msg = {
        to: 'info@newshunt.io',
        from: req.body.email,
        subject: req.body.subject,
        html: `<div><strong><label>From</label></strong> : <label>${req.body.name}</label></div>
            <div><strong><label>Email</label>   : <label>${req.body.email}</label></strong></div>
            <div><strong><label>Subject</label> :</strong> <span> ${req.body.subject}</span></div>
            <div><strong><label>Message</label> :</strong> <span> ${req.body.message}</span></div>
            </div>`
    };
    try {
        sgMail.send(msg);
        res.send({ success: 'Email send.' });
    } catch (err) {
        res.send({ error: 'Email sending fail.' });

    }

});

router.use('/admin', adminRouter);
router.use('/dashboard', dashboardRouter);
router.use('/mobile', mobileRouter);
router.use('/focus', focusRouter);
router.use('/final', finalRouter);
router.use('/dem', demrouter);
router.use('/int', introuter);

router.use(textAnalysisRouter);
router.use(opnionScraperRouter);
router.use(opinionRouter);

router.use('/news', newsRouter);
router.use('/source', sourceRouter);
router.use('/country', countryRouter);
router.use('/category', categoryRouter);
router.use('/region', regionRouter);
router.use(userRouter);
router.use(irfanRouter);
router.use(hotColdRouter);
router.use(testRouter);


module.exports = router;
function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/admin/login');
}
