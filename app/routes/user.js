const express = require('express'),
    passport = require('passport'),
    fs = require('fs'),
    router = express.Router();
const query = require('../factory/query');
const bcrypt = require('bcryptjs');
const saltRounds = 10;

router.post('/login', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.redirect('/login');
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.redirect('/dashboard/');
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});

// focus user login
router.post('/focuslogin', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.redirect('/login');
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.redirect('/focus/');
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});
// dem user login
router.post('/dem', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.redirect('/login');
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.redirect('/dem/');
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});

// int user login
router.post('/int', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.redirect('/login');
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.redirect('/int/');
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});

router.post('/create/user', async (req, res) => {
    try {
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = bcrypt.hashSync(req.body.password, salt);
        console.log(req);
        if (req.files && req.files.file) {
            const dir = './public/uploads/';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            const sampleFile = req.files.file;
            let imagename = `${dir + new Date() / 1000}${sampleFile.name}`;
            await sampleFile.mv(imagename, function (err) {
                if (err) return res.status(500).send(err);
            });

            imagename = imagename.substring(9);
            console.log(req.body.userId);
            const rows = await query(
                `INSERT INTO \`user\`(name,email,password,picture,oauth_provider) VALUES ('${
                    req.body.name
                }','${
                    req.body.email
                }','${
                    hash
                }','${
                    imagename
                }','newshunt.io')`
            );
            res.send({ sucess: 'true' });
        } else {
            const rows = await query(
                `INSERT INTO \`user\`(name,email,password,oauth_provider) VALUES ('${
                    req.body.name
                }','${
                    req.body.email
                }','${
                    hash
                }','newshunt.io')`
            );
            res.send({ sucess: 'true' });
        }
    } catch (err) {
        res.send({ sucess: 'false' });
    }
});
router.post('/admin/login', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.redirect('/login');
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.redirect('/admin');
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});
module.exports = router;
