const express = require('express'),
    router = express.Router();
const { Region } = require('../controllers');


router.post('/fetch', async (req, res) => {
    const sources  = await Region.getRegions(req.body);
    res.json(sources);
});

module.exports = router;
