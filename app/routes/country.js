const express = require('express'),
    router = express.Router();
const { Country } = require('../controllers');


router.post('/fetch', async (req, res) => {
    const sources  = await Country.getCountries(req.body);
    res.json(sources);
});
router.post('/fetchOpinion', async (req, res) => {
    const sources  = await Country.getOpinionCountries(req.body);
    res.json(sources);
});

module.exports = router;
