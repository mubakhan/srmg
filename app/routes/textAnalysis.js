const express = require('express'),
    axios = require('axios'),
    cheerio = require('cheerio'),
    router = express.Router();
const query = require('../factory/query');

const Promises = require('bluebird');

const { TextAnalyticsClient, AzureKeyCredential } = require("@azure/ai-text-analytics");
const { urlencoded } = require('body-parser');
const key = '759b4f4c9d9645c6acb8369227e6ced9';
const endpoint = 'https://mubashir-text.cognitiveservices.azure.com/';
const sum = require('sum');
const textAnalyticsClient = new TextAnalyticsClient(endpoint, new AzureKeyCredential(key));
const deepai = require('deepai'); // OR include deepai.min.js as a script tag in your HTML

deepai.setApiKey('36266dc7-3b2d-4f21-99bb-31ee338b04c7');


router.get('/textAna/:id', async (req, res) => {

    try {

        const news = await query(`select * from news where id = ${req.params.id}`);
        let body = news[0].body;
        let bodyHtml = cheerio.load(body);
        bodyHtml('img').remove();
        let desc = bodyHtml.text();
        desc = desc.replace(/<[^>]*>?/gm, '');
        descTemp = desc.substr(0, 5100)
        let temp = [];
        temp.push(descTemp)
        const sentimentInput = temp;
        const sentimentResult = await textAnalyticsClient.analyzeSentiment(sentimentInput);

        console.log(sentimentResult)
        // sentimentResult.forEach(document => {
        //     console.log(`ID: ${document.id}`);
        //     console.log(`\tDocument Sentiment: ${document.sentiment}`);
        //     console.log(`\tDocument Scores:`);
        //     console.log(`\t\tPositive: ${document.confidenceScores.positive.toFixed(2)} \tNegative: ${document.confidenceScores.negative.toFixed(2)} \tNeutral: ${document.confidenceScores.neutral.toFixed(2)}`);
        //     console.log(`\tSentences Sentiment(${document.sentences.length}):`);
        //     document.sentences.forEach(sentence => {
        //         console.log(`\t\tSentence sentiment: ${sentence.sentiment}`)
        //         console.log(`\t\tSentences Scores:`);
        //         console.log(`\t\tPositive: ${sentence.confidenceScores.positive.toFixed(2)} \tNegative: ${sentence.confidenceScores.negative.toFixed(2)} \tNeutral: ${sentence.confidenceScores.neutral.toFixed(2)}`);
        //     });
        res.send({ sentimentResult });

    } catch (error) {
        console.log(error);
    }

});

router.get('/textSummary/:id', async (req, res) => {
    const news = await query(`select * from news where id = ${req.params.id}`);
    let body = news[0].body;
    let bodyHtml = cheerio.load(body);
    bodyHtml('img').remove();
    let desc = bodyHtml.text();
    desc = desc.replace(/<[^>]*>?/gm, '');
    // try {
    //     var resp = await deepai.callStandardApi("summarization", {
    //         text: descTemp,
    //     });
    //         res.send(resp);

    // } catch (error) {
    //     console.log(error)
    // }
 
    try {
        let abstract = sum({ 'corpus': desc });
        let summary = abstract.summary;
        res.send({summary})
    } catch (error) {
        console.log(error);
    }
  

})






module.exports = router;
