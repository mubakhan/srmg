const express = require('express'),
    cheerio = require('cheerio'),
    router = express.Router();
const query = require('../factory/query');
const { concat } = require('lodash');

const sum = require('sum');
const { TextAnalyticsClient, AzureKeyCredential } = require("@azure/ai-text-analytics");
const key = '759b4f4c9d9645c6acb8369227e6ced9';
const endpoint = 'https://mubashir-text.cognitiveservices.azure.com/';
const textAnalyticsClient = new TextAnalyticsClient(endpoint, new AzureKeyCredential(key));

router.get('/', isLoggedIn, (req, res) => {
    res.render('./int/index.html')
});
router.get('/login', (req, res) => {
    res.render('./int/login')
});

// int country
router.post('/get_multiCountries', async(req, res) => {
    try {
        const rows = await query('SELECT * FROM `country` WHERE `reg_int` in ("int")');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
// count all news
router.get('/getCount', async(req, res) => {
    try {
        
        let totalNewsCount = await query(`SELECT COUNT(*) as totalCount FROM news INNER JOIN country ON news.country=country.name INNER JOIN source ON news.source_id=source.id WHERE country.reg_int='int'`)
        let sourcesCount = await query(`SELECT count(*) as sourceCount FROM source INNER JOIN country ON source.country_id = country.id where country.reg_int='int'`);
        let countriesCount = await query(`SELECT count(*) as countryCount FROM country where country.reg_int='int'`);

        res.send({ totalNewsCount, sourcesCount, countriesCount });
    } catch (error) {
        res.send({ error });
    }

});

// news from selected sources
router.post("/getGeneralNewsMulti/bySource/", async(req, res) => {
    let { sourceIds, lastId } = req.body;
    
    let sources = [];
    for (let element of sourceIds) {
        sources.push(Number(element));
    }

    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    
            if(lastId != null){
                try {
                    let rows = [];
                    let pageID = Number(req.body.pageId);
                    pageID = (pageID-1);
                    pageID = pageID * 10;
                      
                    let newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources})  order by id DESC  LIMIT 100 OFFSET ${pageID}`);  
                    let count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources})`);
                            newsn.forEach(val => {
                                    rows.push({
                                        id: val.id,
                                        title: val.title,
                                        media: val.media,
                                        link: val.url,
                                        body: val.body,
                                        description: val.description,
                                        pub_date: val.pub_date,
                                        source: val.source
                                    });
                            });
                        var Elements = rows;
                        var lastPages = Elements.slice(0, pageNumber);
                
                        var newElem = lastPages;
                      
                        var lastRecords = newElem.slice(-10);
                        let numberOfNews = count
                        res.send({ lastRecords, numberOfNews });
            
                } catch (error) {
                    res.send({ error });
            
                }
    }else{
        try {
            let rows = [];
            
            let newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) order by id DESC  LIMIT 100 OFFSET 0`);   
            let count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources})`);
            
                newsn.forEach(val => {
                   rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            source: val.source
                        });
                });
            var Elements = rows;
    
            var lastPages = Elements.slice(0, pageNumber);
    
            var newElem = lastPages;
    
            var lastRecords = newElem.slice(-10);
            let numberOfNews = count
            
            
            res.send({ lastRecords, numberOfNews });
    
        } catch (error) {
            res.send({ error });
    
        }
    }
});


// get sources for selected country
router.post('/get_multisources', async(req, res) => {
    let countries = req.body.countries;

    let temp = [];
    for (let element of countries) {
        temp.push(Number(element));
    }
    try {
        const rows = await query('SELECT source.id AS sourceID, source.source AS sourcename FROM source INNER JOIN news ON news.source_id = source.id WHERE source.country_id in ('+temp+')  GROUP BY source.id');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }



});


// search news
router.post('/searchNews/',async (req,res)=>{
    let { sourceIds, word } = req.body;
    let sources = [];
    for (let element of sourceIds) {
        sources.push(Number(element));
    }
    try {
        const news = await query(`SELECT id,title FROM news WHERE title LIKE '%${word}%' AND news.source_id IN (${sources})  ORDER BY news.id  DESC LIMIT 20`);
        res.send({ "Success": true, news });


    } catch (err) {
        res.send(err);
    }
});

// get detail news on search
router.post('/detail-searchNews/',async (req,res)=>{
    let{word, sourceIds} = req.body;
    let sources = [];
    for (let element of sourceIds) {
        sources.push(Number(element));
    }
    let pageNumber = Number(1);
    pageNumber = pageNumber * 10;
    try {
        let rows = [];
            const news = await query(`SELECT * FROM news WHERE title like '%${word}%' AND news.source_id IN (${sources})  order by id desc LIMIT 500`)
            const newscount = await query(`select COUNT(news.id) AS newscount from news where title LIKE '%${word}%' AND news.source_id IN (${sources})`)
            
        news.forEach(val => {
                
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source
                    });
                    
            });
        
        var Elements = rows;
        var lastPages = Elements.slice(0, pageNumber);
        var newElem = lastPages;

        var lastRecords = Elements;
        let numberOfNews = newscount
       
        
        res.send({ lastRecords, numberOfNews });

    } catch (error) {
        res.send({ error });

    }
});

// news summary
router.get('/textSummary/:id', async (req, res) => {
    const news = await query(`select * from news where id = ${req.params.id}`);
    let body = news[0].body;
    let bodyHtml = cheerio.load(body);
    bodyHtml('img').remove();
    let desc = bodyHtml.text();
    desc = desc.replace(/<[^>]*>?/gm, '');
 
    try {
        let abstract = sum({ 'corpus': desc });
        let summary = abstract.summary;
        res.send({summary})
    } catch (error) {
        console.log(error);
    }
})

// Sentiment analysis
router.get('/textAna/:id', async (req, res) => {

    try {

        const news = await query(`select * from news where id = ${req.params.id}`);
        let body = news[0].body;
        let bodyHtml = cheerio.load(body);
        bodyHtml('img').remove();
        let desc = bodyHtml.text();
        desc = desc.replace(/<[^>]*>?/gm, '');
        descTemp = desc.substr(0, 5100)
        let temp = [];
        temp.push(descTemp)
        const sentimentInput = temp;
        const sentimentResult = await textAnalyticsClient.analyzeSentiment(sentimentInput);

        res.send({ sentimentResult });

    } catch (error) {
        console.log(error);
    }

});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/int/login');
}