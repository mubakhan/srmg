const express = require('express'),
    axios = require('axios'),
    cheerio = require('cheerio'),
    router = express.Router();
const util = require('util');
const mysql = require('mysql');
const query = require('../../factory/query');
const Promises = require('bluebird');

let _ = require('lodash');

let qs = require('qs');

let fs = require('fs');
// const puppeteer = require('puppeteer');
const puppeteer = require('puppeteer-extra')

// Add stealth plugin and use defaults (all tricks to hide puppeteer usage)
const StealthPlugin = require('puppeteer-extra-plugin-stealth')();
StealthPlugin.onBrowser = () => {};
const AdblockerPlugin = require('puppeteer-extra-plugin-adblocker')
puppeteer.use(AdblockerPlugin())
puppeteer.use(StealthPlugin)

router.get('/test', async(req, res) => {

    res.render('test')

});

router.get('/test/getSourceUrls/:id/cate/:cateID', async(req, res) => {

    let sourceId = req.params.id;
    let categoryId = req.params.cateID;
    res.render('test-url-data', { sourceId, categoryId })
});

router.get('/test/meth-getSourceUrls/:id/cate/:cateID', async(req, res) => {

    let sourceId = req.params.id;
    let categoryId = req.params.cateID;
    res.render('test-url-data-mth', { sourceId, categoryId })
});
router.get('/test/getCategories/:id', async(req, res) => {

    let sourceId = req.params.id;

    res.render('test-source-cate', { sourceId })

});
router.get('/test/getSourceCategories/:id', async(req, res) => {

    let sourceId = req.params.id;
    try {
        let categories = await query(`SELECT category_id FROM source_url WHERE source_id = ${sourceId}`)
        let cateIds = []
        for (const category of categories) {
            cateIds.push(category.category_id);
        }
        let getCatBySource = await query(`SELECT * FROM category where id in (${cateIds})`)
        res.send(getCatBySource)

    } catch (error) {
        console.log(error)
    }
    // res.render('test-url-data',{sourceId})
});

router.get('/test/getSourceUrlsData/:id/cat/:catId', async(req, res) => {
    let news = [];
    let source_id = req.params.id;
    let categor_id = req.params.catId;

    try {
        const rows = await query(`SELECT * FROM source_url WHERE source_id =${source_id} AND category_id = ${categor_id}`);
        // const browser = await puppeteer.launch({ headless: false });
        let browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });

        const page = await browser.newPage();
        await page.setViewport({ width: 1300, height: 1000 });
        await page.setDefaultNavigationTimeout(0);

        for (const index of rows) {
            let response;
            try {
                await page.goto(index.url, { waitUntil: 'networkidle2' });
                await page.waitFor(3000);

            } catch (error) {
                console.log(error)
            }
            try {
                response = await page.evaluate(() => document.documentElement.outerHTML);

            } catch (error) {
                console.log(error)

            }
            let category_id = index.category_id;
            if (response) {
                const $ = cheerio.load(response, {
                    xmlMode: true
                });

                if (index.encoding == "xml") {
                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key + "Filter"] = dataPath[i].data_filter;

                    }
                    if (dataPath[1].data_param == "html") {
                        let newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            let link = $(elem).find(arr['url']).text();
                            news.push(link);

                        });

                    } else {
                        $(arr['*']).each(async(i, elem) => {
                            let singleNews = [];
                            let obj = {};
                            let title = $(elem).find(arr['title']).text();
                            title = title.trim();

                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.split(' ').join('_');
                            let body = $(elem).find(arr['body']).text();

                            // let body = $(elem).find('content\\:encoded').text();

                            let bodyHtml = cheerio.load(body);
                            let desc = bodyHtml.text();
                            desc = desc.replace(/<[^>]*>?/gm, '');

                            let description = desc.substring(0, 200);

                            let link = $(elem).find(arr['url']).text();
                            let media = $(elem).find(arr['media']).attr('url');
                            if (!media) {
                                media = $(elem).find(arr['media']).attr('src');
                                if (!media) {
                                    let newHtm = cheerio.load(body);
                                    media = newHtm('img').attr('src');

                                }

                            }


                            let pub_date = new Date();
                            let date = mysql.escape(pub_date);
                            date = date.substring(1, 20)

                            desc = mysql.escape(desc);
                            obj.title = mysql.escape(title);
                            obj.slug = mysql.escape(slug);
                            obj.body = mysql.escape(body);
                            obj.description = mysql.escape(description);
                            obj.media = mysql.escape(media);
                            obj.link = mysql.escape(link);
                            obj.category_id = mysql.escape(category_id);
                            obj.source_id = mysql.escape(source_id);
                            obj.date = mysql.escape(date);
                            obj.desc = mysql.escape(desc);


                            news.push(obj);
                        });
                    }


                } else {

                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key + "Filter"] = dataPath[i].data_filter;

                    }


                    if (arr['baseUrl']) {
                        let links = [];
                        await Promises
                        // Use this to iterate serially
                            .each($(arr['*']).get(), async function(elem) {
                            let link = $(elem).attr('href');
                            if (link) {
                                if (link.substring(0, 4) == "http") {

                                } else {
                                    link = arr['baseUrl'] + link;
                                    news.push(link);
                                }

                            }

                        }).then(async function() {

                        });


                    } else {
                        console.log(arr['*']);
                        let links = [];
                        let exLinks = [];
                        await Promises
                        // Use this to iterate serially
                            .each($(arr['*']).get(), async function(elem) {
                            let newObj = {};
                            let link = $(elem).attr('href');
                            links.push(link);

                            let title = $(elem).text();
                            title = title.trim();
                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.trim();
                            slug = slug.split(' ').join('_');

                            newObj.link = link;
                            newObj.title = title;
                            newObj.slug = slug;

                            news.push(link)

                        }).then(async function() {});



                    }

                }

            }

        }
        browser.close();
        res.send({ news, source_id })
    } catch (error) {
        console.log(error)
    }


});
router.get('/test/meth-getSourceUrlsData/:id/cat/:catId', async(req, res) => {
    let news = [];
    let source_id = req.params.id;
    let categor_id = req.params.catId;

    try {
        const rows = await query(`SELECT * FROM source_url WHERE source_id =${source_id} AND category_id = ${categor_id}`);
        // const browser = await puppeteer.launch({ headless: false });
        for (const index of rows) {
            try {
                const response = await axios.get(index.url);
                if (response.status === 200) {
                    const html = response.data;
                    const $ = cheerio.load(html, {
                        xmlMode: true
                    });
                    let category_id = index.category_id;
                    if (index.encoding == "xml") {
                        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                        let arr = [];
                        for (let i = 0; i < dataPath.length; i++) {
                            let key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[key + "Filter"] = dataPath[i].data_filter;
    
                        }
                        if (dataPath[1].data_param == "html") {
                            let newLinks = [];
                            $(arr['*']).each((i, elem) => {
                                let link = $(elem).find(arr['url']).text();
                                news.push(link);
    
                            });
    
                        } else {
                            $(arr['*']).each(async(i, elem) => {
                                let singleNews = [];
                                let obj = {};
                                let title = $(elem).find(arr['title']).text();
                                title = title.trim();
    
                                let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                slug = slug.split(' ').join('_');
                                let body = $(elem).find(arr['body']).text();
    
                                // let body = $(elem).find('content\\:encoded').text();
    
                                let bodyHtml = cheerio.load(body);
                                let desc = bodyHtml.text();
                                desc = desc.replace(/<[^>]*>?/gm, '');
    
                                let description = desc.substring(0, 200);
    
                                let link = $(elem).find(arr['url']).text();
                                let media = $(elem).find(arr['media']).attr('url');
                                if (!media) {
                                    media = $(elem).find(arr['media']).attr('src');
                                    if (!media) {
                                        let newHtm = cheerio.load(body);
                                        media = newHtm('img').attr('src');
    
                                    }
    
                                }
    
    
                                let pub_date = new Date();
                                let date = mysql.escape(pub_date);
                                date = date.substring(1, 20)
    
                                desc = mysql.escape(desc);
                                obj.title = mysql.escape(title);
                                obj.slug = mysql.escape(slug);
                                obj.body = mysql.escape(body);
                                obj.description = mysql.escape(description);
                                obj.media = mysql.escape(media);
                                obj.link = mysql.escape(link);
                                obj.category_id = mysql.escape(category_id);
                                obj.source_id = mysql.escape(source_id);
                                obj.date = mysql.escape(date);
                                obj.desc = mysql.escape(desc);
    
                                news.push(obj);
                            });
                        }
    
    
                    } else {
    
                        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                        let arr = [];
    
                        for (let i = 0; i < dataPath.length; i++) {
                            let key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[key + "Filter"] = dataPath[i].data_filter;
    
                        }    
                        if (arr['baseUrl']) {
                            let links = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {
                                let link = $(elem).attr('href');
                                if (link) {
                                    if (link.substring(0, 4) == "http") {
    
                                    } else {
                                        link = arr['baseUrl'] + link;
                                        news.push(link);
                                    }
    
                                }
    
                            }).then(async function() {
    
                            });
    
    
                        } else {
                            console.log(arr['*']);
                            let links = [];
                            let exLinks = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {
                                let newObj = {};
                                let link = $(elem).attr('href');
                                links.push(link);
    
                                let title = $(elem).text();
                                title = title.trim();
                                let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                slug = slug.trim();
                                slug = slug.split(' ').join('_');
    
                                newObj.link = link;
                                newObj.title = title;
                                newObj.slug = slug;
    
                                news.push(link)
    
                            }).then(async function() {});
    
    
    
                        }
    
                    }
    
                }
    
            } catch (error) {
                console.log(error);
            }
           
        }
        res.send({ news, source_id })
    } catch (error) {
        console.log(error)
    }


});
router.post('/test/getUrlData/', async(req, res) => {
    let news = [];
    let source_id = req.body.source;
    try {
        const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);
        const browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
        const page = await browser.newPage();
        await page.setViewport({ width: 1300, height: 1000 });

        await page.setDefaultNavigationTimeout(0);

        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + rows[0].id);
        let arr = [];

        for (let i = 0; i < dataPath.length; i++) {
            let key = dataPath[i].data_name;
            arr[key] = dataPath[i].data_path;
            arr[key + "Filter"] = dataPath[i].data_filter;

        }

        await page.goto(decodeURIComponent(req.body.url), { waitUntil: 'networkidle2' });

        // await page.evaluate(scrollToBottom);
        await page.waitFor(3000);

        let responseAgain = await page.evaluate(() => document.documentElement.outerHTML);
        if (responseAgain) {
            // if (responseAgain.status === 200) {

            // const htmlAgain = responseAgain.data;

            let $Again = cheerio.load(responseAgain);
            let slug;


            let title = $Again(arr['title']).attr('content')
            if (!title) {
                title = $Again(arr['title']).text();

                title = title.trim();
                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                slug = slug.split(' ').join('_');
            } else {
                title = title.trim();
                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                slug = slug.split(' ').join('_');
            }



            let media = $Again(arr['media']).attr('src');
            if (!media) {
                media = $Again(arr['media']).attr('content');
            }

            if (media) {
                if (media.substring(0, 5) == "http:") {
                    media = media.replace("http:", "https:");
                }
            }

            let body = $Again(arr['body']).html();

            if (body != null) {

                let newCheerio = cheerio.load(body, { decodeEntities: false });

                if (arr["bodyFilter"] != null) {
                    let bodyFil = arr["bodyFilter"].split('|');

                    for (const filter of bodyFil) {
                        newCheerio(filter).remove();

                    }
                }

                newCheerio('script').remove();
                newCheerio('style').remove();
                newCheerio('head').remove();

                let obj = {}
                body = newCheerio('body').html();
                let bodyHtml = cheerio.load(body);
                let desc = bodyHtml.text();
                desc = desc.replace(/<[^>]*>?/gm, '');

                let description = desc.substring(0, 200);

                let pub_date = new Date();
                let date = mysql.escape(pub_date);
                date = date.substring(1, 20);

                desc = mysql.escape(desc);

                obj.title = mysql.escape(title);

                obj.slug = mysql.escape(slug);

                obj.body = mysql.escape(body);
                obj.description = mysql.escape(description);
                obj.media = mysql.escape(media);
                obj.date = mysql.escape(date);
                obj.desc = mysql.escape(desc);
                // console.log(body)

                news.push(obj);
            }
        }

        res.send(news)


    } catch (err) {
        console.log(err)
    }

});
router.post('/test/meth-getUrlData/', async(req, res) => {
    let news = [];
    let source_id = req.body.source;
    try {
        const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);

        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + rows[0].id);
        let arr = [];

        for (let i = 0; i < dataPath.length; i++) {
            let key = dataPath[i].data_name;
            arr[key] = dataPath[i].data_path;
            arr[key + "Filter"] = dataPath[i].data_filter;

        }
        console.log(arr)
        const responseAgain = await axios.get(decodeURIComponent(req.body.url));
        if (responseAgain.status === 200) {
            const htmlAgain = responseAgain.data;

            let $Again = cheerio.load(htmlAgain);
            let slug;


            let title = $Again(arr['title']).attr('content')
            if (!title) {
                title = $Again(arr['title']).text();

                title = title.trim();
                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                slug = slug.split(' ').join('_');
            } else {
                title = title.trim();
                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                slug = slug.split(' ').join('_');
            }



            let media = $Again(arr['media']).attr('src');
            if (!media) {
                media = $Again(arr['media']).attr('content');
            }

            if (media) {
                if (media.substring(0, 5) == "http:") {
                    media = media.replace("http:", "https:");
                }
            }

            let body = $Again(arr['body']).html();

            if (body != null) {

                let newCheerio = cheerio.load(body, { decodeEntities: false });

                if (arr["bodyFilter"] != null) {
                    let bodyFil = arr["bodyFilter"].split('|');

                    for (const filter of bodyFil) {
                        newCheerio(filter).remove();

                    }
                }

                newCheerio('script').remove();
                newCheerio('style').remove();
                newCheerio('head').remove();

                let obj = {}
                body = newCheerio('body').html();
                let bodyHtml = cheerio.load(body);
                let desc = bodyHtml.text();
                desc = desc.replace(/<[^>]*>?/gm, '');

                let description = desc.substring(0, 200);

                let pub_date = new Date();
                let date = mysql.escape(pub_date);
                date = date.substring(1, 20);

                desc = mysql.escape(desc);

                obj.title = mysql.escape(title);

                obj.slug = mysql.escape(slug);

                obj.body = mysql.escape(body);
                obj.description = mysql.escape(description);
                obj.media = mysql.escape(media);
                obj.date = mysql.escape(date);
                obj.desc = mysql.escape(desc);
                // console.log(body)

                news.push(obj);
            }
        }

        res.send(news)


    } catch (err) {
        console.log(err)
    }

});
// router.get("/puptest", async (req, res) => {
//     console.log("hit")

//     let id = await query('SELECT source.*,country.name FROM source INNER JOIN country ON source.country_id=country.id');
//     for (const iterator of id) {
//         console.log(iterator.id)
//         let browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
//         const page = await browser.newPage();
//         await page.setViewport({ width: 1300, height: 1000 });
//         await page.setDefaultNavigationTimeout(0);
//         let source_id = iterator.id;
//         let news = [];
//         try {

//             const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);
//             let getLinks = await query(`SELECT url FROM news WHERE source_id = ${source_id} ORDER BY id DESC LIMIT 100`);
//             let checkLinks = []
//             for (const iterator of getLinks) {

//                 checkLinks.push(iterator.url)
//             }

//             for (const index of rows) {

//                 try {
//                     await page.goto(index.url, { waitUntil: 'networkidle2' });
//                     await page.waitFor(3000);

//                 } catch (error) {
//                     console.log(error)
//                 }
//                 try {
//                     response = await page.evaluate(() => document.documentElement.outerHTML);

//                 } catch (error) {
//                     console.log(error)

//                 }
//                 let category_id = index.category_id;
//                 if (response) {
//                     const html = response;
//                     const $ = cheerio.load(html, {
//                         xmlMode: true
//                     });

//                     if (index.encoding == "xml") {
//                         const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
//                         let arr = [];

//                         for (let i = 0; i < dataPath.length; i++) {
//                             let key = dataPath[i].data_name;
//                             arr[key] = dataPath[i].data_path;
//                             arr[key + "Filter"] = dataPath[i].data_filter;

//                         }
//                         if (dataPath[1].data_param == "html") {
//                             let newLinks = [];
//                             $(arr['*']).each((i, elem) => {
//                                 let link = $(elem).find(arr['url']).text();
//                                 newLinks.push(link);

//                             });

//                             for (const [n, newlink] of newLinks) {
//                                 let singleNews = [];
//                                 let obj = {};
//                                 let linkChecked = checkLinks.includes(newlink);
//                                 if (!linkChecked) {
//                                     if (n < 11) {

//                                         let responseAgain;
//                                         try {
//                                             await page.goto(newlink, { waitUntil: 'networkidle2' });
//                                             await page.waitFor(3000);

//                                         } catch (error) {
//                                             console.log(error)
//                                         }
//                                         try {
//                                             responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

//                                         } catch (error) {
//                                             console.log(error)

//                                         }

//                                         if (responseAgain) {
//                                             const htmlAgain = responseAgain;
//                                             const $Again = cheerio.load(htmlAgain);
//                                             let title = $Again(arr['title']).attr('content');
//                                             title = title.trim();
//                                             let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                             slug = slug.trim();
//                                             slug = slug.split(' ').join('_');

//                                             let media = $Again(arr['media']).attr('src');
//                                             if (!media) {
//                                                 media = $Again(arr['media']).attr('content');
//                                             }
//                                             if (media) {
//                                                 if (media.substring(0, 5) == "http:") {
//                                                     media = media.replace("http:", "https:");
//                                                 }
//                                             }
//                                             let body = $Again(arr['body']).html();

//                                             if (body != null) {
//                                                 let newCheerio = cheerio.load(body, { decodeEntities: false });
//                                                 let bodyFil = arr["bodyFilter"].split('|');

//                                                 for (const filter of bodyFil) {
//                                                     newCheerio(filter).remove();

//                                                 }
//                                                 newCheerio('script').remove();
//                                                 newCheerio('style').remove();
//                                                 newCheerio('svg').remove();
//                                                 newCheerio('button').remove();
//                                                 newCheerio('video').remove();

//                                                 body = newCheerio('body').html();

//                                                 let bodyHtml = cheerio.load(body);
//                                                 let desc = bodyHtml.text();
//                                                 desc = desc.replace(/<[^>]*>?/gm, '');

//                                                 let description = desc.substring(0, 200)

//                                                 let pub_date = new Date();
//                                                 let date = mysql.escape(pub_date);
//                                                 date = date.substring(1, 20);

//                                                 desc = mysql.escape(desc);
//                                                 obj.title = mysql.escape(title);
//                                                 obj.slug = mysql.escape(slug);
//                                                 obj.body = mysql.escape(body);
//                                                 obj.description = mysql.escape(description);
//                                                 obj.media = mysql.escape(media);
//                                                 obj.link = mysql.escape(newlink);
//                                                 obj.category_id = mysql.escape(category_id);
//                                                 obj.source_id = mysql.escape(source_id);
//                                                 obj.date = mysql.escape(date);
//                                                 obj.desc = mysql.escape(desc);
//                                                 obj.surce = mysql.escape(iterator.source);
//                                                 obj.lang = mysql.escape(iterator.lang);
//                                                 obj.source_slug = mysql.escape(iterator.image);
//                                                 obj.country = mysql.escape(iterator.name);
//                                                 news.push(obj);
//                                             }

//                                         }
//                                     }
//                                 }


//                             }


//                         } else {
//                             $(arr['*']).each(async (i, elem) => {
//                                 let singleNews = [];
//                                 let obj = {};
//                                 let title = $(elem).find(arr['title']).text();
//                                 title = title.trim();

//                                 let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                 slug = slug.split(' ').join('_');
//                                 let body = $(elem).find(arr['body']).text();

//                                 // let body = $(elem).find('content\\:encoded').text();

//                                 let bodyHtml = cheerio.load(body);
//                                 let desc = bodyHtml.text();
//                                 desc = desc.replace(/<[^>]*>?/gm, '');

//                                 let description = desc.substring(0, 200);

//                                 let link = $(elem).find(arr['url']).text();
//                                 console.log(link);
//                                 console.log(checkLinks[0]);

//                                 let linkChecked = checkLinks.includes(link);
//                                 if (!linkChecked) {
//                                     let media = $(elem).find(arr['media']).attr('url');
//                                     if (!media) {
//                                         media = $(elem).find(arr['media']).attr('src');
//                                         if (!media) {
//                                             let newHtm = cheerio.load(body);
//                                             media = newHtm('img').attr('src');

//                                         }

//                                     }
//                                     if (media) {
//                                         if (media.substring(0, 5) == "http:") {
//                                             media = media.replace("http:", "https:");
//                                         }
//                                     }

//                                     let pub_date = new Date();
//                                     let date = mysql.escape(pub_date);
//                                     date = date.substring(1, 20)

//                                     desc = mysql.escape(desc);
//                                     obj.title = mysql.escape(title);
//                                     obj.slug = mysql.escape(slug);
//                                     obj.body = mysql.escape(body);
//                                     obj.description = mysql.escape(description);
//                                     obj.media = mysql.escape(media);
//                                     obj.link = mysql.escape(link);
//                                     obj.category_id = mysql.escape(category_id);
//                                     obj.source_id = mysql.escape(source_id);
//                                     obj.date = mysql.escape(date);
//                                     obj.desc = mysql.escape(desc);
//                                     obj.surce = mysql.escape(iterator.source);
//                                     obj.lang = mysql.escape(iterator.lang);
//                                     obj.source_slug = mysql.escape(iterator.image);
//                                     obj.country = mysql.escape(iterator.name);

//                                     news.push(obj);
//                                 }
//                             });
//                         }


//                     } else {

//                         const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
//                         let arr = [];

//                         for (let i = 0; i < dataPath.length; i++) {
//                             let key = dataPath[i].data_name;
//                             arr[key] = dataPath[i].data_path;
//                             arr[key + "Filter"] = dataPath[i].data_filter;

//                         }
//                         if (arr['baseUrl']) {
//                             let links = [];
//                             await Promises
//                                 // Use this to iterate serially
//                                 .each($(arr['*']).get(), async function (elem) {
//                                     // let newObj= {};
//                                     let link = $(elem).attr('href');
//                                     if (link) {
//                                         if (link.substring(0, 4) == "http") {

//                                         } else {
//                                             link = arr['baseUrl'] + link;
//                                             links.push(link);
//                                         }

//                                     }

//                                     // let title = $(elem).text();
//                                     // title=title.trim();
//                                     // let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                     //     slug = slug.trim();
//                                     //     slug = slug.split(' ').join('_');

//                                     // // newObj.link=link;
//                                     // newObj.title=title
//                                     // newObj.slug=slug;

//                                     // exLinks.push(newObj)

//                                 }).then(async function () {

//                                     for (const indexs of links) {
//                                         let singleNews = [];
//                                         let obj = {};
//                                         let linkChecked = checkLinks.includes(indexs);
//                                         let responseAgain;
//                                         if (!linkChecked) {


//                                             try {
//                                                 await page.goto(indexs, { waitUntil: 'networkidle2' });
//                                                 await page.waitFor(3000);

//                                             } catch (error) {
//                                                 console.log(error)
//                                             }
//                                             try {
//                                                 responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

//                                             } catch (error) {
//                                                 console.log(error)

//                                             }



//                                             if (responseAgain) {
//                                                 const htmlAgain = responseAgain;
//                                                 const $Again = cheerio.load(htmlAgain);
//                                                 let title;
//                                                 let slug;

//                                                 if (!title) {
//                                                     title = $Again(arr['title']).attr('content');
//                                                     title = title.trim();
//                                                     slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                                     slug = slug.split(' ').join('_');
//                                                 }

//                                                 let media = $Again(arr['media']).attr('src');

//                                                 if (!media) {
//                                                     media = $Again(arr['media']).attr('content');
//                                                 }
//                                                 if (media) {
//                                                     if (media.substring(0, 5) == "http:") {
//                                                         media = media.replace("http:", "https:");
//                                                     }
//                                                 }
//                                                 let body = $Again(arr['body']).html();
//                                                 if (body != null) {

//                                                     let newCheerio = cheerio.load(body, { decodeEntities: false });
//                                                     let bodyFil = arr["bodyFilter"].split('|');
//                                                     for (const filter of bodyFil) {
//                                                         newCheerio(filter).remove();

//                                                     }
//                                                     newCheerio('script').remove();
//                                                     newCheerio('style').remove();
//                                                     newCheerio('svg').remove();
//                                                     newCheerio('button').remove();
//                                                     newCheerio('video').remove();


//                                                     body = newCheerio('body').html();
//                                                     let bodyHtml = cheerio.load(body);
//                                                     let desc = bodyHtml.text();
//                                                     desc = desc.replace(/<[^>]*>?/gm, '');

//                                                     let description = desc.substring(0, 200);

//                                                     let pub_date = new Date();
//                                                     let date = mysql.escape(pub_date);
//                                                     date = date.substring(1, 20);

//                                                     desc = mysql.escape(desc);
//                                                     obj.title = mysql.escape(title);
//                                                     obj.slug = mysql.escape(slug);
//                                                     obj.body = mysql.escape(body);
//                                                     obj.description = mysql.escape(description);
//                                                     obj.media = mysql.escape(media);
//                                                     obj.link = mysql.escape(indexs);
//                                                     obj.category_id = mysql.escape(category_id);
//                                                     obj.source_id = mysql.escape(source_id);
//                                                     obj.date = mysql.escape(date);
//                                                     obj.desc = mysql.escape(desc);
//                                                     // console.log(body)
//                                                     obj.surce = mysql.escape(iterator.source);
//                                                     obj.lang = mysql.escape(iterator.lang);
//                                                     obj.source_slug = mysql.escape(iterator.image);
//                                                     obj.country = mysql.escape(iterator.name);
//                                                     news.push(obj);
//                                                 }
//                                             }
//                                         }

//                                     }
//                                 });


//                         } else {
//                             let links = [];
//                             let exLinks = [];
//                             await Promises
//                                 // Use this to iterate serially
//                                 .each($(arr['*']).get(), async function (elem) {

//                                     let newObj = {};
//                                     let link = $(elem).attr('href');
//                                     links.push(link);
//                                     let title = $(elem).text();
//                                     title = title.trim();
//                                     let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                     slug = slug.trim();
//                                     slug = slug.split(' ').join('_');

//                                     newObj.link = link;
//                                     newObj.title = title;
//                                     newObj.slug = slug;

//                                     exLinks.push(newObj)

//                                 }).then(async function () {
//                                     for (const myData of exLinks) {
//                                         // console.log(myData)
//                                         let singleNews = [];
//                                         let obj = {};
//                                         let linkChecked = checkLinks.includes(myData.link);
//                                         if (!linkChecked) {
//                                             let responseAgain;

//                                             try {
//                                                 await page.goto(myData.link, { waitUntil: 'networkidle2' });
//                                                 await page.waitFor(3000);

//                                             } catch (error) {
//                                                 console.log(error)
//                                             }
//                                             try {
//                                                 responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

//                                             } catch (error) {
//                                                 console.log(error)

//                                             }

//                                             if (responseAgain) {
//                                                 const htmlAgain = responseAgain.data;
//                                                 const $Again = cheerio.load(htmlAgain);
//                                                 let title;
//                                                 let slug;
//                                                 if (myData.title.length > 0 && myData.title != undefined) {
//                                                     title = myData.title;
//                                                     slug = myData.slug;
//                                                 } else {
//                                                     title = $Again(arr['title']).attr('content');
//                                                     title = title.trim();
//                                                     slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                                     slug = slug.split(' ').join('_');
//                                                 }

//                                                 let media = $Again(arr['media']).attr('src');

//                                                 if (!media) {
//                                                     media = $Again(arr['media']).attr('content');
//                                                 }
//                                                 if (media) {
//                                                     if (media.substring(0, 5) == "http:") {
//                                                         media = media.replace("http:", "https:");
//                                                     }
//                                                 }
//                                                 if (source_id == 84) {
//                                                     title = $Again(arr['title']).attr('content');
//                                                     title = title.trim();
//                                                     slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
//                                                     slug = slug.split(' ').join('_');
//                                                 }
//                                                 let body = $Again(arr['body']).html();
//                                                 if (body != null) {

//                                                     let newCheerio = cheerio.load(body, { decodeEntities: false });
//                                                     if (arr["bodyFilter"] != null) {
//                                                         let bodyFil = arr["bodyFilter"].split('|');
//                                                         for (const filter of bodyFil) {
//                                                             newCheerio(filter).remove();

//                                                         }

//                                                     }

//                                                     newCheerio('script').remove();
//                                                     newCheerio('style').remove();

//                                                     newCheerio('svg').remove();
//                                                     newCheerio('button').remove();
//                                                     newCheerio('video').remove();


//                                                     body = newCheerio('body').html();
//                                                     let bodyHtml = cheerio.load(body);
//                                                     let desc = bodyHtml.text();
//                                                     desc = desc.replace(/<[^>]*>?/gm, '');

//                                                     let description = desc.substring(0, 200);

//                                                     let pub_date = new Date();
//                                                     let date = mysql.escape(pub_date);
//                                                     date = date.substring(1, 20);

//                                                     desc = mysql.escape(desc);
//                                                     obj.title = mysql.escape(title);
//                                                     obj.slug = mysql.escape(slug);
//                                                     obj.body = mysql.escape(body);
//                                                     obj.description = mysql.escape(description);
//                                                     obj.media = mysql.escape(media);
//                                                     obj.link = mysql.escape(myData.link);
//                                                     obj.category_id = mysql.escape(category_id);
//                                                     obj.source_id = mysql.escape(source_id);
//                                                     obj.date = mysql.escape(date);
//                                                     obj.desc = mysql.escape(desc);
//                                                     obj.surce = mysql.escape(iterator.source);
//                                                     obj.lang = mysql.escape(iterator.lang);
//                                                     obj.source_slug = mysql.escape(iterator.image);
//                                                     obj.country = mysql.escape(iterator.name);
//                                                     news.push(obj);
//                                                 }
//                                             }
//                                         }

//                                     }

//                                 });



//                         }

//                     }

//                 }
//             }
//         } catch (err) {
//             console.log(err);
//         }
//         browser.close();

//         news = [...new Set(news)];
//         for (const element of news) {       
//             let sentidata;
//             let deepData;
//             // try {
//             //     let senti = await axios.get('http://192.64.113.8:5000/newshunt_senti?text=' + encodeURIComponent(element.desc));
//             //     sentidata = senti;
//             //     let newSenti = senti.data;
//             //     if (newSenti != undefined && newSenti != null) {
//             //         element.senti = mysql.escape(newSenti.sentences);
//             //         element.sentiStatus = mysql.escape(newSenti.status);
//             //         element.sentiNeg = mysql.escape(newSenti.Neg);
//             //         element.sentiNeu = mysql.escape(newSenti.Neu);
//             //         element.sentiPos = mysql.escape(newSenti.Pos);
//             //     }
//             // } catch (err) {


//             // }
//             // if (!sentidata) {
//             //     element.senti = null;
//             //     element.sentiStatus = null;
//             //     element.sentiNeg = null;
//             //     element.sentiNeu = null;
//             //     element.sentiPos = null;
//             // }
//             // let sumaryData;

//             // try {
//             //     let sumary = await axios.get('http://192.64.113.8:5000/newshunt_summary?text=' + encodeURIComponent(element.desc));
//             //     sumaryData =sumary.data;
//             //     let summary = sumary.data;
//             //     if (summary != undefined && summary != null) {
//             //         element.summary = mysql.escape(summary);

//             //     }


//             // } catch (err) {
//             //     // element.summary = null;

//             //     console.log(err);
//             // }
//             // if (!sumaryData) {
//             //     element.summary = null;

//             // }
//             // try {
//             //     let deepAnalysis = await axios.get('http://192.64.113.8:5000/newshunt_deep?text=' + encodeURIComponent(element.desc));
//             //     deepData=deepAnalysis;
//             //     let deep = deepAnalysis.data;
//             //     if (deep != undefined && deep != null) {

//             //         element.deepSentences = mysql.escape(deep.sentences);
//             //         element.deepStatus = mysql.escape(deep.status);
//             //         element.deepInsult = mysql.escape(deep.insult);
//             //         element.deepToxicity = mysql.escape(deep.toxicity);
//             //         element.deepProfanity = mysql.escape(deep.profanity);
//             //         element.deepexual = mysql.escape(deep.sexual);
//             //         element.deeoIdentity = mysql.escape(deep.identity);
//             //         element.deepFlirt = mysql.escape(deep.flirt);
//             //         element.deepThreat = mysql.escape(deep.threat);
//             //         element.DeepSevere = mysql.escape(deep.severe);



//             //     }
//             // } catch (err) {


//             // }
//             // if(!deepData){
//             //     element.deepSentences = '';
//             //     element.deepStatus = '';
//             //     element.deepInsult = '';
//             //     element.deepToxicity = '';
//             //     element.deepProfanity = '';
//             //     element.deepexual = '';
//             //     element.deeoIdentity = '';
//             //     element.deepFlirt = '';
//             //     element.deepThreat = '';
//             //     element.DeepSevere = '';
//             // }

//             // try {
//             //     const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,sentiment,sentiment_state,senti_neg,senti_neu,senti_pos,summary) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.senti + ', ' + element.sentiStatus + ', ' + element.sentiNeg + ', ' + element.sentiNeu + ', ' + element.sentiPos + ', ' + element.summary + ')');

//             // } catch (err) {
//             //     console.log(err)
//             // }
//             try {
//                 const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,source,lang,source_slug,country) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.surce + ', ' + element.lang + ', ' + element.source_slug + ', ' + element.country + ')');
//                 const updateSource = await query(`UPDATE source SET updated_at = current_timestamp WHERE id=${element.source_id}`)

//             } catch (err) {
//                 console.log(err)
//             }

//         }

//     }
// })
router.get("/puptest/:id", async(req, res) => {
    console.log("hit")
    console.time("start");
    let id = await query('SELECT source.*,country.name FROM source INNER JOIN country ON source.country_id=country.id where source.id = ' + req.params.id);
    for (const iterator of id) {
        console.log(iterator.id)
        let browser = await puppeteer.launch({ headless: true, args: ['--no-sandbox'] });
        const page = await browser.newPage();
        await page.setViewport({ width: 1300, height: 1000 });
        await page.setDefaultNavigationTimeout(0);
        let source_id = iterator.id;
        let news = [];
        try {

            const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);
            let getLinks = await query(`SELECT url FROM news WHERE source_id = ${source_id} ORDER BY id DESC LIMIT 100`);
            let checkLinks = []
            for (const iterator of getLinks) {

                checkLinks.push(iterator.url)
            }

            for (const index of rows) {

                try {
                    await page.goto(index.url, { waitUntil: 'networkidle2' });
                    await page.waitFor(3000);

                } catch (error) {
                    console.log(error)
                }
                try {
                    response = await page.evaluate(() => document.documentElement.outerHTML);

                } catch (error) {
                    console.log(error)

                }
                let category_id = index.category_id;
                if (response) {
                    const html = response;
                    const $ = cheerio.load(html, {
                        xmlMode: true
                    });

                    if (index.encoding == "xml") {
                        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                        let arr = [];

                        for (let i = 0; i < dataPath.length; i++) {
                            let key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[key + "Filter"] = dataPath[i].data_filter;

                        }
                        if (dataPath[1].data_param == "html") {
                            let newLinks = [];
                            $(arr['*']).each((i, elem) => {
                                let link = $(elem).find(arr['url']).text();
                                newLinks.push(link);

                            });

                            for (const [n, newlink] of newLinks) {
                                let singleNews = [];
                                let obj = {};
                                let linkChecked = checkLinks.includes(newlink);
                                if (!linkChecked) {
                                    if (n < 11) {

                                        let responseAgain;
                                        try {
                                            await page.goto(newlink, { waitUntil: 'networkidle2' });
                                            await page.waitFor(3000);

                                        } catch (error) {
                                            console.log(error)
                                        }
                                        try {
                                            responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

                                        } catch (error) {
                                            console.log(error)

                                        }

                                        if (responseAgain) {
                                            const htmlAgain = responseAgain;
                                            const $Again = cheerio.load(htmlAgain);
                                            let title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.trim();
                                            slug = slug.split(' ').join('_');

                                            let media = $Again(arr['media']).attr('src');
                                            if (!media) {
                                                media = $Again(arr['media']).attr('content');
                                            }
                                            if (media) {
                                                if (media.substring(0, 5) == "http:") {
                                                    media = media.replace("http:", "https:");
                                                }
                                            }
                                            let body = $Again(arr['body']).html();

                                            if (body != null) {
                                                let newCheerio = cheerio.load(body, { decodeEntities: false });
                                                let bodyFil = arr["bodyFilter"].split('|');

                                                for (const filter of bodyFil) {
                                                    newCheerio(filter).remove();

                                                }
                                                newCheerio('script').remove();
                                                newCheerio('style').remove();
                                                newCheerio('svg').remove();
                                                newCheerio('button').remove();
                                                newCheerio('video').remove();

                                                body = newCheerio('body').html();

                                                let bodyHtml = cheerio.load(body);
                                                let desc = bodyHtml.text();
                                                desc = desc.replace(/<[^>]*>?/gm, '');

                                                let description = desc.substring(0, 200)

                                                let pub_date = new Date();
                                                let date = mysql.escape(pub_date);
                                                date = date.substring(1, 20);

                                                desc = mysql.escape(desc);
                                                obj.title = mysql.escape(title);
                                                obj.slug = mysql.escape(slug);
                                                obj.body = mysql.escape(body);
                                                obj.description = mysql.escape(description);
                                                obj.media = mysql.escape(media);
                                                obj.link = mysql.escape(newlink);
                                                obj.category_id = mysql.escape(category_id);
                                                obj.source_id = mysql.escape(source_id);
                                                obj.date = mysql.escape(date);
                                                obj.desc = mysql.escape(desc);
                                                obj.surce = mysql.escape(iterator.source);
                                                obj.lang = mysql.escape(iterator.lang);
                                                obj.source_slug = mysql.escape(iterator.image);
                                                obj.country = mysql.escape(iterator.name);
                                                news.push(obj);
                                            }

                                        }
                                    }
                                }


                            }


                        } else {
                            $(arr['*']).each(async(i, elem) => {
                                let singleNews = [];
                                let obj = {};
                                let title = $(elem).find(arr['title']).text();
                                title = title.trim();

                                let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                slug = slug.split(' ').join('_');
                                let body = $(elem).find(arr['body']).text();

                                // let body = $(elem).find('content\\:encoded').text();

                                let bodyHtml = cheerio.load(body);
                                let desc = bodyHtml.text();
                                desc = desc.replace(/<[^>]*>?/gm, '');

                                let description = desc.substring(0, 200);

                                let link = $(elem).find(arr['url']).text();
                                console.log(link);
                                console.log(checkLinks[0]);

                                let linkChecked = checkLinks.includes(link);
                                if (!linkChecked) {
                                    let media = $(elem).find(arr['media']).attr('url');
                                    if (!media) {
                                        media = $(elem).find(arr['media']).attr('src');
                                        if (!media) {
                                            let newHtm = cheerio.load(body);
                                            media = newHtm('img').attr('src');

                                        }

                                    }
                                    if (media) {
                                        if (media.substring(0, 5) == "http:") {
                                            media = media.replace("http:", "https:");
                                        }
                                    }

                                    let pub_date = new Date();
                                    let date = mysql.escape(pub_date);
                                    date = date.substring(1, 20)

                                    desc = mysql.escape(desc);
                                    obj.title = mysql.escape(title);
                                    obj.slug = mysql.escape(slug);
                                    obj.body = mysql.escape(body);
                                    obj.description = mysql.escape(description);
                                    obj.media = mysql.escape(media);
                                    obj.link = mysql.escape(link);
                                    obj.category_id = mysql.escape(category_id);
                                    obj.source_id = mysql.escape(source_id);
                                    obj.date = mysql.escape(date);
                                    obj.desc = mysql.escape(desc);
                                    obj.surce = mysql.escape(iterator.source);
                                    obj.lang = mysql.escape(iterator.lang);
                                    obj.source_slug = mysql.escape(iterator.image);
                                    obj.country = mysql.escape(iterator.name);

                                    news.push(obj);
                                }
                            });
                        }


                    } else {

                        const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                        let arr = [];

                        for (let i = 0; i < dataPath.length; i++) {
                            let key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[key + "Filter"] = dataPath[i].data_filter;

                        }
                        if (arr['baseUrl']) {
                            let links = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {
                                // let newObj= {};
                                let link = $(elem).attr('href');
                                if (link) {
                                    if (link.substring(0, 4) == "http") {

                                    } else {
                                        link = arr['baseUrl'] + link;
                                        links.push(link);
                                    }

                                }

                                // let title = $(elem).text();
                                // title=title.trim();
                                // let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                //     slug = slug.trim();
                                //     slug = slug.split(' ').join('_');

                                // // newObj.link=link;
                                // newObj.title=title
                                // newObj.slug=slug;

                                // exLinks.push(newObj)

                            }).then(async function() {

                                for (const indexs of links) {
                                    let singleNews = [];
                                    let obj = {};
                                    let linkChecked = checkLinks.includes(indexs);
                                    let responseAgain;
                                    if (!linkChecked) {


                                        try {
                                            await page.goto(indexs, { waitUntil: 'networkidle2' });
                                            await page.waitFor(3000);

                                        } catch (error) {
                                            console.log(error)
                                        }
                                        try {
                                            responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

                                        } catch (error) {
                                            console.log(error)

                                        }



                                        if (responseAgain) {
                                            const htmlAgain = responseAgain;
                                            const $Again = cheerio.load(htmlAgain);
                                            let title;
                                            let slug;

                                            if (!title) {
                                                title = $Again(arr['title']).attr('content');
                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }

                                            let media = $Again(arr['media']).attr('src');

                                            if (!media) {
                                                media = $Again(arr['media']).attr('content');
                                            }
                                            if (media) {
                                                if (media.substring(0, 5) == "http:") {
                                                    media = media.replace("http:", "https:");
                                                }
                                            }
                                            let body = $Again(arr['body']).html();
                                            if (body != null) {

                                                let newCheerio = cheerio.load(body, { decodeEntities: false });
                                                let bodyFil = arr["bodyFilter"].split('|');
                                                for (const filter of bodyFil) {
                                                    newCheerio(filter).remove();

                                                }
                                                newCheerio('script').remove();
                                                newCheerio('style').remove();
                                                newCheerio('svg').remove();
                                                newCheerio('button').remove();
                                                newCheerio('video').remove();


                                                body = newCheerio('body').html();
                                                let bodyHtml = cheerio.load(body);
                                                let desc = bodyHtml.text();
                                                desc = desc.replace(/<[^>]*>?/gm, '');

                                                let description = desc.substring(0, 200);

                                                let pub_date = new Date();
                                                let date = mysql.escape(pub_date);
                                                date = date.substring(1, 20);

                                                desc = mysql.escape(desc);
                                                obj.title = mysql.escape(title);
                                                obj.slug = mysql.escape(slug);
                                                obj.body = mysql.escape(body);
                                                obj.description = mysql.escape(description);
                                                obj.media = mysql.escape(media);
                                                obj.link = mysql.escape(indexs);
                                                obj.category_id = mysql.escape(category_id);
                                                obj.source_id = mysql.escape(source_id);
                                                obj.date = mysql.escape(date);
                                                obj.desc = mysql.escape(desc);
                                                // console.log(body)
                                                obj.surce = mysql.escape(iterator.source);
                                                obj.lang = mysql.escape(iterator.lang);
                                                obj.source_slug = mysql.escape(iterator.image);
                                                obj.country = mysql.escape(iterator.name);
                                                news.push(obj);
                                            }
                                        }
                                    }

                                }
                            });


                        } else {
                            let links = [];
                            let exLinks = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {

                                let newObj = {};
                                let link = $(elem).attr('href');
                                links.push(link);
                                let title = $(elem).text();
                                title = title.trim();
                                let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                slug = slug.trim();
                                slug = slug.split(' ').join('_');

                                newObj.link = link;
                                newObj.title = title;
                                newObj.slug = slug;

                                exLinks.push(newObj)
                            }).then(async function() {
                                let non_duplidated_data = _.uniqBy(exLinks, 'link');
                                for (const myData of non_duplidated_data) {
                                    // console.log(myData)
                                    let singleNews = [];
                                    let obj = {};
                                    let linkChecked = checkLinks.includes(myData.link);
                                    if (!linkChecked) {
                                        let responseAgain;

                                        try {
                                            await page.goto(myData.link, { waitUntil: 'networkidle2' });
                                            await page.waitFor(3000);

                                        } catch (error) {
                                            console.log(error)
                                        }
                                        try {
                                            responseAgain = await page.evaluate(() => document.documentElement.outerHTML);

                                        } catch (error) {
                                            console.log(error)

                                        }

                                        if (responseAgain) {
                                            const htmlAgain = responseAgain;
                                            const $Again = cheerio.load(htmlAgain);
                                            let title;
                                            let slug;
                                            if (myData.title.length > 0 && myData.title != undefined) {
                                                title = myData.title;
                                                slug = myData.slug;
                                            } else {
                                                if (!title) {
                                                    title = $Again(arr['title']).attr('content');
                                                }
                                                if (title) {
                                                    title = title.trim();
                                                    slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                    slug = slug.split(' ').join('_');
                                                }

                                            }

                                            let media = $Again(arr['media']).attr('src');

                                            if (!media) {
                                                media = $Again(arr['media']).attr('content');
                                            }
                                            if (media) {
                                                if (media.substring(0, 5) == "http:") {
                                                    media = media.replace("http:", "https:");
                                                }
                                            }
                                            if (source_id == 84) {
                                                title = $Again(arr['title']).attr('content');
                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }
                                            let body = $Again(arr['body']).html();
                                            if (body != null) {

                                                let newCheerio = cheerio.load(body, { decodeEntities: false });
                                                if (arr["bodyFilter"] != null) {
                                                    let bodyFil = arr["bodyFilter"].split('|');
                                                    for (const filter of bodyFil) {
                                                        newCheerio(filter).remove();

                                                    }

                                                }

                                                newCheerio('script').remove();
                                                newCheerio('style').remove();

                                                newCheerio('svg').remove();
                                                newCheerio('button').remove();
                                                newCheerio('video').remove();


                                                body = newCheerio('body').html();
                                                let bodyHtml = cheerio.load(body);
                                                let desc = bodyHtml.text();
                                                desc = desc.replace(/<[^>]*>?/gm, '');

                                                let description = desc.substring(0, 200);

                                                let pub_date = new Date();
                                                let date = mysql.escape(pub_date);
                                                date = date.substring(1, 20);

                                                desc = mysql.escape(desc);
                                                obj.title = mysql.escape(title);
                                                obj.slug = mysql.escape(slug);
                                                obj.body = mysql.escape(body);
                                                obj.description = mysql.escape(description);
                                                obj.media = mysql.escape(media);
                                                obj.link = mysql.escape(myData.link);
                                                obj.category_id = mysql.escape(category_id);
                                                obj.source_id = mysql.escape(source_id);
                                                obj.date = mysql.escape(date);
                                                obj.desc = mysql.escape(desc);
                                                obj.surce = mysql.escape(iterator.source);
                                                obj.lang = mysql.escape(iterator.lang);
                                                obj.source_slug = mysql.escape(iterator.image);
                                                obj.country = mysql.escape(iterator.name);
                                                news.push(obj);
                                            }
                                        }
                                    }

                                }

                            });



                        }

                    }

                }
            }
        } catch (err) {
            console.log(err);
        }
        browser.close();

        news = [...new Set(news)];
        for (const element of news) {
            let sentidata;
            let deepData;
            // try {
            //     let senti = await axios.get('http://192.64.113.8:5000/newshunt_senti?text=' + encodeURIComponent(element.desc));
            //     sentidata = senti;
            //     let newSenti = senti.data;
            //     if (newSenti != undefined && newSenti != null) {
            //         element.senti = mysql.escape(newSenti.sentences);
            //         element.sentiStatus = mysql.escape(newSenti.status);
            //         element.sentiNeg = mysql.escape(newSenti.Neg);
            //         element.sentiNeu = mysql.escape(newSenti.Neu);
            //         element.sentiPos = mysql.escape(newSenti.Pos);
            //     }
            // } catch (err) {


            // }
            // if (!sentidata) {
            //     element.senti = null;
            //     element.sentiStatus = null;
            //     element.sentiNeg = null;
            //     element.sentiNeu = null;
            //     element.sentiPos = null;
            // }
            // let sumaryData;

            // try {
            //     let sumary = await axios.get('http://192.64.113.8:5000/newshunt_summary?text=' + encodeURIComponent(element.desc));
            //     sumaryData =sumary.data;
            //     let summary = sumary.data;
            //     if (summary != undefined && summary != null) {
            //         element.summary = mysql.escape(summary);

            //     }


            // } catch (err) {
            //     // element.summary = null;

            //     console.log(err);
            // }
            // if (!sumaryData) {
            //     element.summary = null;

            // }
            // try {
            //     let deepAnalysis = await axios.get('http://192.64.113.8:5000/newshunt_deep?text=' + encodeURIComponent(element.desc));
            //     deepData=deepAnalysis;
            //     let deep = deepAnalysis.data;
            //     if (deep != undefined && deep != null) {

            //         element.deepSentences = mysql.escape(deep.sentences);
            //         element.deepStatus = mysql.escape(deep.status);
            //         element.deepInsult = mysql.escape(deep.insult);
            //         element.deepToxicity = mysql.escape(deep.toxicity);
            //         element.deepProfanity = mysql.escape(deep.profanity);
            //         element.deepexual = mysql.escape(deep.sexual);
            //         element.deeoIdentity = mysql.escape(deep.identity);
            //         element.deepFlirt = mysql.escape(deep.flirt);
            //         element.deepThreat = mysql.escape(deep.threat);
            //         element.DeepSevere = mysql.escape(deep.severe);



            //     }
            // } catch (err) {


            // }
            // if(!deepData){
            //     element.deepSentences = '';
            //     element.deepStatus = '';
            //     element.deepInsult = '';
            //     element.deepToxicity = '';
            //     element.deepProfanity = '';
            //     element.deepexual = '';
            //     element.deeoIdentity = '';
            //     element.deepFlirt = '';
            //     element.deepThreat = '';
            //     element.DeepSevere = '';
            // }

            // try {
            //     const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,sentiment,sentiment_state,senti_neg,senti_neu,senti_pos,summary) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.senti + ', ' + element.sentiStatus + ', ' + element.sentiNeg + ', ' + element.sentiNeu + ', ' + element.sentiPos + ', ' + element.summary + ')');

            // } catch (err) {
            //     console.log(err)
            // }
            try {
                const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,source,lang,source_slug,country) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.surce + ', ' + element.lang + ', ' + element.source_slug + ', ' + element.country + ')');
                const updateSource = await query(`UPDATE source SET updated_at = current_timestamp WHERE id=${element.source_id}`)


            } catch (err) {
                // console.log(err)
            }

        }

    }
    console.timeEnd('start')
    res.send("done")
})


router.get('/simpleTest/:id', async(req, res) => {
    let errors = [];

    let id = await query(`SELECT source.*,country.name FROM source INNER JOIN country ON source.country_id=country.id where source.id = ${req.params.id} `);
    let iterator = id[0];


    let source_id = iterator.id;
    let news = [];
    try {

        const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);
        let getLinks = await query(`SELECT url FROM news WHERE source_id = ${source_id} ORDER BY id DESC LIMIT 100`);
        let checkLinks = []
        for (const iterator of getLinks) {

            checkLinks.push(iterator.url)
        }
        for (let index = 0; index < rows.length; index++) {
            const response = await axios.get(rows[index].url);
            let category_id = rows[index].category_id;
            if (response.status === 200) {
                const html = response.data;
                const $ = cheerio.load(html, {
                    xmlMode: true
                });

                if (rows[index].encoding == "xml") {
                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + rows[index].id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key + "Filter"] = dataPath[i].data_filter;

                    }
                    if (dataPath[1].data_param == "html") {
                        let newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            let link = $(elem).find(arr['url']).text();
                            newLinks.push(link);

                        });
                        for (let n = 0; n < newLinks.length; n++) {
                            let singleNews = [];
                            let obj = {};
                            let linkChecked = checkLinks.includes(newLinks[n]);
                            console.log(newLinks[n])
                            if (!linkChecked) {
                                if (n < 11) {
                                    const responseAgain = await axios.get(newLinks[n]);
                                    if (responseAgain.status === 200) {
                                        const htmlAgain = responseAgain.data;
                                        const $Again = cheerio.load(htmlAgain);
                                        let title = $Again(arr['title']).attr('content');
                                        title = title.trim();
                                        let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                        slug = slug.trim();
                                        slug = slug.split(' ').join('_');

                                        let media = $Again(arr['media']).attr('src');
                                        if (!media) {
                                            media = $Again(arr['media']).attr('content');
                                        }
                                        if (media) {
                                            if (media.substring(0, 5) == "http:") {
                                                media = media.replace("http:", "https:");
                                            }
                                        }
                                        let body = $Again(arr['body']).html();

                                        if (body != null) {
                                            let newCheerio = cheerio.load(body, { decodeEntities: false });
                                            let bodyFil = arr["bodyFilter"].split('|');

                                            for (const filter of bodyFil) {
                                                newCheerio(filter).remove();

                                            }
                                            newCheerio('script').remove();
                                            newCheerio('style').remove();
                                            newCheerio('svg').remove();
                                            newCheerio('button').remove();
                                            newCheerio('video').remove();

                                            body = newCheerio('body').html();

                                            let bodyHtml = cheerio.load(body);
                                            let desc = bodyHtml.text();
                                            desc = desc.replace(/<[^>]*>?/gm, '');

                                            let description = desc.substring(0, 200)

                                            let pub_date = new Date();
                                            let date = mysql.escape(pub_date);
                                            date = date.substring(1, 20);

                                            desc = mysql.escape(desc);
                                            obj.title = mysql.escape(title);
                                            obj.slug = mysql.escape(slug);
                                            obj.body = mysql.escape(body);
                                            obj.description = mysql.escape(description);
                                            obj.media = mysql.escape(media);
                                            obj.link = mysql.escape(newLinks[n]);
                                            obj.category_id = mysql.escape(category_id);
                                            obj.source_id = mysql.escape(source_id);
                                            obj.date = mysql.escape(date);
                                            obj.desc = mysql.escape(desc);
                                            obj.surce = mysql.escape(iterator.source);
                                            obj.lang = mysql.escape(iterator.lang);
                                            obj.source_slug = mysql.escape(iterator.image);
                                            obj.country = mysql.escape(iterator.name);
                                            news.push(obj);
                                        }

                                    }
                                }
                            }


                        }


                    } else {
                        $(arr['*']).each(async(i, elem) => {
                            let singleNews = [];
                            let obj = {};
                            let title = $(elem).find(arr['title']).text();
                            title = title.trim();

                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.split(' ').join('_');
                            let body = $(elem).find(arr['body']).text();

                            // let body = $(elem).find('content\\:encoded').text();

                            let bodyHtml = cheerio.load(body);
                            let desc = bodyHtml.text();
                            desc = desc.replace(/<[^>]*>?/gm, '');

                            let description = desc.substring(0, 200);

                            let link = $(elem).find(arr['url']).text();
                            let linkChecked = checkLinks.includes(link);
                            if (!linkChecked) {
                                let media = $(elem).find(arr['media']).attr('url');
                                if (!media) {
                                    media = $(elem).find(arr['media']).attr('src');
                                    if (!media) {
                                        let newHtm = cheerio.load(body);
                                        media = newHtm('img').attr('src');

                                    }

                                }
                                if (media) {
                                    if (media.substring(0, 5) == "http:") {
                                        media = media.replace("http:", "https:");
                                    }
                                }

                                let pub_date = new Date();
                                let date = mysql.escape(pub_date);
                                date = date.substring(1, 20)

                                desc = mysql.escape(desc);
                                obj.title = mysql.escape(title);
                                obj.slug = mysql.escape(slug);
                                obj.body = mysql.escape(body);
                                obj.description = mysql.escape(description);
                                obj.media = mysql.escape(media);
                                obj.link = mysql.escape(link);
                                obj.category_id = mysql.escape(category_id);
                                obj.source_id = mysql.escape(source_id);
                                obj.date = mysql.escape(date);
                                obj.desc = mysql.escape(desc);
                                obj.surce = mysql.escape(iterator.source);
                                obj.lang = mysql.escape(iterator.lang);
                                obj.source_slug = mysql.escape(iterator.image);
                                obj.country = mysql.escape(iterator.name);

                                news.push(obj);
                            }
                        });
                    }


                } else {

                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + rows[index].id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key + "Filter"] = dataPath[i].data_filter;

                    }
                    if (arr['baseUrl']) {
                        let links = [];
                        await Promises
                        // Use this to iterate serially
                            .each($(arr['*']).get(), async function(elem) {
                            // let newObj= {};
                            let link = $(elem).attr('href');
                            if (link) {
                                if (link.substring(0, 4) == "http") {

                                } else {
                                    link = arr['baseUrl'] + link;
                                    links.push(link);
                                }

                            }

                            // let title = $(elem).text();
                            // title=title.trim();
                            // let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            //     slug = slug.trim();
                            //     slug = slug.split(' ').join('_');

                            // // newObj.link=link;
                            // newObj.title=title
                            // newObj.slug=slug;

                            // exLinks.push(newObj)

                        }).then(async function() {

                            for (var indexs = 0; indexs < links.length; indexs++) {
                                let singleNews = [];
                                let obj = {};
                                let linkChecked = checkLinks.includes(links[indexs]);
                                if (!linkChecked) {
                                    const responseAgain = await axios.get(links[indexs]);
                                    if (responseAgain.status === 200) {

                                        const htmlAgain = responseAgain.data;
                                        const $Again = cheerio.load(htmlAgain);
                                        let title;
                                        let slug;

                                        if (!title) {
                                            title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.split(' ').join('_');
                                        }

                                        let media = $Again(arr['media']).attr('src');

                                        if (!media) {
                                            media = $Again(arr['media']).attr('content');
                                        }
                                        if (media) {
                                            if (media.substring(0, 5) == "http:") {
                                                media = media.replace("http:", "https:");
                                            }
                                        }

                                        let body = '';

                                        if ($Again(arr['body']).find('p').text() != '') {
                                            $Again(arr['body']).find('p').each(function() {
                                                var $el = $(this);
                                                body = body + `<p>${$el.text()} </p>`;
                                            });
                                        } else {
                                            body = $Again(arr['body']).html();

                                        }


                                        // let body = $Again(arr['body']);
                                        if (body != null && body != '') {

                                            let newCheerio = cheerio.load(body, { decodeEntities: false });
                                            let bodyFil = arr["bodyFilter"].split('|');
                                            for (const filter of bodyFil) {
                                                newCheerio(filter).remove();

                                            }
                                            newCheerio('script').remove();
                                            newCheerio('style').remove();
                                            newCheerio('svg').remove();
                                            newCheerio('button').remove();
                                            newCheerio('video').remove();


                                            body = newCheerio('body').html();
                                            let bodyHtml = cheerio.load(body);
                                            let desc = bodyHtml.text();
                                            desc = desc.replace(/<[^>]*>?/gm, '');

                                            let description = desc.substring(0, 200);

                                            let pub_date = new Date();
                                            let date = mysql.escape(pub_date);
                                            date = date.substring(1, 20);

                                            desc = mysql.escape(desc);
                                            obj.title = mysql.escape(title);
                                            obj.slug = mysql.escape(slug);
                                            obj.body = mysql.escape(body);
                                            obj.description = mysql.escape(description);
                                            obj.media = mysql.escape(media);
                                            obj.link = mysql.escape(links[indexs]);
                                            obj.category_id = mysql.escape(category_id);
                                            obj.source_id = mysql.escape(source_id);
                                            obj.date = mysql.escape(date);
                                            obj.desc = mysql.escape(desc);
                                            // console.log(body)
                                            obj.surce = mysql.escape(iterator.source);
                                            obj.lang = mysql.escape(iterator.lang);
                                            obj.source_slug = mysql.escape(iterator.image);
                                            obj.country = mysql.escape(iterator.name);
                                            news.push(obj);
                                        }
                                    }
                                }

                            }
                        });


                    } else {
                        let links = [];
                        let exLinks = [];
                        await Promises
                        // Use this to iterate serially
                            .each($(arr['*']).get(), async function(elem) {

                            let newObj = {};
                            let link = $(elem).attr('href');
                            links.push(link);
                            let title = $(elem).text();
                            title = title.trim();
                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.trim();
                            slug = slug.split(' ').join('_');

                            newObj.link = link;
                            newObj.title = title;
                            newObj.slug = slug;

                            exLinks.push(newObj)

                        }).then(async function() {
                            for (const myData of exLinks) {
                                // console.log(myData)
                                let singleNews = [];
                                let obj = {};
                                let linkChecked = checkLinks.includes(myData.link);
                                if (!linkChecked) {
                                    const responseAgain = await axios.get(myData.link);
                                    if (responseAgain.status === 200) {
                                        const htmlAgain = responseAgain.data;
                                        const $Again = cheerio.load(htmlAgain);
                                        let title;
                                        let slug;
                                        if (myData.title.length > 0 && myData.title != undefined) {
                                            title = myData.title;
                                            slug = myData.slug;
                                        } else {
                                            title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.split(' ').join('_');
                                        }

                                        let media = $Again(arr['media']).attr('src');

                                        if (!media) {
                                            media = $Again(arr['media']).attr('content');
                                        }
                                        if (media) {
                                            if (media.substring(0, 5) == "http:") {
                                                media = media.replace("http:", "https:");
                                            }
                                        }
                                        if (source_id == 84) {
                                            title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.split(' ').join('_');
                                        }
                                        let body = $Again(arr['body']).html();
                                        if (body != null) {

                                            let newCheerio = cheerio.load(body, { decodeEntities: false });
                                            if (arr["bodyFilter"] != null) {
                                                let bodyFil = arr["bodyFilter"].split('|');
                                                for (const filter of bodyFil) {
                                                    newCheerio(filter).remove();

                                                }

                                            }

                                            newCheerio('script').remove();
                                            newCheerio('style').remove();

                                            newCheerio('svg').remove();
                                            newCheerio('button').remove();
                                            newCheerio('video').remove();


                                            body = newCheerio('body').html();
                                            let bodyHtml = cheerio.load(body);
                                            let desc = bodyHtml.text();
                                            desc = desc.replace(/<[^>]*>?/gm, '');

                                            let description = desc.substring(0, 200);

                                            let pub_date = new Date();
                                            let date = mysql.escape(pub_date);
                                            date = date.substring(1, 20);

                                            desc = mysql.escape(desc);
                                            obj.title = mysql.escape(title);
                                            obj.slug = mysql.escape(slug);
                                            obj.body = mysql.escape(body);
                                            obj.description = mysql.escape(description);
                                            obj.media = mysql.escape(media);
                                            obj.link = mysql.escape(myData.link);
                                            obj.category_id = mysql.escape(category_id);
                                            obj.source_id = mysql.escape(source_id);
                                            obj.date = mysql.escape(date);
                                            obj.desc = mysql.escape(desc);
                                            obj.surce = mysql.escape(iterator.source);
                                            obj.lang = mysql.escape(iterator.lang);
                                            obj.source_slug = mysql.escape(iterator.image);
                                            obj.country = mysql.escape(iterator.name);
                                            news.push(obj);
                                        }
                                    }
                                }

                            }

                        });



                    }

                }

            }
        }
    } catch (err) {
        errors.push(err)
    }
    news = [...new Set(news)];

    for (const element of news) {

        try {
            const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,source,lang,source_slug,country) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.surce + ', ' + element.lang + ', ' + element.source_slug + ', ' + element.country + ')');
            const updateSource = await query(`UPDATE source SET updated_at = current_timestamp WHERE id=${element.source_id}`)


        } catch (err) {
            errors.push(err)

        }

    }

    res.send({ news, errors })






});

module.exports = router;