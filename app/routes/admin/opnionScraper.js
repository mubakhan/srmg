let express = require('express'),
    axios = require('axios'),
    cheerio = require('cheerio'),
    router = express.Router();
const util = require('util');
const mysql = require('mysql');
const query = require('../../factory/query');
let Promises = require('bluebird');

router.get('/scrape_opinion/:id', async (req, res) => {

    let opinion_urls = await query(`SELECT opinion_url.*, opinion_author.name,source.source FROM opinion_url inner join opinion_author on opinion_url.opinion_author_id = opinion_author.id  inner JOIN opinion_source as source on source.id = opinion_url.source_id WHERE opinion_url.source_id=${req.params.id}`);
    let news = [];
    let getLinks = await query(`SELECT link FROM opinions WHERE source_id = ${req.params.id} ORDER BY id DESC LIMIT 50`);
    let checkLinks = []
    if(getLinks.length > 0){
    for (const iterator of getLinks) {

        checkLinks.push(iterator.link)
    }
}

    for (const url of opinion_urls) {
        const response = await axios.get(url.url);
        if (response.status === 200) {
            const html = response.data;
            const $ = cheerio.load(html, {
                xmlMode: true
            });

            const dataPath = await query(`SELECT * FROM opinions_datapath WHERE opinionUrl_id = ${url.id}`);
            let arr = [];

            for (let i = 0; i < dataPath.length; i++) {
                let key = dataPath[i].data_name;
                arr[key] = dataPath[i].data_path;
            }

            if (arr['baseUrl']) {

                let links = [];
                await Promises
                    // Use this to iterate serially
                    .each($(arr['*']).get(), async function (elem) {

                        // let newObj= {};

                        let link = $(elem).attr('href');
                        if (link) {
                            if (link.substring(0, 4) == "http") {

                            } else {
                                link = arr['baseUrl'] + link;
                                links.push(link);
                            }

                        }

                    }).then(async function () {
                        for (var indexs = 0; indexs < links.length; indexs++) {
                            let singleNews = [];
                            let obj = {};
                            let linkChecked = checkLinks.includes(links[indexs]);
                            if (!linkChecked) {
                                const responseAgain = await axios.get(links[indexs]);
                                if (responseAgain.status === 200) {
                                    const htmlAgain = responseAgain.data;
                                    const $Again = cheerio.load(htmlAgain);
                                    let title;
                                    let slug;
                                    console.log(title)
                                    if (!title) {
                                        title = $Again(arr['title']).attr('content');
                                        title = title.trim();
                                        slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                        slug = slug.split(' ').join('_');
                                    }

                                    let media = $Again(arr['media']).attr('src');
                                    let author = $Again(arr['author']).attr('content');
                                    if (!author) {
                                        author = $Again(arr['author']).text();
                                    }
                                    if (!media) {

                                        media = $Again(arr['media']).attr('content');

                                    }
                                    if(media){
                                        if (media.substring(0, 5) == "http:") {
                                            media = media.replace("http:", "https:");
                                        }
                                    }
                                   
                                    let body = $Again(arr['body']).html();
                                    let desc;
                                    let description;

                                    desc = $Again(arr['desc']).attr('content')
                                    description = $Again(arr['desc']).attr('content')

                                    if (!author) {
                                        author = url.source;
                                    }
                                    if (url.title == 1) {
                                        title = $Again(arr['title']).attr('content');
                                        title = title.trim();
                                        slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                        slug = slug.split(' ').join('_');
                                    }
                                    if (body != null) {

                                        let newCheerio = cheerio.load(body, { decodeEntities: false });
                                        newCheerio('script').remove();
                                        newCheerio('style').remove();
                                        newCheerio('head').remove();


                                        body = newCheerio('body').html();
                                        let bodyHtml = cheerio.load(body);
                                        bodyHtml('img').remove();
                                        if (!desc) {
                                            desc = bodyHtml.text();
                                            desc = desc.replace(/<[^>]*>?/gm, '');
                                            description = desc.substring(0, 200);
                                        }

                                        author = author.replace(' | ', ',');
                                        if(author.toLowerCase().includes("by ")){
                                            author = author.replace('by ','');
                                        }

                                        let pub_date = new Date();
                                        let date = mysql.escape(pub_date);
                                        date = date.substring(1, 20);

                                        desc = mysql.escape(desc);
                                        obj.title = mysql.escape(title);
                                        obj.slug = mysql.escape(slug);
                                        obj.body = mysql.escape(body);
                                        obj.description = mysql.escape(description);
                                        obj.media = mysql.escape(media);
                                        obj.link = mysql.escape(links[indexs]); obj.date = mysql.escape(date);
                                        obj.source_id = mysql.escape(url.source_id);
                                        obj.author_id = mysql.escape(url.opinion_author_id);
                                        obj.author = mysql.escape(author);
                                        obj.desc = mysql.escape(desc);
                                        news.push(obj);


                                    }
                                }
                            }
                        }
                    });


            } else {
                let links = [];
                let exLinks = [];
                try {
                    await Promises
                        // Use this to iterate serially
                        .each($(arr['*']).get(), async function (elem) {

                            let newObj = {};
                            let link = $(elem).first().attr('href');
                            links.push(link);

                            let title = $(elem).first().text();
                            title = title.trim();
                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.trim();
                            slug = slug.split(' ').join('_');

                            newObj.link = link;
                            newObj.title = title;
                            newObj.slug = slug;
                            exLinks.push(newObj)

                        }).then(async function () {

                            for (const myData of exLinks) {
                                console.log(myData)
                                let singleNews = [];
                                let obj = {};
                                let linkChecked = checkLinks.includes(myData.link);
                                if (!linkChecked) {
                                    try {
                                        const responseAgain = await axios.get(myData.link);

                                        if (responseAgain.status === 200) {
                                            const htmlAgain = responseAgain.data;
                                            const $Again = cheerio.load(htmlAgain);
                                            let title;
                                            let slug;
                                            if (myData.title.length > 0 && myData.title != undefined) {
                                                title = myData.title;
                                                slug = myData.slug;
                                            } else {
                                                title = $Again(arr['title']).attr('content');
                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }

                                            let media = $Again(arr['media']).attr('src');
                                            let author = $Again(arr['author']).attr('content');
                                            if (!author) {
                                                author = $Again(arr['author']).text();
                                            }
                                            if (!media) {
                                                media = $Again(arr['media']).attr('content');
                                            }
                                            if (media) {
                                                if (media.substring(0, 5) == "http:") {
                                                    media = media.replace("http:", "https:");
                                                }
                                            }
                                            let body = $Again(arr['body']).html();
                                            let desc;
                                            let description;
                                            console.log(body)

                                            desc = $Again(arr['desc']).attr('content')
                                            description = $Again(arr['desc']).attr('content')
                                            if (!author) {
                                                author = url.source;
                                            }
                                            if (url.title == 1) {
                                                title = $Again(arr['title']).attr('content');
                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }
                                            if (body != null) {

                                                let newCheerio = cheerio.load(body, { decodeEntities: false });
                                                newCheerio('script').remove();
                                                newCheerio('style').remove();
                                                newCheerio('head').remove();


                                                body = newCheerio('body').html();
                                                let bodyHtml = cheerio.load(body);
                                                bodyHtml('img').remove();
                                                if (!desc) {
                                                    desc = bodyHtml.text();
                                                    desc = desc.replace(/<[^>]*>?/gm, '');
                                                    description = desc.substring(0, 200);
                                                }

                                                author = author.replace(' | ', ',');
                                                if(author.toLowerCase().includes("by ")){
                                                    author = author.replace('by ','');
                                                }

                                                let pub_date = new Date();
                                                let date = mysql.escape(pub_date);
                                                date = date.substring(1, 20);

                                                desc = mysql.escape(desc);
                                                obj.title = mysql.escape(title);
                                                obj.slug = mysql.escape(slug);
                                                obj.body = mysql.escape(body);
                                                obj.description = mysql.escape(description);
                                                obj.media = mysql.escape(media);
                                                obj.link = mysql.escape(myData.link);
                                                obj.date = mysql.escape(date);
                                                obj.source_id = mysql.escape(url.source_id);
                                                obj.author_id = mysql.escape(url.opinion_author_id);
                                                obj.author = mysql.escape(author);
                                                obj.desc = mysql.escape(desc);
                                                news.push(obj);


                                            }
                                        }
                                    } catch (error) {
                                        console.log(error)
                                    }

                                }



                            }

                        });

                } catch (error) {
                    console.log(error)
                }
            }




        }


    }

    for (const element of news) {
        try {
            let opinionSave = await query(`INSERT INTO opinions (title, slug, link, description, body, media, source_id, author_id,author, pub_date) VALUES (${element.title},${element.slug},${element.link},${element.description},${element.body},${element.media},${element.source_id},${element.author_id},${element.author},${element.date})`)

        } catch (error) {
            console.log(error)
        }
    }



    res.send(news)

});





module.exports = router;