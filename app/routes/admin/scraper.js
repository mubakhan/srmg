let express = require('express'),
    axios = require('axios'),
    cheerio = require('cheerio'),
    router = express.Router();
const util = require('util');
const mysql = require('mysql');
const query = require('../../factory/query');
let Promises = require('bluebird');

let qs=require('qs');

let fs = require('fs');
const puppeteer = require('puppeteer');

router.get('/add-opinion',(req, res)=>{
    res.render('opinion')
})

router.get('/add-region', (req, res) => {
    res.render('region');
})
router.get('/add-country', (req, res) => {
    res.render('country');
})
router.get('/add-keyword', (req, res) => {
    res.render('keyword');
})
router.get('/add-keyword-category', (req, res) => {
    res.render('keyword-cat');
})
router.get('/source', (req, res) => {
    console.log("Rach");
    res.render('source');
});

router.get('/url', (req, res) => {
    res.render('source_url');
});
router.get('/getRegion', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `regions`');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/getKeywordCat', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `keyword_category`');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.post('/addRegion', async (req, res) => {
    try {
        const rows = await query('INSERT INTO `regions`(`name`, `description`) VALUES ("' + req.body.name + '","' + req.body.desc + '")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});

router.get('/getCountry', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `country`');
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/getkeywords', async (req, res) => {
    try {
        const rows = await query(`SELECT keywords.id,keywords.word,keyword_category.name FROM keywords INNER JOIN keyword_category on keywords.keyword_catId = keyword_category.id`)
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});
router.get('/get-keyword-cat', async (req, res) => {
    try {
        const rows = await query(`SELECT * FROM keyword_category`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }
});

router.post('/add-keyword-cate', async (req, res) => {
    try {
        const rows = await query('INSERT INTO keyword_category (name) VALUES ("' + req.body.name + '")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});
router.post('/addCountry', async (req, res) => {
    try {
        const rows = await query('INSERT INTO `country`(`region_id`, `name`) VALUES ("' + req.body.region + '","' + req.body.name + '")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});
router.post('/addKeyword', async (req, res) => {
    try {
        const rows = await query('INSERT INTO `keywords`(`keyword_catId`, `word`) VALUES ("' + req.body.category + '","' + req.body.name + '")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});
router.post('/addSources', async (req, res) => {
    if(req.files && req.files.logo){
        try {
            var dir = './public/sources/';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            let sampleFile = req.files.logo;
            var imagename = dir + req.body.slug+".png" ;
            await sampleFile.mv(imagename, function (err) {
                if (err)
                    return res.status(500).send(err);
            });
           
            imagename = imagename.substring(9);
            const rows = await query('INSERT INTO `source`(`source`, `slug`, `website`, `lang`, `country_id`, `default`,image) VALUES ("' + req.body.name + '","' + req.body.slug + '","' + req.body.url + '","' + req.body.lng + '","' + req.body.countryId + '","true","'+imagename+'")');
            res.send({ success: "true" });
        } catch (err) {
            console.log(err);
        }
    }
  
});

router.get('/getSourceUrls/:sourceId', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `source_url` WHERE `source_id` =' + req.params.sourceId);
        res.render('source', { sourceUrls: rows });

    } catch (err) {
        console.log(err);
    }
});

router.post('/addSourceUrl', async (req, res) => {
    try {
        const rows = await query('INSERT INTO `source_url`(`url`, `encoding`, `category_id`, `source_id`) VALUES ("' + req.body.url + '","' + req.body.encoding + '","' + req.body.catId + '","' + req.body.sourceId + '")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});

router.get('/getSourceDataPaths/:sourceId/sourceId/:sourceUrlId', async (req, res) => {

    try {
        let sourceId = req.params.sourceId;
        let sourceUrls = await query('SELECT * FROM `source_url` WHERE `source_id` =' + sourceId);
        if (sourceUrls.length > 0) {


            const relatedSourceUrlsDataPath = await query('SELECT * FROM `source_data_path` WHERE `sourceUrl_id` = ' + sourceUrls[0].id);
            if (relatedSourceUrlsDataPath.length > 0) {
                let relatedSourceUrlId = sourceUrls[0].id;
                const rows = await query('SELECT * FROM `source_data_path` WHERE `sourceUrl_id` = ' + req.params.sourceUrlId);

                res.render('source_url', { sourceUrlsDataPaths: rows, relatedSourceUrlId });
            } else {
                let relatedSourceUrlId = null;

                const rows = await query('SELECT * FROM `source_data_path` WHERE `sourceUrl_id` = ' + req.params.sourceUrlId);
                res.render('source_url', { sourceUrlsDataPaths: rows, relatedSourceUrlId });
            }

        } else {
            console.log('hi');
            const rows = await query('SELECT * FROM `source_data_path` WHERE `sourceUrl_id` = ' + req.params.sourceUrlId);
            res.render('source_url', { sourceUrlsDataPaths: rows });
        }



    } catch (err) {
        console.log(err);
    }
});

router.post('/copyDataPaths', async (req, res) => {

    let relData = req.body.relatedData;
    let sourceUrlId = req.body.sourceUrlId;
    const relatedSourceUrlsDataPath = await query('SELECT * FROM `source_data_path` WHERE `sourceUrl_id` = ' + relData);
    for (const iterator of relatedSourceUrlsDataPath) {
        const rows = await query("INSERT INTO `source_data_path`(`data_name`, `data_path`, `data_param`, `data_filter`, `data_type`, `sourceUrl_id`) VALUES ('" + iterator.data_name + "',' " + iterator.data_path + "','" + iterator.data_param + "','" + iterator.data_filter + "','" + iterator.data_type + "','" + sourceUrlId + "')");
    }

    res.send({ success: "true" });

    // var o = new Function("return " + relData)();
    //         console.log(o)
    // var jsonObject = (new Function( "return " + relData ) )() ;
    // console.log(jsonObject);
});

router.post('/addSourceDataPath', async (req, res) => {
    try {
        console.log(mysql.escape(req.body.path))

        const rows = await query("INSERT INTO `source_data_path`(`data_name`, `data_path`, `data_param`, `data_filter`, `data_type`, `sourceUrl_id`) VALUES ('" + req.body.name + "',' " + req.body.path + "','" + req.body.param + "','" + req.body.filter + "','" + req.body.type + "','" + req.body.id + "')");
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});

router.post('/sourceDataPaths/update/', async (req, res) => {
    try {
        const rows = await query("UPDATE `source_data_path` SET `data_name`='" + req.body.name + "',`data_path`='" + req.body.path + "',`data_param`='" + req.body.param + "',`data_filter`='" + req.body.filter + "',`data_type`='" + req.body.type + "' WHERE `id`=' " + req.body.id + "'");
        res.send({ success: "true" });

    } catch (err) {
        console.log(err);
    }
});


router.get('/getSources', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `source`');
        res.send({ sources: rows });
    } catch (err) {
        console.log(err);
    }
});

// get all opinions
router.get('/getOpinion', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM `opinion_source`');
        res.send({ sources: rows });
    } catch (err) {
        console.log(err);
    }
});

// add new opinions
router.post('/addOpinion', async (req, res) => {
    if(req.files && req.files.logo){
        try {
            var dir = './public/opinions/';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            let sampleFile = req.files.logo;
            var imagename = dir + req.body.slug+".png" ;
            await sampleFile.mv(imagename, function (err) {
                if (err)
                    return res.status(500).send(err);
            });
           
            imagename = imagename.substring(9);
            const rows = await query('INSERT INTO `opinion_source`(`source`, `slug`, `website`, `lang`, `country_id`, `default`,image) VALUES ("' + req.body.name + '","' + req.body.slug + '","' + req.body.url + '","' + req.body.lng + '","' + req.body.countryId + '","true","'+imagename+'")');
            res.send({ success: "true" });
        } catch (err) {
            console.log(err);
        }
    }
  
});


// getOpinionUrls  by ids
router.get('/getOpinionUrls/:sourceId', async(req, res)=>{
    try {
        const rows = await query('SELECT * FROM `opinion_url` WHERE `source_id` =' + req.params.sourceId);
        res.render('opinionurl', { sourceUrls: rows });

    } catch (err) {
        console.log(err);
    }
})

// add opinion source 
router.post('/addOpinionUrl', async (req, res) => {
    try {
        const rows = await query('INSERT INTO `opinion_url`(`url`, `opinion_author_id`, `source_id`, `title`) VALUES ("' + req.body.url + '","1","' + req.body.sourceId + '","0")');
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});

// getopiniondata path 
router.get('/getOpinionDataPaths/:sourceId/sourceId/:sourceUrlId', async (req, res) => {

    try {
        let sourceId = req.params.sourceId;
        let sourceUrls = await query('SELECT * FROM `opinion_url` WHERE `source_id` =' + sourceId);
        if (sourceUrls.length > 0) {


            const relatedSourceUrlsDataPath = await query('SELECT * FROM `opinions_datapath` WHERE `opinionUrl_id` = ' + sourceUrls[0].id);
            if (relatedSourceUrlsDataPath.length > 0) {
                let relatedSourceUrlId = sourceUrls[0].id;
                const rows = await query('SELECT * FROM `opinions_datapath` WHERE `opinionUrl_id` = ' + req.params.sourceUrlId);

                res.render('opiniondata', { sourceUrlsDataPaths: rows, relatedSourceUrlId });
            } else {
                let relatedSourceUrlId = null;

                const rows = await query('SELECT * FROM `opinions_datapath` WHERE `opinionUrl_id` = ' + req.params.sourceUrlId);
                res.render('opiniondata', { sourceUrlsDataPaths: rows, relatedSourceUrlId });
            }

        } else {
            console.log('hi');
            const rows = await query('SELECT * FROM `opinions_datapath` WHERE `opinionUrl_id` = ' + req.params.sourceUrlId);
            res.render('opiniondata', { sourceUrlsDataPaths: rows });
        }



    } catch (err) {
        console.log(err);
    }
});

// post opinion data
router.post('/addOpinionDataPath', async (req, res) => {
    try {
        console.log(mysql.escape(req.body.path))

        const rows = await query("INSERT INTO `opinions_datapath`(`data_name`, `data_path`, `data_param`, `data_filter`, `data_type`, `opinionUrl_id`) VALUES ('" + req.body.name + "',' " + req.body.path + "','" + req.body.param + "','" + req.body.filter + "','" + req.body.type + "','" + req.body.id + "')");
        res.send({ success: "true" });
    } catch (err) {
        console.log(err);
    }
});


// copy opinion data
router.post('/copyOpinionDataPaths', async (req, res) => {

    let relData = req.body.relatedData;
    let sourceUrlId = req.body.sourceUrlId;
    const relatedSourceUrlsDataPath = await query('SELECT * FROM `opinions_datapath` WHERE `opinionUrl_id` = ' + relData);
    for (const iterator of relatedSourceUrlsDataPath) {
        const rows = await query("INSERT INTO `opinions_datapath`(`data_name`, `data_path`, `data_param`, `data_filter`, `data_type`, `opinionUrl_id`) VALUES ('" + iterator.data_name + "',' " + iterator.data_path + "','" + iterator.data_param + "','" + iterator.data_filter + "','" + iterator.data_type + "','" + sourceUrlId + "')");
    }

    res.send({ success: "true" });
});

// edit opinion datapath
router.post('/opinionDataPaths/update/', async (req, res) => {
    try {
        const rows = await query("UPDATE `opinions_datapath` SET `data_name`='" + req.body.name + "',`data_path`='" + req.body.path + "',`data_param`='" + req.body.param + "',`data_filter`='" + req.body.filter + "',`data_type`='" + req.body.type + "' WHERE `id`=' " + req.body.id + "'");
        res.send({ success: "true" });

    } catch (err) {
        console.log(err);
    }
});


router.get('/scrapeNews/:sourceID', async (req, res) => {
    let news = []
    let source_id = req.params.sourceID;
    try {

        const rows = await query('SELECT * FROM `source_url` WHERE source_id = ' + source_id);
        const browser = await puppeteer.launch({headless: true,args: ['--no-sandbox']});
        const page = await browser.newPage();
        await page.setViewport({ width: 1300, height: 1000 });
        await page.setDefaultNavigationTimeout(0); 
        await page.setRequestInterception(true);

        page.on('request', (req) => {
            if(req.resourceType() === 'image'){
                req.abort();
            }
            else {
                req.continue();
            }
        });
for (const index of rows) {
// }
        // for (let index = 0; index < rows.length; index++) {
            let response;
          try {
            await page.goto(index.url, {waitUntil: 'networkidle2'});
          } catch (error) {
              
          }
          try {
             response = await page.evaluate(() => document.documentElement.outerHTML);

          } catch (error) {
              
          }
                    // await page.evaluate(scrollToBottom);
                    // await page.waitFor(3000);

            

            // const response = await axios.get(rows[index].url);
            let category_id = index.category_id;
            if (response) {
        const $ = cheerio.load(response, {
                    xmlMode: true
                });



            // if (response.status === 200) {
            //     const html = response.data;
            //     const $ = cheerio.load(html, {
            //         xmlMode: true
            //     });

                if (index.encoding == "xml") {
                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key+"Filter"] = dataPath[i].data_filter;

                    }
                    if (dataPath[1].data_param == "html") {
                        let newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            let link = $(elem).find(arr['url']).text();
                            newLinks.push(link);

                        });
                        for (let n = 0; n < newLinks.length; n++) {
                            let singleNews = [];
                            let obj = {};
                            if (n < 11) {
                                const responseAgain = await axios.get(newLinks[n]);
                                if (responseAgain.status === 200) {
                                    const htmlAgain = responseAgain.data;
                                    const $Again = cheerio.load(htmlAgain);
                                    let title = $Again(arr['title']).attr('content');
                                    title = title.trim();
                                    let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                    slug = slug.trim();
                                    slug = slug.split(' ').join('_');

                                    let media = $Again(arr['media']).attr('src');
                                    if (!media) {
                                        media = $Again(arr['media']).attr('content');
                                    }
                                    let body = $Again(arr['body']).html();

                                    if (body != null) {
                                        let newCheerio = cheerio.load(body, { decodeEntities: false });
                                        let bodyFil =arr["bodyFilter"].split('|');
                                        console.log(bodyFil)
                                        for (const filter of bodyFil) {
                                            newCheerio(filter).remove();

                                        } 
                                        newCheerio('script').remove();
                                        newCheerio('style').remove();
                                        newCheerio('div.single-related').remove();
                                        newCheerio('head').remove();

                                        body = newCheerio('body').html();

                                        let bodyHtml = cheerio.load(body);
                                        let desc = bodyHtml.text();
                                        desc=desc.replace(/<[^>]*>?/gm, '');

                                        let description = desc.substring(0, 200)

                                        let pub_date = new Date();
                                        let date = mysql.escape(pub_date);
                                        date = date.substring(1, 20);

                                        desc = mysql.escape(desc);
                                        obj.title = mysql.escape(title);
                                        obj.slug = mysql.escape(slug);
                                        obj.body = mysql.escape(body);
                                        obj.description = mysql.escape(description);
                                        obj.media = mysql.escape(media);
                                        obj.link = mysql.escape(newLinks[n]);
                                        obj.category_id = mysql.escape(category_id);
                                        obj.source_id = mysql.escape(source_id);
                                        obj.date = mysql.escape(date);
                                        obj.desc = mysql.escape(desc);
                                        console.log(obj)
                                        news.push(obj);
                                    }

                                }
                            }

                        }


                    } else {
                        $(arr['*']).each(async (i, elem) => {
                            let singleNews = [];
                            let obj = {};
                            let title = $(elem).find(arr['title']).text();
                            title = title.trim();

                            let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                            slug = slug.split(' ').join('_');
                            let body = $(elem).find(arr['body']).text();

                            // let body = $(elem).find('content\\:encoded').text();

                            let bodyHtml = cheerio.load(body);
                            let desc = bodyHtml.text();
                            desc=desc.replace(/<[^>]*>?/gm, '');

                            let description = desc.substring(0, 200);

                            let link = $(elem).find(arr['url']).text();
                            let media = $(elem).find(arr['media']).attr('url');
                            if (!media) {
                                media = $(elem).find(arr['media']).attr('src');
                                if (!media) {
                                    let newHtm = cheerio.load(body);
                                    media = newHtm('img').attr('src');

                                }

                            }


                            let pub_date = new Date();
                            let date = mysql.escape(pub_date);
                            date = date.substring(1, 20)

                            desc = mysql.escape(desc);
                            obj.title = mysql.escape(title);
                            obj.slug = mysql.escape(slug);
                            obj.body = mysql.escape(body);
                            obj.description = mysql.escape(description);
                            obj.media = mysql.escape(media);
                            obj.link = mysql.escape(link);
                            obj.category_id = mysql.escape(category_id);
                            obj.source_id = mysql.escape(source_id);
                            obj.date = mysql.escape(date);
                            obj.desc = mysql.escape(desc);
                            console.log(obj)


                            news.push(obj);
                        });
                    }


                } else {

                    const dataPath = await query('SELECT * FROM `source_data_path` WHERE sourceUrl_id = ' + index.id);
                    let arr = [];

                    for (let i = 0; i < dataPath.length; i++) {
                        let key = dataPath[i].data_name;
                        arr[key] = dataPath[i].data_path;
                        arr[key+"Filter"] = dataPath[i].data_filter;

                    }


                    if (arr['baseUrl']) {
                        let links = [];                
                        await Promises
                            // Use this to iterate serially
                            .each($(arr['*']).get(), async function (elem) {

                                // let newObj= {};

                                let link = $(elem).attr('href');
                                console.log(link)
                                if (link) {
                                    if (link.substring(0, 4) == "http") {

                                    } else {
                                        link = arr['baseUrl'] + link;
                                        links.push(link);
                                    }

                                }
                                // let title = $(elem).text();
                                // title=title.trim();
                                // let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                //     slug = slug.trim();
                                //     slug = slug.split(' ').join('_');

                                // // newObj.link=link;
                                // newObj.title=title
                                // newObj.slug=slug;

                                // exLinks.push(newObj)
                                console.log(links)

                            }).then(async function () {
                                for (var indexs = 0; indexs < links.length; indexs++) {
                                    let singleNews = [];
                                    let obj = {};
                                    
                                    // const responseAgain = await axios.get(links[indexs]);
                                   
                                    await page.goto(links[indexs], {waitUntil: 'networkidle2'});
                                    // await page.evaluate(scrollToBottom);
                                    // await page.waitFor(3000);
                
                                let responseAgain = await page.evaluate(() => document.documentElement.outerHTML);
                                if (responseAgain) {
    
                                // if (responseAgain.status === 200) {

                                        // const htmlAgain = responseAgain.data;
                                        const $Again = cheerio.load(responseAgain);
                                        let title;
                                        let slug;

                                        if (!title) {
                                            title = $Again(arr['title']).attr('content');
                                            if(!title){
                                                title = $Again(arr['title']).text();

                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }else{
                                                title = title.trim();
                                                slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                                slug = slug.split(' ').join('_');
                                            }
                                           
                                        }

                                        let media = $Again(arr['media']).attr('src');

                                        if (!media) {
                                            media = $Again(arr['media']).attr('content');
                                        }
                                        if (media) {
                                            if (media.substring(0, 5) == "http:") {
                                                media = media.replace("http:", "https:");
                                            }
                                        }
                                       
                                        let body = $Again(arr['body']).html();
                                        if (body != null) {

                                            let newCheerio = cheerio.load(body, { decodeEntities: false });
                                            let bodyFil =arr["bodyFilter"].split('|');
                                            for (const filter of bodyFil) {
                                                newCheerio(filter).remove();

                                            } 
                                            newCheerio('script').remove();
                                            newCheerio('style').remove();
                                            newCheerio('head').remove();


                                            body = newCheerio('body').html();
                                            let bodyHtml = cheerio.load(body);
                                            let desc = bodyHtml.text();
                                            desc=desc.replace(/<[^>]*>?/gm, '');

                                            let description = desc.substring(0, 200);

                                            let pub_date = new Date();
                                            let date = mysql.escape(pub_date);
                                            date = date.substring(1, 20);

                                            desc = mysql.escape(desc);
                                            obj.title = mysql.escape(title);
                                            obj.slug = mysql.escape(slug);
                                            obj.body = mysql.escape(body);
                                            obj.description = mysql.escape(description);
                                            obj.media = mysql.escape(media);
                                            obj.link = mysql.escape(links[indexs]);
                                            obj.category_id = mysql.escape(category_id);
                                            obj.source_id = mysql.escape(source_id);
                                            obj.date = mysql.escape(date);
                                            obj.desc = mysql.escape(desc);
                                            // console.log(body)

                                            news.push(obj);
                                        }
                                    }
                                }
                            });


                    } else {
                        let links = [];
                        let exLinks = [];

                        await Promises
                            // Use this to iterate serially
                            .each($(arr['*']).get(), async function (elem) {
                                let newObj = {};
                                let link = $(elem).attr('href');
                                links.push(link);
                                
                                let title = $(elem).text();
                                title = title.trim();
                                let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                slug = slug.trim();
                                slug = slug.split(' ').join('_');

                                newObj.link = link;
                                newObj.title = title;
                                newObj.slug = slug;

                                exLinks.push(newObj)

                            }).then(async function () {
                                for (const myData of exLinks) {
                                    // console.log(myData)
                                    let singleNews = [];
                                    let obj = {};
                                    await page.goto(myData.link, {waitUntil: 'networkidle2'});
                                    // await page.evaluate(scrollToBottom);
                                    // await page.waitFor(3000);
                
                                let responseAgain = await page.evaluate(() => document.documentElement.outerHTML);
                                    // const responseAgain = await axios.get(myData.link);
                                    if (responseAgain) {
                                        const htmlAgain = responseAgain;
                                        const $Again = cheerio.load(htmlAgain);
                                        let title;
                                        let slug;
                                        if (myData.title.length > 0 && myData.title != undefined) {
                                            title = myData.title;
                                            slug = myData.slug;
                                        } else {
                                            title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.split(' ').join('_');
                                        }

                                        let media = $Again(arr['media']).attr('src');

                                        if (!media) {
                                            media = $Again(arr['media']).attr('content');
                                        }
                                        if(media){
                                        if (media.substring(0, 5) == "http:") {
                                            media = media.replace("http:", "https:");
                                        }
                                        }
                                        if(source_id == 84){
                                            title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                            slug = slug.split(' ').join('_');
                                        }
                                        let body = $Again(arr['body']).html();
                                        if (body != null) {

                                            let newCheerio = cheerio.load(body, { decodeEntities: false });
                                            let bodyFil =arr["bodyFilter"].split('|');
                                            console.log(bodyFil)
                                            for (const filter of bodyFil) {
                                                newCheerio(filter).remove();

                                            } 
                                            newCheerio('script').remove();
                                            newCheerio('style').remove();
                                            newCheerio('head').remove();


                                            body = newCheerio('body').html();
                                            let bodyHtml = cheerio.load(body);
                                            bodyHtml('img').remove();
                                            let desc = bodyHtml.text();
                                            desc=desc.replace(/<[^>]*>?/gm, '');
                                            let description = desc.substring(0, 200);

                                            let pub_date = new Date();
                                            let date = mysql.escape(pub_date);
                                            date = date.substring(1, 20);
                                            
                                            desc = mysql.escape(desc);
                                            obj.title = mysql.escape(title);
                                            obj.slug = mysql.escape(slug);
                                            obj.body = mysql.escape(body);
                                            obj.description = mysql.escape(description);
                                            obj.media = mysql.escape(media);
                                            obj.link = mysql.escape(myData.link);
                                            obj.category_id = mysql.escape(category_id);
                                            obj.source_id = mysql.escape(source_id);
                                            obj.date = mysql.escape(date);
                                            obj.desc = mysql.escape(desc);
                                            news.push(obj);
                                            console.log(obj)

                                        }
                                    }
                                }

                            });



                    }

                }

            }
        }

     browser.close();

    } catch (err) {
        console.log(err);
    }
    let newNews = [];
    for (let index = 0; index < news.length; index++) {
        const element = news[index];
        let sentidata;
        let deepData;
        // try {
        //     let senti = await axios.get('http://192.64.113.8:5000/newshunt_senti?text=' + encodeURIComponent(element.desc));
        //     sentidata = senti;
        //     let newSenti = senti.data;
        //     if (newSenti != undefined && newSenti != null) {
        //         element.senti = mysql.escape(newSenti.sentences);
        //         element.sentiStatus = mysql.escape(newSenti.status);
        //         element.sentiNeg = mysql.escape(newSenti.Neg);
        //         element.sentiNeu = mysql.escape(newSenti.Neu);
        //         element.sentiPos = mysql.escape(newSenti.Pos);
        //     }
        // } catch (err) {


        // }
        // if (!sentidata) {
        //     element.senti = null;
        //     element.sentiStatus = null;
        //     element.sentiNeg = null;
        //     element.sentiNeu = null;
        //     element.sentiPos = null;
        // }
        // let sumaryData;

        // try {
        //     let sumary = await axios.get('http://192.64.113.8:5000/newshunt_summary?text=' + encodeURIComponent(element.desc));
        //     sumaryData =sumary.data;
        //     let summary = sumary.data;
        //     if (summary != undefined && summary != null) {
        //         element.summary = mysql.escape(summary);

        //     }


        // } catch (err) {
        //     // element.summary = null;

        //     console.log(err);
        // }
        // if (!sumaryData) {
        //     element.summary = null;

        // }
        // try {
        //     let deepAnalysis = await axios.get('http://192.64.113.8:5000/newshunt_deep?text=' + encodeURIComponent(element.desc));
        //     deepData=deepAnalysis;
        //     let deep = deepAnalysis.data;
        //     if (deep != undefined && deep != null) {

        //         element.deepSentences = mysql.escape(deep.sentences);
        //         element.deepStatus = mysql.escape(deep.status);
        //         element.deepInsult = mysql.escape(deep.insult);
        //         element.deepToxicity = mysql.escape(deep.toxicity);
        //         element.deepProfanity = mysql.escape(deep.profanity);
        //         element.deepexual = mysql.escape(deep.sexual);
        //         element.deeoIdentity = mysql.escape(deep.identity);
        //         element.deepFlirt = mysql.escape(deep.flirt);
        //         element.deepThreat = mysql.escape(deep.threat);
        //         element.DeepSevere = mysql.escape(deep.severe);



        //     }
        // } catch (err) {


        // }
        // if(!deepData){
        //     element.deepSentences = '';
        //     element.deepStatus = '';
        //     element.deepInsult = '';
        //     element.deepToxicity = '';
        //     element.deepProfanity = '';
        //     element.deepexual = '';
        //     element.deeoIdentity = '';
        //     element.deepFlirt = '';
        //     element.deepThreat = '';
        //     element.DeepSevere = '';
        // }
        newNews.push(element);
        // console.log(element)
        // try {
        //     const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,sentiment,sentiment_state,senti_neg,senti_neu,senti_pos,summary) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.senti + ', ' + element.sentiStatus + ', ' + element.sentiNeg + ', ' + element.sentiNeu + ', ' + element.sentiPos + ', ' + element.summary + ')');

        // } catch (err) {
        //     console.log(err)
        // }

        // try {
        //                     const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ')');

        //                 } catch (err) {
        //                     console.log(err)
        //                 }

    }

    res.send(newNews);






});


router.get('/mountKeyword', async (req, res) => {

    try {
        const keywords = await query(`SELECT * FROM keywords`);
        let newsWithkeyWord=[];
            const rows = await query(`SELECT * FROM news WHERE keyStatus = 0 ORDER BY id DESC LIMIT 50`);
            for (const news of rows) {
                for (const keyword of keywords) {
                    let title =news.title.toLowerCase();
                    let body = news.body.toLowerCase();
                    let newsId = news.id;
                    let keywordID =keyword.id;
                if(title.includes(keyword.word) || body.includes(keyword.word) ){
                let insertIN = await query(`INSERT INTO keyword_news(news_id, keyword_id) VALUES (${newsId},${keywordID})`)

                }
            }

        }

        const updateNews = await query(`UPDATE news SET keyStatus=1 WHERE keyStatus = 0 ORDER BY id DESC LIMIT 50`)

        res.send({sucess:"true"});

    } catch (err) {
        res.send(err);
    }

});
router.post('/getKeywordNews', async (req, res) => {
    let keywordId = req.body.keywordId;
    let temp = [];
    for (let element of keywordId) {
        temp.push(Number(element));
    }

    try {
        const news = await query(`SELECT n.id,n.title,n.slug,n.description,n.body,n.summary,n.media,n.url,n.pub_date,n.tags, n.tags,n.category_id,n.sub_category,n.sentiment,n.senti_pos,n.senti_neg,n.senti_neu ,n.sentiment_state,n.deep_analyze,n.deep_analyze_state,n.deep_toxicity,n.deep_insult,n.deep_profainty,n.deep_sexuality,n.deep_identity,n.deep_threat,n.deep_flitation,n.deep_severe_tox,s.source,s.lang,s.slug as sources_slug FROM news as n INNER JOIN source as s ON s.id = n.source_id INNER JOIN keyword_news as ky ON n.id = ky.news_id WHERE ky.keyword_id in (${temp}) ORDER BY n.id DESC`);
        
        res.send({news,sucess:"true"});

    } catch (err) {

    }

});

router.get('/getTokens',async(req,res)=>{
    const config = {
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded'
        }
      }
    const responseAgain = await axios.post('https://api.listhub.com/oauth2/token',qs.stringify({
        grant_type: "client_credentials",
        client_id: "public_sandbox",
        client_secret: "public_sandbox"
      }),config);
          
    console.log(responseAgain)
   

})

router.get('/updateSources',async(req,res)=>{


    const sources = await query(`SELECT * FROM source`);
    for (const source of sources) {
        let img =`sources/${source.slug}.png`
        let updateSource = await query(`UPDATE source SET image ="${img}" WHERE id = ${source.id}`) 
    }
res.send("done")

})
async function scrollToBottom() {
    await new Promise(resolve => {
      const distance = 100; // should be less than or equal to window.innerHeight
      const delay = 100;
      const timer = setInterval(() => {
        document.scrollingElement.scrollBy(0, distance);
        if (document.scrollingElement.scrollTop + window.innerHeight >= document.scrollingElement.scrollHeight) {
          clearInterval(timer);
          resolve();
        }
      }, delay);
    });
  }
module.exports = router;