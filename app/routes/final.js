let express = require('express'),
    router = express.Router();
const query = require('../factory/query');

router.get('/', (req, res) => {
    res.render('./final/index.html')
});

router.get('/getCount', async (req, res) => {
    try {
        //Count total
        let totalNews = await query(`SELECT COUNT(*) as newsCount FROM news`);
        let totalSources = await query(`SELECT COUNT(*) as sourceCount FROM source `);
        let totalCountry = await query(`SELECT COUNT(*) as countryCount FROM country `);
        let totalRegions = await query(`SELECT COUNT(*) as reginCount FROM regions `);aw
        res.send({ totalNews, totalSources, totalCountry, totalRegions });
    } catch (error) {
        res.send({ error });
    }

});

router.get('/get-region-country', async (req, res) => {
    const region = await query(`select * from regions`);
    res.send(region);
})
router.get('/get-country/:region', async (req, res) => {
    const country = await query(`select * from country where region_id =${req.params.region}`);
    res.send(country);
});
router.get('/get-news-by-region', async (req, res) => {
    try {
        const getNews = await query(`SELECT regions.name as region_name,(SELECT COUNT(news.id)) AS count FROM news INNER JOIN country ON country.name = news.country INNER JOIN regions ON regions.id = country.region_id GROUP BY region_name`);
        res.send(getNews);
    } catch (error) {
        res.send(error);
    }
})
router.get('/get-sources-by-region', async (req, res) => {
    try {
        const getSources = await query(`SELECT regions.name as region_name,(SELECT COUNT(source.id)) AS count FROM source INNER JOIN country ON country.id = source.country_id INNER JOIN regions ON regions.id = country.region_id GROUP BY region_name`);
        res.send(getSources);
    } catch (error) {
        res.send(error);
    }
})
router.get('/get-news-by-country', async (req, res) => {
    try {
        const getNewsByCountry = await query(`SELECT news.country as country_name,(SELECT COUNT(news.id)) AS count FROM news INNER JOIN country ON country.name = news.country GROUP BY country_name`);
        res.send(getNewsByCountry);
    } catch (error) {
        res.send(error);
    }
})
router.get('/get-sources-by-country', async (req, res) => {
    try {
        const getSourcesByCountry = await query(`SELECT country.name as country_name,(SELECT COUNT(source.country_id)) AS count FROM source INNER JOIN country ON country.id = source.country_id GROUP BY country_name`);
        res.send(getSourcesByCountry);
    } catch (error) {
        res.send(error);
    }
})
router.get('/get-news-source-by-filter/:regionId/:countryName', async (req, res) => {
    try {
        const filterNewsSource = await query(`SELECT source.source as source_name,country.name as country_name, regions.name as region_name,(SELECT COUNT(news.source_id)) AS count FROM news INNER JOIN source ON source.id = news.source_id INNER JOIN country on country.name = news.country INNER join regions ON country.region_id = regions.id WHERE regions.id = ${req.params.regionId} AND country.name='${req.params.countryName}' GROUP BY source_name`);
        res.send(filterNewsSource);
    } catch (error) {
        res.send(error);
    }
})
// keywords page 
router.get('/keywords', (req, res) => {
    res.render('./final/keyword.html')
})
router.get('/getkeywords', async (req, res) => {
    try {
        const getKeywords = await query(`SELECT * FROM keywords`);
        res.send(getKeywords);
    } catch (error) {
        res.send(error);
    }
})

// analysis page
router.get('/analysis', (req, res) => {
    res.render('./final/analysis.html')
})

module.exports = router;