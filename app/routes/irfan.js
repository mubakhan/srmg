
const express = require('express'),
    router = express.Router();
const query = require('../factory/query');

router.get('/newsByCountry/:countryName', async (req, res) => {
    let name = req.params.countryName;
    try {
        const country = await query("SELECT * FROM country WHERE name = '" + name + "'");

        if (country.length > 0) {
            for (let element of country) {
                const news = await query('select * from news where country_id =' + element.id + ' ORDER BY id DESC');
                res.send({ "Success": true, news });
            }

        } else {
            res.send({ "Success": false });
        }
        // res.send(rows);
    } catch (err) {
        console.log(err);
    }

});
router.get('/getDateNews/:date', async (req, res) => {
    let { date } = req.params;

    try {
        const news = await query(`select * from news n WHERE n.pub_date LIKE '${date}%' ORDER BY n.id DESC`);
        res.send({ "Success": true, news });


    } catch (err) {
        console.log(err);
    }

});
router.get('/getBetweenDates/:startDate/:endDate', async (req, res) => {
    let { startDate, endDate } = req.params;
    try {

        const news = await query('select * from news WHERE pub_date BETWEEN "' + startDate + ' 00:00:00" AND "' + endDate + ' 00:00:00" And lang="english" ORDER BY id');
        res.send({ "Success": true, news });

        // res.send(rows);
    } catch (err) {
        console.log(err);
    }

});
router.get('/getBetweenDates/:startDate/:endDate/:country', async (req, res) => {
    let { startDate, endDate } = req.params;
    try {

        const news = await query('select * from news WHERE pub_date BETWEEN "' + startDate + ' 00:00:00" AND "' + endDate + ' 00:00:00" And lang="english" And country ="'+req.params.country+'" ORDER BY id');
        res.send({ "Success": true, news });

        // res.send(rows);
    } catch (err) {
        console.log(err);
    }

});

router.get('/getCountriesIrfan', async (req, res) => {
    try {

        const country = await query('select * from country');
        res.send({ "Success": true, country });

        // res.send(rows);
    } catch (err) {
        console.log(err);
    }

}); module.exports = router;