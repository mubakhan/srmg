const express = require('express'),
    router = express.Router();
const { Category } = require('../controllers');

router.post('/fetch', async (req, res) => {
    const sources  = await Category.getCategories(req.body);
    res.json(sources);
});

module.exports = router;
