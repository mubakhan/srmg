const express = require('express'),
    router = express.Router();
const query = require('../factory/query');



router.post('/opinion_hot', async  (req, res) => {
    const { opinionId } = req.body;
    let { ip } = req;
    if (ip.substr(0, 7) == '::ffff:') {
        ip = ip.substr(7);
    }
    try {
        const hotOpinion = await query(`SELECT * FROM hot_opinions WHERE opinion_id=${opinionId} And ip='${ip}'`);
        const coldOpinion = await query(`SELECT * FROM cold_opinions WHERE opinion_id=${opinionId} And ip='${ip}'`);

        if (hotOpinion.length > 0 || coldOpinion.length > 0) {
            res.send({ success:false, message:'you have already added to this opinion' });
        } else {
            const insertColdOpionion = await query(`INSERT INTO hot_opinions ( opinion_id, ip) VALUES (${opinionId},'${ip}')`);
            const opinion = await query(`SELECT * FROM opinions WHERE id = ${opinionId}`);
            const countHot = opinion[0].hot + 1;
            const updateOpinion = await query(`UPDATE opinions SET hot =${countHot} WHERE id =${opinionId}`);
            res.send({ success:true, message:'you have added hot to this opinion' });


        }
    } catch (err) {
        res.send(err);
    }

});


router.post('/opinion_cold', async  (req, res) => {
    const { opinionId } = req.body;
    let { ip } = req;
    if (ip.substr(0, 7) == '::ffff:') {
        ip = ip.substr(7);
    }
    try {
        const hotOpinion = await query(`SELECT * FROM hot_opinions WHERE opinion_id=${opinionId} And ip='${ip}'`);
        const coldOpinion = await query(`SELECT * FROM cold_opinions WHERE opinion_id=${opinionId} And ip='${ip}'`);

        if (hotOpinion.length > 0 || coldOpinion.length > 0) {
            res.send({ success:false, message:'you have already added to this opinion' });
        } else {
            const insertColdOpinion = await query(`INSERT INTO cold_opinions ( opinion_id, ip) VALUES (${opinionId},'${ip}')`);
            const news = await query(`SELECT * FROM opinions WHERE id = ${opinionId}`);
            const countHot = news[0].cold + 1;
            const updateNews = await query(`UPDATE opinions SET cold =${countHot} WHERE id =${opinionId}`);
            res.send({ success:true, message:'you have added cold to this opinion' });


        }
    } catch (err) {
        res.send(err);
    }

});


module.exports = router;
