const express = require('express'),
    router = express.Router();
const query = require('../factory/query');
// const News = require('../models/news');
const { News } = require('../controllers');


router.post('/fetch', async (req, res) => {
    return News.getNews(req, res);
});

// post route for hot and cold news
router.post('/addHot', async  (req, res) => {
    return News.addHot(req, res);
});

// post route for cold news
router.post('/addCold', async  (req, res) => {
    return News.addCold(req, res);
});

router.get('/get_newDetail/:id', async (req, res) => {
    try {
        const news = await query(
            `SELECT n.\`id\`,n.\`title\`,n.\`slug\`,n.seen,n.\`body\`,n.\`media\`,n.\`url\`,n.category_id,n.\`pub_date\`,n.\`hot\`,n.\`cold\`,s.\`source\`,s.\`lang\`,s.\`slug\` as sources_slug FROM \`news\` as n INNER JOIN \`source\` as s ON s.id = n.source_id AND s.lang="english" WHERE n.id = ${
                req.params.id}`
        );
        // const updateNews = await query(
        //     `UPDATE news SET seen=${news[0].seen + 1} WHERE id = ${news[0].id}`
        // );
        const realTimeNews = await query(
            `SELECT n.\`id\`,n.\`title\`,n.\`media\`,n.\`url\`,n.\`pub_date\`,s.\`source\`,s.\`slug\` as sources_slug FROM \`news\` as n INNER JOIN \`source\` as s ON s.id = n.source_id AND s.lang="english" And n.category_id =${
                news[0].category_id
            } ORDER BY n.id DESC LIMIT 10`
        );
        const keywords = await query(
            `SELECT keywords.word FROM keyword_news inner JOIN keywords on keywords.id=keyword_news.keyword_id  WHERE news_id = ${req.params.id}`
        );
        res.send({ news, realTimeNews, keywords });
    } catch (err) {
        res.send(err);
    }
});

router.get('/news-detail/:id', async (req, res) => {
    try {
        const news = await query(
            `SELECT n.\`id\`,n.\`title\`,n.\`slug\`,n.seen,n.\`body\`,n.\`media\`,n.\`url\`,n.\`pub_date\`,n.category_id,n.\`hot\`,n.\`cold\`,s.\`source\`,s.\`lang\`,s.\`slug\` as sources_slug FROM \`news\` as n INNER JOIN \`source\` as s ON s.id = n.source_id  WHERE n.id = ${
                req.params.id}`
        );
        // const updateNews = await query(
        //     `UPDATE news SET seen=${news[0].seen + 1} WHERE id = ${news[0].id}`
        // );

        const realTimeNews = await query(
            `SELECT n.\`id\`,n.\`title\`,n.\`media\`,n.\`url\`,n.\`pub_date\`,s.\`source\`,s.\`slug\` as sources_slug FROM \`news\` as n INNER JOIN \`source\` as s ON s.id = n.source_id where n.category_id =${
                news[0].category_id
            }  ORDER BY n.id DESC LIMIT 10`
        );
        const keywords = await query(
            `SELECT keywords.word FROM keyword_news inner JOIN keywords on keywords.id=keyword_news.keyword_id  WHERE news_id = ${req.params.id}`
        );
        res.render('detail', { news, realTimeNews, keywords });
    } catch (err) {
        res.send(err);
    }
});
router.get('/get_newDetail/urdu/:id', async (req, res) => {
    try {
        const news = await query(
            `select n.\`id\`,n.\`title\`,n.\`slug\`,n.seen,n.\`hot\`,n.\`cold\`,n.\`description\`,n.\`body\`,n.\`summary\`,n.\`media\`,n.\`url\`,n.hot,n.cold,n.\`pub_date\`,n.\`tags\`,n.\`tags\`,n.\`category_id\`,n.\`sub_category\`,n.\`source\`,n.\`lang\`,n.\`source_slug\` as sources_slug,n.country as country  FROM \`news\` as n  WHERE n.id = ${
                req.params.id}`
        );
        // const updateNews = await query(
        //     `UPDATE news SET seen=${news[0].seen + 1} WHERE id = ${news[0].id}`
        // );
        const realTimeNews = await query(
            `select n.\`id\`,n.\`title\`,n.\`slug\`,n.\`hot\`,n.\`cold\`,n.\`description\`,n.\`body\`,n.\`summary\`,n.\`media\`,n.\`url\`,n.hot,n.cold,n.\`pub_date\`,n.\`tags\`,n.\`tags\`,n.\`category_id\`,n.\`sub_category\`,n.\`source\`,n.\`lang\`,n.\`source_slug\` as sources_slug,n.country as country  FROM \`news\` as n  where n.category_id =${
                news[0].category_id
            } AND n.lang ="${
                news[0].lang
            }"  ORDER BY n.id DESC LIMIT 10`
        );
        res.send({ news, realTimeNews });
    } catch (err) {
        res.send(err);
    }
});

module.exports = router;