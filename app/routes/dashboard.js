const express = require('express'),
    cheerio = require('cheerio'),
    router = express.Router();
const query = require('../factory/query');
const { concat } = require('lodash');

const sum = require('sum');
const { TextAnalyticsClient, AzureKeyCredential } = require('@azure/ai-text-analytics');
const key = '759b4f4c9d9645c6acb8369227e6ced9';
const endpoint = 'https://mubashir-text.cognitiveservices.azure.com/';
const textAnalyticsClient = new TextAnalyticsClient(endpoint, new AzureKeyCredential(key));

router.get('/', isLoggedIn, (req, res) => {
    res.render('./dashboard/index.html');
});
router.get('/login', (req, res) => {
    res.render('./dashboard/login');
});
router.get('/getCount/:start/News/:end', async(req, res) => {
    const { start, end } = req.params;
    try {

        const enUrNewsCount = await query(`SELECT SUM(CASE WHEN source.lang='english' And source.id=news.source_id THEN 1 else 0 END) as eng_news,
                                        SUM(CASE WHEN source.lang='urdu' And source.id=news.source_id THEN 1 else 0 END) as urdu_news
                                        FROM news,source WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        const enUrSourcesCount = await query('SELECT SUM(CASE WHEN source.lang=\'english\'THEN 1 else 0 END) as eng_source, SUM(CASE WHEN source.lang=\'urdu\' THEN 1 else 0 END) as urdu_source FROM source');

        const cateCount = await query(`SELECT SUM(CASE WHEN news.category_id = 1 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_national,SUM(CASE WHEN news.category_id = 2 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_world,SUM(CASE WHEN news.category_id = 3 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_sports,SUM(CASE WHEN news.category_id = 4 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_business,SUM(CASE WHEN news.category_id = 5 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_lifeandstyle,SUM(CASE WHEN news.category_id = 6 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_health,SUM(CASE WHEN news.category_id = 7 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_technology,SUM(CASE WHEN news.category_id = 8 AND news.source_id=source.id AND source.lang = 'english' THEN 1 ELSE 0 END) AS eng_other,SUM(CASE WHEN news.category_id = 1 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_national,SUM(CASE WHEN news.category_id = 2 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_world,SUM(CASE WHEN news.category_id = 3 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_sports,SUM(CASE WHEN news.category_id = 4 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_business,SUM(CASE WHEN news.category_id = 5 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_lifeandstyle,SUM(CASE WHEN news.category_id = 6 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_health, SUM(CASE WHEN news.category_id = 7 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_technology,SUM(CASE WHEN news.category_id = 8 AND news.source_id=source.id AND source.lang = 'urdu' THEN 1 ELSE 0 END) AS urd_other FROM  news JOIN source WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        const sentiCount = await query(`SELECT SUM(CASE WHEN news.sentiment_state='positive'THEN 1 else 0 END) as sent_pos,
                                    SUM(CASE WHEN news.sentiment_state='neutral'THEN 1 else 0 END) as sent_neu,
                                    SUM(CASE WHEN news.sentiment_state='negative'THEN 1 else 0 END) as sent_neg
                                    FROM news WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        const deepCount = await query(`SELECT SUM(CASE WHEN news.deep_analyze_state='toxicity'THEN 1 else 0 END) as toxicity, 
                                        SUM(CASE WHEN news.deep_analyze_state='threat'THEN 1 else 0 END) as threat, 
                                        SUM(CASE WHEN news.deep_analyze_state='severe toxicity'THEN 1 else 0 END) as severe_toxicity, 
                                        SUM(CASE WHEN news.deep_analyze_state='insult'THEN 1 else 0 END) as insult, 
                                        SUM(CASE WHEN news.deep_analyze_state='sexual'THEN 1 else 0 END) as sexual, 
                                        SUM(CASE WHEN news.deep_analyze_state='flirt'THEN 1 else 0 END) as flirt, 
                                        SUM(CASE WHEN news.deep_analyze_state='insult'THEN 1 else 0 END) as profanity, 
                                        SUM(CASE WHEN news.deep_analyze_state='identity'THEN 1 else 0 END) as identity 
                                        FROM news WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

        res.send({ enUrNewsCount, enUrSourcesCount, cateCount, sentiCount, deepCount });
    } catch (error) {
        res.send({ error });
    }

});
router.get('/getKeywordCat', async(req, res) => {
    try {

        const categories = await query('SELECT * FROM keyword_category WHERE name NOT LIKE \'middle east\'');
        res.send({ categories });
    } catch (error) {
        res.send({ error });

    }

});
router.get('/getKeyword/all', async(req, res) => {
    try {
        const keywords = await query('SELECT * FROM keywords');
        res.send({ keywords });
    } catch (error) {
        res.send({ error });

    }

});
router.post('/getKeywordByCat', async(req, res) => {
    const categories = req.body.catIds;
    const temp = [];
    for (const element of categories) {
        temp.push(Number(element));
    }
    try {

        const keywords = await query(`SELECT keywords.id AS keyID, keywords.word AS keyWord FROM keywords INNER JOIN keyword_news ON keyword_news.keyword_id = keywords.id WHERE keyword_catId IN (${temp}) GROUP BY keyID`);
        res.send({ keywords });
    } catch (error) {
        res.send({ error });

    }

});
router.get('/getNew/:start/:end/byLang/:lang', async(req, res) => {
    const { start, end, lang } = req.params;
    try {

        const news = await query(`SELECT news.id, news.slug ,news.title, news.description, news.media, news.pub_date,source.source FROM news,source WHERE source.lang='${lang}' And source.id=news.source_id AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});

router.get('/getSourcesByLang/:lang', async(req, res) => {
    const { lang } = req.params;
    try {

        const sources = await query(`SELECT * FROM source where lang = '${lang}'`);
        res.send({ sources });
    } catch (error) {
        res.send({ error });

    }

});

router.get('/getSourcesByLang/:lang', async(req, res) => {
    const { lang } = req.params;
    try {

        const sources = await query(`SELECT * FROM source where lang = '${lang}'`);
        res.send({ sources });
    } catch (error) {
        res.send({ error });

    }

});

router.get('/getcatNews/:start/:end/byLang/:lang/:cat', async(req, res) => {
    const { start, end, lang, cat } = req.params;
    try {

        const news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,
    news.pub_date, news.tags, s.source AS source, c.category
    FROM news
    INNER JOIN source s ON news.source_id = s.id
    INNER JOIN category c ON news.category_id = c.id
    WHERE (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')
    AND c.slug = '${cat}' AND s.lang='${lang}'`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});

router.get('/getSentiNews/:start/:end/byState/:state', async(req, res) => {
    const { start, end, state } = req.params;
    try {

        const news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,news.pub_date ,news.sentiment_state
    FROM news WHERE news.sentiment_state='${state}' AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});
router.get('/getDeepNews/:start/:end/byState/:state', async(req, res) => {
    const { start, end, state } = req.params;
    try {

        const news = await query(`SELECT news.id, news.title, news.slug AS news_slug, news.description, news.media, news.url,news.pub_date ,news.deep_analyze_state FROM news WHERE news.deep_analyze_state='${state}' AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
        res.send({ news });
    } catch (error) {
        res.send({ error });

    }
});
router.post('/get_regions', async(req, res) => {


    const { keywords } = req.body;
    if (keywords.length > 0) {
        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }
        try {
            const regions = await query(`SELECT regions.id AS regId, regions.name AS regName FROM regions INNER JOIN country ON country.region_id = regions.id INNER JOIN source ON source.country_id = country.id INNER JOIN news ON news.source_id = source.id INNER JOIN keyword_news ON keyword_news.news_id = news.id WHERE keyword_news.keyword_id IN (${temp}) GROUP BY regId`);
            // let regionKeywordCount = [];
            // for (const region of regions) {
            //     let obj = {};
            //     const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.region_id =${region.id} and kn.keyword_id in (${temp})`);
            //     obj.id = region.id;
            //     obj.name = region.name;
            //     obj.description = region.description;
            //     obj.count = Count[0].cunt;
            //     regionKeywordCount.push(obj);
            // }
            // const asiaCount = await query(`SELECT COUNT(*) as asiaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.region_id =1 and kn.keyword_id in (${temp}))`);
            res.send(regions);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query('SELECT * FROM `regions`');
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }


});
router.post('/get_countries', async(req, res) => {


    const { keywords } = req.body;
    if (keywords.length > 0) {
        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }
        try {
            const countries = await query(`SELECT * FROM \`country\` WHERE \`region_id\` = ${req.body.regionId}`);

            const countryKeywordCount = [];
            for (const country of countries) {
                const obj = {};
                const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE country.id =${country.id} and kn.keyword_id in (${temp})`);
                obj.id = country.id;
                obj.name = country.name;
                obj.count = Count[0].cunt;
                countryKeywordCount.push(obj);
            }
            res.send(countryKeywordCount);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query(`SELECT * FROM \`country\` WHERE \`region_id\` = ${req.body.regionId}`);
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }


});
router.post('/get_multiCountries', async(req, res) => {
    const {  regions, keyword } = req.body;

    try {
        const temp = [];
        for (const element of regions) {
            temp.push(Number(element));
        }
        const key = [];
        for (const element of keyword) {
            key.push(Number(element));
        }

        const rows = await query(`SELECT country.id AS countryId, country.name AS contryName FROM country INNER JOIN source ON source.country_id = country.id INNER JOIN news ON news.source_id = source.id INNER JOIN keyword_news ON keyword_news.news_id = news.id WHERE keyword_news.keyword_id IN (${key}) AND country.region_id IN (${temp}) GROUP BY countryId`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }


});
router.post('/get_multisources', async(req, res) => {
    const {  countries, keywords } = req.body;

    const temp = [];
    for (const element of countries) {
        temp.push(Number(element));
    }
    const key = [];
    for (const element of keywords) {
        key.push(Number(element));
    }
    try {
        const rows = await query(`SELECT source.id AS sourceId, source.source AS sourcename FROM source INNER JOIN news ON news.source_id = source.id INNER JOIN keyword_news ON keyword_news.news_id = news.id WHERE keyword_news.keyword_id IN (${key}) AND source.country_id IN (${temp}) GROUP BY sourceId`);
        res.send(rows);
    } catch (err) {
        console.log(err);
    }


});
router.post('/get_sources', async(req, res) => {
    const { keywords } = req.body;
    if (keywords.length > 0) {
        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }
        try {
            const sources = await query(`SELECT * FROM \`source\` WHERE \`country_id\` = ${req.body.countryId}`);

            const sourceKeywordCount = [];
            for (const source of sources) {
                const obj = {};
                const Count = await query(`SELECT Count(DISTINCT(news.id)) as cunt FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE  source.id =${source.id} and kn.keyword_id in (${temp})`);
                obj.id = source.id;
                obj.source = source.source;
                obj.slug = source.slug;
                obj.website = source.website;
                obj.lang = source.lang;
                obj.count = Count[0].cunt;
                sourceKeywordCount.push(obj);
            }
            res.send(sourceKeywordCount);
        } catch (err) {
            console.log(err);
        }
    } else {
        try {
            const rows = await query(`SELECT * FROM \`source\` WHERE \`country_id\` = ${req.body.countryId}`);
            res.send(rows);
        } catch (err) {
            console.log(err);
        }
    }


});

// count all news
router.get('/getCount', async(req, res) => {
    try {
        //
        const enUrNewsCount = await query('SELECT SUM(CASE WHEN news.lang=\'english\' THEN 1 else 0 END) as engCount, SUM(CASE WHEN news.lang=\'urdu\' THEN 1 else 0 END) as urduCount, SUM(CASE WHEN news.lang=\'pushto\' THEN 1 else 0 END) as pushtoCount, SUM(CASE WHEN news.lang=\'arabic\' THEN 1 else 0 END) as arabicCount  FROM news');

        const asiaNewsCount = await query('SELECT COUNT(*) as asiaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =1');
        const europeNewsCount = await query('SELECT COUNT(*) as europeCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =2');
        const africaNewsCount = await query('SELECT COUNT(*) as africaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =3');
        const americaNewsCount = await query('SELECT COUNT(*) as americaCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.region_id =4');

        const totalNewsCount = await query('SELECT COUNT(*) as totalCount FROM news');
        const sourcesEngCount = await query('SELECT count(*) as engSourceCount FROM source where lang=\'english\'');
        const sourcesUrduCount = await query('SELECT count(*) as urSourceCount FROM source where lang=\'urdu\'');


        res.send({ enUrNewsCount, asiaNewsCount, europeNewsCount, africaNewsCount, americaNewsCount, totalNewsCount, sourcesEngCount, sourcesUrduCount });
    } catch (error) {
        res.send({ error });
    }

});
router.post('/getNewsBySource', async(req, res) => {


    const { start, end, sourceId, keywords } = req.body;


    if (keywords.length > 0) {

        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }
        try {
            const news = await query(`SELECT Count(DISTINCT(news.id)) as newsCount FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE  source.id =${sourceId} and kn.keyword_id in (${temp}) AND  (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
            const categories = await query('SELECT * FROM keyword_category');
            const sourcesCateNewsCount = [];

            for (const category of categories) {
                const newobj = {};

                const categoryCount = await query(`SELECT Count(DISTINCT(news.id)) as Count FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id = kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id =${sourceId} and kn.keyword_id in (${temp}) and kc.id=${category.id} AND  (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
                newobj.id = category.id;
                newobj.category = category.name;
                newobj.cateNewsCount = categoryCount[0].Count;
                sourcesCateNewsCount.push(newobj);
            }

            res.send({ news, sourcesCateNewsCount });
        } catch (error) {

        }
    } else {
        try {

            const news = await query(`SELECT COUNT(*) as newsCount FROM news WHERE source_id=${sourceId} AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
            const category = await query(`SELECT category.id,category.slug FROM category INNER JOIN source_url ON category.id = source_url.category_id WHERE source_url.source_id =${sourceId}`);
            const sourcesCateNewsCount = [];

            for (const iterator of category) {
                const newobj = {};
                const categoryCount = await query(`SELECT count(*) as Count FROM news WHERE category_id=${iterator.id} AND source_id=${sourceId} AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
                newobj.id = iterator.id;
                newobj.category = iterator.slug;
                newobj.cateNewsCount = categoryCount[0].Count;
                sourcesCateNewsCount.push(newobj);
            }


            res.send({ news, sourcesCateNewsCount });
        } catch (error) {
            res.send({ error });

        }
    }


});
router.post('/getNewsBySourcekey', async(req, res) => {

    const { start, end, sourceId, keyword } = req.body;
    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    const sources = [];
    let temp = [];
    for (const element of keyword) {
        if (element == '0') {
            temp = [];
        } else {
            temp.push(Number(element));
        }
    }
    for (const element of sourceId) {
        sources.push(Number(element));
    }
    console.log(sources);
    try {
        const rows = [];
        // let news = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id desc LIMIT 100 OFFSET 0`);

        let count;
        //  = await query(`SELECT DISTINCT(news.id) FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE source.id in (${sources}) order by id desc`);
        if (temp.length > 0) {
            console.log('news');
            let dayIndex = 0;

            console.log('with keyword');

            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id asc `);
            count = await query(`SELECT count(news.id) as countNews, k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);


            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source,
                        keyword: [val.keyword]
                    });
                }
            });
        } else {
            console.log('news n ');
            const newsn = await query(`SELECT news.* FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id   WHERE source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00') order by id asc`);
            count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id WHERE country.sub_reg = 'm_east' AND source.id in (${sources}) AND (pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);
            if (newsn.length > 0) {
                newsn.forEach(val => {
                    if (rows.some(person => person.id === val.id)) {
                        dayIndex = rows.map(e => e.id).indexOf(val.id);
                        rows[dayIndex].keyword.push(val.keyword);
                    } else {
                        rows.push({
                            id: val.id,
                            title: val.title,
                            media: val.media,
                            link: val.url,
                            body: val.body,
                            description: val.description,
                            pub_date: val.pub_date,
                            source: val.source,
                            keyword: [val.keyword]
                        });
                    }
                });
            }
        }
        const Elements = rows;

        const lastPages = Elements.slice(0, pageNumber);

        const newElem = lastPages;

        const lastRecords = newElem.slice(-10);
        let numberOfNews;
        // if(count.length > 0){
        numberOfNews = count;
        // }


        res.send({ lastRecords, numberOfNews });

        // res.send({ news });
    } catch (error) {
        res.send({ error });

    }

});

router.post('/getDashNews/bySource/', async(req, res) => {
    const { start, end, sourceId, keywords } = req.body;
    if (keywords.length > 0) {
        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }
        try {
            const rows = [];
            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id= ${sourceId} and kn.keyword_id IN (${temp}) AND  (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

            const elemntCateogery = [];
            let dayIndex = 0;
            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        keyword: [val.keyword]
                    });
                }
            });
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }

    } else {
        try {
            const rows = await query(`select n.id, n.title, n.slug, n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n  WHERE n.pub_date BETWEEN "${start} 00:00:00" AND "${end} 00:00:00" AND n.source_id =${sourceId} ORDER BY n.id DESC`);
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }
    }


});
router.post('/getDashNewsMulti/bySource/', async(req, res) => {
    const { start, end, sourceIds, keywords, lastId } = req.body;
    console.log(lastId);

    const temp = [];
    for (const element of keywords) {
        temp.push(Number(element));
    }
    const sources = [];
    for (const element of sourceIds) {
        sources.push(Number(element));
    }

    let pageNumber = Number(req.body.pageId);
    pageNumber = pageNumber * 10;
    if (lastId != null) {
        console.log(lastId);
        try {
            const rows = [];
            const pageID = Number(req.body.pageId);

            console.log('Inside last page id');
            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) AND news.id > ${lastId} order by id asc LIMIT 10`);
            const count = await query(`SELECT count(news.id) as countNews FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN keyword_news as kn on kn.news_id=news.id WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) `);

            const elemntCateogery = [];
            let dayIndex = 0;
            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source,
                        keyword: [val.keyword]
                    });
                }
            });

            var Elements = rows;

            var lastPages = await  Elements.slice(0, pageNumber);

            var newElem = lastPages;

            var lastRecords = await newElem.slice(-10);
            console.log(count.length);
            const numberOfNews = count;
            res.send({ lastRecords, numberOfNews });

        } catch (error) {
            res.send({ error });

        }
    } else {
        try {
            const rows = [];
            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) order by id asc LIMIT 10 OFFSET 0`);
            const count = await query(`SELECT count(news.id) as countNews  FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id in (${sources}) and kn.keyword_id IN (${temp}) order by news.id asc`);


            const elemntCateogery = [];
            let dayIndex = 0;
            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        source: val.source,
                        keyword: [val.keyword]
                    });
                }
            });

            var Elements = rows;

            var lastPages = await  Elements.slice(0, pageNumber);

            var newElem = lastPages;

            var lastRecords = await newElem.slice(-10);
            console.log(count.length);
            const numberOfNews = count;
            res.send({ lastRecords, numberOfNews });

        } catch (error) {
            res.send({ error });

        }
    }


});

router.post('/getDashNews/byKeywordCate', async(req, res) => {
    const { start, end, sourceId, catId, keywords } = req.body;
    if (keywords.length > 0) {
        console.log(`${sourceId} hi ${keywords} sdsf ${catId}`);
        const temp = [];
        for (const element of keywords) {
            temp.push(Number(element));
        }

        try {
            const rows = [];
            const news = await query(`SELECT news.*,k.word as keyword FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE source.id =${sourceId} and kn.keyword_id in (${temp}) and kc.id=${catId} AND (news.pub_date BETWEEN '${start} 00:00:00' AND '${end} 00:00:00')`);

            const elemntCateogery = [];
            let dayIndex = 0;
            news.forEach(val => {
                if (rows.some(person => person.id === val.id)) {
                    dayIndex = rows.map(e => e.id).indexOf(val.id);
                    rows[dayIndex].keyword.push(val.keyword);
                } else {
                    rows.push({
                        id: val.id,
                        title: val.title,
                        media: val.media,
                        link: val.url,
                        body: val.body,
                        description: val.description,
                        pub_date: val.pub_date,
                        keyword: [val.keyword]
                    });
                }
            });
            console.log(rows);
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }

    } else {
        try {
            const rows = await query(`select n.id, n.title, n.slug, n.hot,n.cold,n.description,n.body,n.summary,n.media,n.url,n.hot,n.cold,n.pub_date,n.tags,n.tags,n.category_id,n.sub_category,n.source,n.lang,n.source_slug as sources_slug,n.country as country from news n WHERE n.pub_date BETWEEN "${start} 00:00:00" AND "${end} 00:00:00" AND n.source_id =${sourceId} AND n.category_id =${catId} ORDER BY n.id DESC`);
            res.send(rows);

        } catch (error) {
            res.send({ error });

        }
    }


});
router.get('/test', async(req, res) => {
    const newArr = [];
    const news = await query('SELECT news.*,kc.name as keywordCat FROM news INNER JOIN source ON news.source_id=source.id INNER JOIN country on source.country_id= country.id INNER JOIN keyword_news as kn on kn.news_id=news.id INNER JOIN keywords as k on k.id=kn.keyword_id INNER JOIN keyword_category as kc on kc.id=k.keyword_catId WHERE kn.keyword_id in (1,2)');
    const elemntCateogery = [];
    let dayIndex = 0;
    news.forEach(val => {
        if (newArr.some(person => person.id === val.id)) {
            dayIndex = newArr.map(e => e.id).indexOf(val.id);
            newArr[dayIndex].cate.push(val.keywordCat);
        } else {
            newArr.push({
                id: val.id,
                title: val.title,
                media: val.media,
                link: val.link,
                body: val.body,
                cate: [val.keywordCat]
            });
        }
    });


    res.send(newArr);
});


router.get('/pagenew/:pageId', async(req, res) => {
    let pageNumber = Number(req.params.pageId);
    pageNumber = pageNumber * 10;
    const getNews = await query(`select * from news order by id desc limit ${pageNumber}`);
    const Elements = getNews;

    const last4 = Elements.slice(-10);

    res.send(last4);

});


// news summary
router.get('/textSummary/:id', async (req, res) => {
    const news = await query(`select * from news where id = ${req.params.id}`);
    const { body } = news[0];
    const bodyHtml = cheerio.load(body);
    bodyHtml('img').remove();
    let desc = bodyHtml.text();
    desc = desc.replace(/<[^>]*>?/gm, '');

    try {
        const abstract = sum({ corpus: desc });
        const { summary } = abstract;
        res.send({ summary });
    } catch (error) {
        console.log(error);
    }
});

// Sentiment analysis
router.get('/textAna/:id', async (req, res) => {

    try {

        const news = await query(`select * from news where id = ${req.params.id}`);
        const { body } = news[0];
        const bodyHtml = cheerio.load(body);
        bodyHtml('img').remove();
        let desc = bodyHtml.text();
        desc = desc.replace(/<[^>]*>?/gm, '');
        descTemp = desc.substr(0, 5100);
        const temp = [];
        temp.push(descTemp);
        const sentimentInput = temp;
        const sentimentResult = await textAnalyticsClient.analyzeSentiment(sentimentInput);

        res.send({ sentimentResult });

    } catch (error) {
        console.log(error);
    }

});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/dashboard/login');
}
