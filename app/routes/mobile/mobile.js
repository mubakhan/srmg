let express = require("express"),
  router = express.Router();
const query = require("../../factory/query");
router.get("/get_regions", async (req, res) => {
  try {
    const countries = await query("SELECT * FROM country");
    const regions = await query("SELECT * FROM regions");

    res.send({ countries, regions });
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_catNewsMore", async (req, res) => {
  try {
    let { cateId, sources, lastId } = req.body;
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    if (cateId == "0") {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang` ,n.`source_slug` as sources_image FROM `news` as n where  n.`source_id` in (" +
          temp +
          ") AND n.id < " +
          lastId +
          "  ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    } else {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang` ,n.`source_slug` as sources_image FROM `news` as n  where n.`category_id`=" +
          cateId +
          " AND n.`source_id` in (" +
          temp +
          ") AND n.id < " +
          lastId +
          "  ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    }
  } catch (err) {
    console.log(err);
  }
});
router.get("/getNews", async (req, res) => {
  try {
    const rows = await query(
      'select n.`id`,n.`title`,n.`slug`,n.`hot`,n.`cold`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.hot,n.cold,n.`pub_date`,n.`tags`,n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang`,n.`source_slug` as sources_slug,n.country as country from news n where n.lang="english" ORDER BY n.id DESC LIMIT 10'
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.get("/getOpinions", async (req, res) => {
  try {
    const rows = await query(
      "select * from opinions ORDER BY id DESC LIMIT 10"
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});

router.get("/getSources/:lang", async (req, res) => {
  let lang = req.params.lang;
  try {
    const rows = await query(
      `SELECT source.*,country.name as countryName,regions.name as regionName FROM source INNER JOIN country ON source.country_id = country.id INNER JOIN regions ON country.region_id= regions.id where lang = "${lang}"`
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});

router.get("/getOpnionSources/:lang", async (req, res) => {
  let lang = req.params.lang;
  try {
    const rows = await query(
      `SELECT source.*,country.name as countryName,regions.name as regionName FROM opinion_source as source INNER JOIN country ON source.country_id = country.id INNER JOIN regions ON country.region_id= regions.id where lang = "${lang}"`
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_catOpinions", async (req, res) => {
  let { cateId, sources } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    if (cateId == "0") {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`hot`,n.`cold`,n.`link`,n.`pub_date`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `opinions` as n where  n.`source_id` in (" +
          temp +
          ")  ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    } else {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`link`,n.`pub_date`,n.`hot`,n.`cold`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `opinions` as n  where n.`category_id`=" +
          cateId +
          " AND n.`source_id` in (" +
          temp +
          ")  ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    }
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_catOpinionsMore", async (req, res) => {
  let { cateId, sources, lastId } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    if (cateId == "0") {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`link`,n.`pub_date`,n.`hot`,n.`cold`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `opinions` as n where  n.`source_id` in (" +
          temp +
          ") AND n.id < " +
          lastId +
          " ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    } else {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`link`,n.`pub_date`,n.`hot`,n.`cold`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `opinions` as n  where n.`category_id`=" +
          cateId +
          " AND n.`source_id` in (" +
          temp +
          ")  AND n.id < " +
          lastId +
          " ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    }
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_catNews", async (req, res) => {
  let { cateId, sources } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    if (cateId == "0") {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `news` as n where  n.`source_id` in (" +
          temp +
          ")  ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    } else {
      const rows = await query(
        "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `news` as n  where n.`category_id`=" +
          cateId +
          " AND n.`source_id` in (" +
          temp +
          ") ORDER BY n.id DESC LIMIT 10"
      );
      res.send(rows);
    }
  } catch (err) {
    console.log(err);
  }
});

router.get("/opnionSources", async (req, res) => {
  try {
    const sources = await query(
      'SELECT * FROM `opinion_source` where lang="english"'
    );
    res.send(sources);
  } catch (err) {
    console.log(err);
  }
});

router.get("/get_categories/:lang", async (req, res) => {
  let { lang } = req.params;
  let rows;
  try {
    switch (lang) {
      case "english":
        rows = rows = [
          {
            id: 8,
            category: "Other",
          },
          {
            id: 7,
            category: "Technology",
          },
          {
            id: 6,
            category: "Health",
          },
          {
            id: 5,
            category: "Life & Style",
          },
          {
            id: 4,
            category: "Business",
          },
          {
            id: 3,
            category: "Sports",
          },
          {
            id: 2,
            category: "World",
          },
          {
            id: 1,
            category: "National",
          },
          {
            id: 0,
            category: "Latest",
          },
        ];
        break;
      case "urdu":
        rows = [
          {
            id: 8,
            category: "دیگر",
          },
          {
            id: 7,
            category: "ٹیکنالوجی",
          },
          {
            id: 6,
            category: "صحت",
          },
          {
            id: 5,
            category: "زندگی اور انداز",
          },
          {
            id: 4,
            category: "کاروبار",
          },
          {
            id: 3,
            category: "کھیلوں",
          },
          {
            id: 2,
            category: "دنیا",
          },
          {
            id: 1,
            category: "قومی",
          },
          {
            id: 0,
            category: "تازہ ترین",
          },
        ];
        break;
      case "arabic":
        rows = [
          {
            id: 8,
            category: "الآخرين",
          },
          {
            id: 7,
            category: "تقنية",
          },
          {
            id: 6,
            category: "الصحة",
          },
          {
            id: 5,
            category: "أسلوب الحياة",
          },
          {
            id: 4,
            category: "اعمال",
          },
          {
            id: 3,
            category: "رياضات",
          },
          {
            id: 2,
            category: "العالمية",
          },
          {
            id: 1,
            category: "الوطني",
          },
          {
            id: 0,
            category: "جديد",
          },
        ];
        break;
      case "pushto":
        rows = [
          {
            id: 8,
            category: "نور",
          },
          {
            id: 7,
            category: "ټیکنالوژي",
          },
          {
            id: 6,
            category: "روغتیا",
          },
          {
            id: 5,
            category: "د ژوند طرز",
          },
          {
            id: 4,
            category: "سوداګري",
          },
          {
            id: 3,
            category: "لوبې",
          },
          {
            id: 2,
            category: "نړۍ",
          },
          {
            id: 1,
            category: "ملي",
          },
          {
            id: 0,
            category: "تازه",
          },
        ];
        break;
    }
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.get("/get_lang", async (req, res) => {
  try {
    const rows = await query(`SELECT DISTINCT(lang) FROM source`);
    res.send(rows);
  } catch (error) {
    console.log(err);
  }
});

router.post("/get_trendNews", async (req, res) => {
  let { sources } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }

    const rows = await query(
      "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `news` as n where  n.`source_id` in (" +
        temp +
        ') AND n.lang="english"  ORDER BY  n.id DESC, n.hot DESC,n.cold DESC LIMIT 10'
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_trendNewsMore", async (req, res) => {
  let { sources, lastId } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    const rows = await query(
      "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`summary`,n.`media`,n.`url`,n.`pub_date`,n.`hot`,n.`cold`,n.`tags`, n.`tags`,n.`category_id`,n.`sub_category`,n.`source`,n.`lang`,n.`source_slug` as source_image FROM `news` as n where  n.`source_id` in (" +
        temp +
        ') AND n.lang="english"  AND n.id < ' +
        lastId +
        "  ORDER BY  n.id DESC, n.hot DESC,n.cold DESC LIMIT 10"
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_trendOpinions", async (req, res) => {
  console.log(req.body);
  let { sources } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    console.log(temp);
    const rows = await query(
      "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`link`,n.`pub_date`,n.`hot`,n.`cold`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image from `opinions` as n where  n.`source_id` in (" +
        temp +
        ')   ORDER BY n.id DESC, n.hot DESC,n.cold DESC LIMIT 10'
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});
router.post("/get_trendOpinionsMore", async (req, res) => {
  let { sources, lastId } = req.body;
  try {
    let temp = [];
    for (let element of sources) {
      temp.push(Number(element));
    }
    const rows = await query(
      "SELECT n.`id`,n.`title`,n.`slug`,n.`description`,n.`body`,n.`media`,n.`link`,n.`pub_date`,n.`hot`,n.`cold`,n.`category_id`,n.`source`,n.`lang`,n.`source_slug` as source_image from opinions as n where  n.`source_id` in (" +
        temp +
        ')  n.id < ' +
        lastId +
        "  ORDER BY n.id DESC,n.hot DESC,n.cold DESC LIMIT 10"
    );
    res.send(rows);
  } catch (err) {
    console.log(err);
  }
});

module.exports = router;
