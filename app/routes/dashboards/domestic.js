let express = require('express'),
    passport = require('passport'),
    router = express.Router();
const e = require('express');
const query = require('../../factory/query');
const countQuery = 'select COUNT(n.id) as newsCount';
router.get('/dashboard', isLoggedIn, async(req, res) => {
    res.render('./domestic/index.html')
});
let newTemp;




router.post('/get-sources', async(req, res) => {

    const { lang } = req.body;
    try {
        const rows = await query(`SELECT * FROM source WHERE country_id = 1 AND lang = "${lang}"`);
        res.send(rows);
    } catch (err) {
        res.send(err);
    }

});


router.post('/get-news', async(req, res) => {

    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, sort } = req.body;

    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }

    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count;
    if (!searchWord || !searchWords)
        count = await query(`${countQuery} FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' ORDER BY n.id ${sort}`);
    const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan'   ORDER BY n.id ${sort} LIMIT 10`);
    res.send({ rows, count });

});


router.post('/get-news-more', async(req, res) => {

    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, lastId, sort } = req.body;
    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }

    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    if (sort == 'desc') {
        const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan' AND n.id < ${lastId} ORDER BY n.id ${sort}  LIMIT 10`);
        res.send(rows);
    } else {
        const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan' AND n.id > ${lastId} ORDER BY n.id ${sort}  LIMIT 10`);
        res.send(rows);
    }





});

router.post('/get-opinion-sources', async(req, res) => {

    const { lang } = req.body;

    try {
        const rows = await query(`SELECT * FROM opinion_source WHERE country_id = 1 AND lang = "${lang}"`);
        res.send(rows);
    } catch (err) {
        res.send(err);
    }


});

router.post('/get-opinions', async(req, res) => {

    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, sort } = req.body;
    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }

    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count;
    if (!searchWord || !searchWords)
        count = await query(`${countQuery} FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' ORDER BY n.id ${sort}`);
    const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan'  ORDER BY n.id ${sort} LIMIT 10`);
    res.send({ rows, count });





});

router.post('/get-opinions-more', async(req, res) => {

    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, sort, lastId } = req.body;

    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }

    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    if (sort == 'desc') {
        const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan' AND n.id < ${lastId} ORDER BY n.id ${sort}  LIMIT 10`);
        res.send(rows);
    } else {
        const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan' AND n.id > ${lastId} ORDER BY n.id ${sort}  LIMIT 10`);
        res.send(rows);
    }

});

router.post('/get-news-archive', async(req, res) => {


    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, sort } = req.body;
    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }
    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count = [];
    // if (!searchWord || !searchWords)
    //     count = await query(`${countQuery} FROM  news_archives  as n  where  ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' ORDER BY n.id ${sort}`);

    const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body  ,n.media ,n.url ,n.hot,n.cold,n.pub_date,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news_archives_dom as na INNER JOIN news as n on n.id = na.news_id   where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan'  ORDER BY n.id ${sort}`);
    res.send({ rows, count });



});

router.post('/add-news-archive', async(req, res) => {
    const { newsId } = req.body;
    try {
        const archiveNews = await query(`INSERT INTO news_archives_dom ( news_id, user_id) VALUES (${newsId},${req.user[0].id})`);
        res.send(true);
    } catch (e) {
        res.send(e);
    }

});

router.post('/delete-news-archive', async(req, res) => {
    const { newsId } = req.body;
    try {
        const archiveNews = await query(`DELETE FROM news_archives_dom WHERE news_id = ${newsId} AND user_id = ${req.user[0].id}`);
        res.send(true);
    } catch (e) {
        res.send(e);
    }

});

router.post('/add-opinion-archive', async(req, res) => {
    const { opinionId } = req.body;
    try {
        const archiveNews = await query(`INSERT INTO opinion_archives_dom( news_id, user_id) VALUES (${opinionId},${req.user[0].id})`);
        res.send(true);
    } catch (e) {
        res.send(e);
    }

});

router.post('/delete-opinion-archive', async(req, res) => {
    const { opinionId } = req.body;
    try {
        const archiveNews = await query(`DELETE FROM opinion_archives_dom WHERE news_id = ${opinionId} AND user_id = ${req.user[0].id}`);
        res.send(true);
    } catch (e) {
        res.send(e);
    }

});

router.post('/get-opinion-archive', async(req, res) => {

    const { sources, startDate, endDate, lang, searchPhrase, searchKeywords, sort } = req.body;
    let keywordMatch = ''
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`
        }
    }

    let source = sources.length ? `AND n.source_id in (${sources})` : '';
    let date = startDate ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"` : '';
    let searchWord = searchPhrase ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')` : '';
    let searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count = [];
    // if (!searchWord || !searchWords)
    //     count = await query(`${countQuery} FROM  opinions  as n  where  ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' ORDER BY n.id ${sort}`);

    const rows = await query(`select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body  ,n.media ,n.link ,n.hot,n.cold,n.pub_date,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinion_archives_dom as na INNER JOIN opinions as n on n.id = na.news_id   where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords} AND n.country='pakistan'  ORDER BY n.id ${sort}`);
    res.send({ rows, count });
});

router.get('/login', async(req, res) => {
    if (req.user) {
        res.redirect('/domestic/dashboard')
    } else {
        res.render('./domestic/login.html')
    }
});

router.get('/logout', function(req, res) {
    req.logout();
    res.redirect('/domestic/login');
});

router.post('/login', function(req, res, next) {
    passport.authenticate('local-login', function(err, user, info) {
        if (err) {
            console.log('error', "" + err);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.send(false);
        }
        if (!user) {
            console.log(info)
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, loginErr => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.send(true)
                // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});

router.get('/get-news-archive', async(req, res) => {

    try {
        const rows = await query(`SELECT * FROM news_archives_dom where user_id = ${req.user[0].id}`);
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }


});

router.get('/get-opinion-archive', async(req, res) => {

    try {
        const rows = await query(`SELECT * FROM opinion_archives_dom where user_id = ${req.user[0].id}`);
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }

});


module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated())
        return next();
    res.redirect('/domestic/login');
}