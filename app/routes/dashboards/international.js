const express = require('express'),
    passport = require('passport'),
    router = express.Router();
const query = require('../../factory/query');
const fs = require('fs');
const bcrypt = require('bcryptjs');
const e = require('express');
const saltRounds = 10;

const countQuery = 'select COUNT(n.id) as newsCount';
router.get('/dashboard', isLoggedIn, async (req, res) => {
    res.render('./international/index.html');
});
router.get('/statistics', isLoggedIn, async (req, res) => {
    res.render('./international/stats.html');
});

router.post('/getChatData', async (req, res) => {
    const { filterType, lang, sources, startDate, endDate, dateFilter, search } =
    req.body;
    if (filterType == 'news') {
        const date = startDate
            ? `AND pub_date BETWEEN "${startDate}" AND "${endDate}"`
            : '';
        const searchWord = search ? `AND (title LIKE '%${search}%' OR body LIKE '%${search}%')` : '';
        let userSources;
        if (sources.length) {
            userSources = await query(
                `SELECT * FROM  source where lang ='${lang}' and id in (${sources})`
            );
        } else {
            userSources = await query(
                `SELECT source.* FROM user_source INNER join source on source.id = user_source.source_id WHERE source.lang ='${lang}' AND user_source.user_id = ${req.user[0].id}`
            );
        }
        if (!userSources.length) {
            userSources = await query(
                `SELECT * FROM  source where lang ='${lang}'`
            );
        }


        const sourcesCountr = [];
        const userSourcesIds = [];
        let totalNews = 0;
        let totalSources = 0;

        const sourcesCount = userSources.map(async (userSource) => {
            const userSources = await query(
                `SELECT COUNT(*) as sourceCount FROM news WHERE lang ='${lang}' AND source_id=${userSource.id} ${date} ${searchWord}`
            );
            userSourcesIds.push(userSource.id);
            // console.log(userSources[0].sourceCount);
            const obj = {};
            obj.name = userSource.source;
            obj.value = userSources[0].sourceCount;
            sourcesCountr.push(obj);
            totalNews = totalNews + userSources[0].sourceCount;
            totalSources = totalSources + 1;


        });
        await Promise.all(sourcesCount);

        let totalCountries;
        totalCountries = await query(
            `select count(*) as count from user_country where user_id = ${req.user[0].id}`
        );
        if (!totalCountries.length) {
            totalCountries = await query('select count(*) as count from country');
        }

        let yearCount;
        if (dateFilter == '1y') {
            yearCount = await query(
                `SELECT extract(month from pub_date) as month, count(id) as count FROM news where news.source_id in(${userSourcesIds}) ${date}  ${searchWord} GROUP BY month ORDER BY pub_date ASC`
            );
        } else if (dateFilter == '1m') {
            yearCount = await query(
                `SELECT extract(week from pub_date) as week, count(id) as count FROM news where news.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY week ORDER BY pub_date ASC`
            );
        } else if (dateFilter == '7d') {
            console.log(
                `SELECT extract(day from pub_date) as day, count(id) as count FROM news where news.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY day ORDER BY pub_date ASC`
            );
            yearCount = await query(
                `SELECT extract(day from pub_date) as day, count(id) as count FROM news where news.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY day ORDER BY pub_date ASC`
            );
        } else {
            yearCount = await query(
                `SELECT extract(month from pub_date) as month, count(id) as count FROM news where news.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY month ORDER BY pub_date ASC`
            );
        }

        const countryCount = await query(
            `SELECT country,count(id) as count FROM news  where news.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY country`
        );
        res.send({
            pieCount: sourcesCountr,
            yearCount,
            countryCount,
            totalNews,
            totalSources,
            totalCountries
        });
    } else {

        const date = startDate
            ? `AND pub_date BETWEEN "${startDate}" AND "${endDate}"`
            : '';
        const searchWord = search ? `AND (title LIKE '%${search}%' OR body LIKE '%${search}%')` : '';

        let userSources;
        if (sources.length) {
            userSources = await query(
                `SELECT * FROM  opinion_source where lang ='${lang}' and id in (${sources})`
            );
        } else {
            userSources = await query(
                `SELECT opinion_source.* FROM user_opinion INNER join opinion_source on opinion_source.id = user_opinion.source_id WHERE opinion_source.lang ='${lang}' AND user_opinion.user_id = ${req.user[0].id}`
            );
            if (!userSources.length) {
                userSources = await query(
                    `SELECT * FROM  opinion_source where lang ='${lang}'`
                );
            }
        }
        const sourcesCountr = [];
        const userSourcesIds = [];
        let totalNews = 0;
        let totalSources = 0;

        const sourcesCount = userSources.map(async (userSource) => {
            const userSources = await query(
                `SELECT COUNT(*) as sourceCount FROM opinions WHERE source_id=${userSource.id} ${date} ${searchWord}`
            );
            userSourcesIds.push(userSource.id);
            // console.log(userSources[0].sourceCount);
            const obj = {};
            obj.name = userSource.source;
            obj.value = userSources[0].sourceCount;
            sourcesCountr.push(obj);
            totalNews = totalNews + userSources[0].sourceCount;
            totalSources = totalSources + 1;
        });
        await Promise.all(sourcesCount);
        let totalCountries;
        totalCountries = await query(
            `select count(*) as count from user_country where user_id = ${req.user[0].id}`
        );
        if (!totalCountries.length) {
            totalCountries = await query('select count(*) as count from country');
        }

        let yearCount;
        if (dateFilter == '1y') {
            yearCount = await query(
                `SELECT extract(month from pub_date) as month, count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY month ORDER BY pub_date ASC`
            );
        } else if (dateFilter == '1m') {
            yearCount = await query(
                `SELECT extract(week from pub_date) as week, count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY week ORDER BY pub_date ASC`
            );
        } else if (dateFilter == '7d') {
            console.log(
                `SELECT extract(day from pub_date) as day, count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY day ORDER BY pub_date ASC`
            );
            yearCount = await query(
                `SELECT extract(day from pub_date) as day, count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY day ORDER BY pub_date ASC`
            );
        } else {
            yearCount = await query(
                `SELECT extract(month from pub_date) as month, count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY month ORDER BY pub_date ASC`
            );
        }

        const countryCount = await query(
            `SELECT country,count(id) as count FROM opinions where opinions.source_id in(${userSourcesIds}) ${date} ${searchWord} GROUP BY country`
        );

        res.send({
            pieCount: sourcesCountr,
            yearCount,
            countryCount,
            totalNews,
            totalSources,
            totalCountries
        });
    }
});

router.get('/opinions', isLoggedIn, async (req, res) => {
    res.render('./international/opinions.html');
});
router.get('/settings', isLoggedIn, async (req, res) => {
    res.render('./international/settings.html');
});

let newTemp;

router.get('/get-countries/:lang', async (req, res) => {
    try {
    // let rows = await query(`SELECT DISTINCT country.* FROM source RIGHT OUTER JOIN country on country.id = source.country_id WHERE source.lang='${req.params.lang}' and country.reg_int='int'`);
        const rows = await query(
            `SELECT country.* FROM country INNER JOIN user_country on country.id = user_country.country_id where user_country.user_id = ${req.user[0].id}`
        );
        if (!rows.length) {
            const rows = await query('SELECT * from country');
            res.send(rows);
        } else {
            res.send(rows);
        }
    } catch (error) {
        res.send(error);
    }
});
router.get('/get-all/:lang', async (req, res) => {
    try {
        const regions = await query('SELECT * from regions');
        const countries = await query('SELECT * from country');
        const sources = await query(
            `SELECT * from source where lang = '${req.params.lang}'`
        );
        const opinionSources = await query(
            `SELECT * from opinion_source WHERE lang='${req.params.lang}'`
        );

        res.send({ regions, countries, sources, opinionSources });
    } catch (error) {
        res.send(error);
    }
});

router.get('/get-userRegions/:lang', async (req, res) => {
    try {
        const regions = await query(
            `SELECT regions.* FROM regions INNER JOIN user_regions on regions.id = user_regions.region_id where user_regions.user_id = ${req.user[0].id}`
        );
        if (!regions.length) {
            const regions = await query('SELECT * from regions');
            res.send(regions);
        } else {
            res.send(regions);
        }
    } catch (error) {
        console.log(error);
        res.send(error);
    }
});

router.post('/get-sources', async (req, res) => {
    const { countries, lang } = req.body;

    const country = countries.length
        ? `source.country_id IN (${JSON.stringify(countries).replace(
            /\[|\]/g,
            ''
        )}) AND`
        : '';
    try {
        const rows = await query(
            `SELECT source.* FROM source INNER JOIN user_source on source.id = user_source.source_id WHERE ${country} source.lang = "${lang}" AND user_source.user_id = ${req.user[0].id}`
        );

        if (!rows.length) {
            const rows = await query(
                `SELECT source.* FROM source WHERE source.lang = "${lang}"`
            );
            res.send(rows);
        } else {
            res.send(rows);
        }
    } catch (error) {
        console.log(error);
        res.send(error);
    }

    // if (countries.length) {
    //   let temp = [];
    //   for (let element of countries) {
    //     temp.push(Number(element));
    //   }

    //   try {
    //     const rows = await query(
    //       `SELECT * FROM source INNER JOIN user_source on source.id = user_source.source_id WHERE source.country_id in (${temp}) AND source.lang = "${lang}" AND user_source.user_id = ${req.user[0].id}`
    //     );
    //     res.send(rows);
    //   } catch (err) {
    //     res.send(err);
    //   }
    // } else {
    //   const getCountriews = await query(
    //     `SELECT country.* FROM country INNER JOIN user_country on country.id = user_country.country_id where user_country.user_id = ${req.user[0].id}`
    //   );
    //   let temp = [];
    //   for (const country of getCountriews) {
    //     temp.push(country.id);
    //   }
    //   try {
    //     const rows = await query(
    //       `SELECT * FROM source INNER JOIN user_source on source.id = user_source.source_id WHERE source.country_id in (${temp}) AND source.lang = "${lang}" AND user_source.user_id = ${req.user[0].id}`
    //     );
    //     res.send(rows);
    //   } catch (err) {
    //     res.send(err);
    //   }
    // }
});

router.post('/get-news', isLoggedIn, async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        sort,
        countries
    } = req.body;
    let userSources;
    if (sources.length) {
        userSources = await query(
            `SELECT * FROM  source where lang ='${lang}' and id in (${sources})`
        );
    } else {
        userSources = await query(
            `SELECT source.* FROM user_source INNER join source on source.id = user_source.source_id WHERE source.lang ='${lang}' AND user_source.user_id = ${req.user[0].id}`
        );
    }
    if (!userSources.length) {
        userSources = await query(
            `SELECT * FROM  source where lang ='${lang}'`
        );
    }
    const ttemp = [];
    for (const source of userSources) {
        ttemp.push(source.id);
    }
    newTemp = JSON.stringify(ttemp).replace(/\[|\]/g, '');

    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }
    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    let coutry;
    if (sources.length) {
        coutry = '';
    } else {
        coutry = countries.length
            ? `AND country IN (${JSON.stringify(countries).replace(
                /\[|\]/g,
                ''
            )}) AND n.source_id IN (${newTemp})`
            : ` AND n.source_id IN (${newTemp})`;
    }
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count;
    console.log(
        `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} ORDER BY n.id ${sort} LIMIT 10`
    );
    if (!searchWord || !searchWords)
        count = await query(
            `${countQuery} FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${coutry} ${searchWord} ${searchWords} ORDER BY n.id ${sort}`
        );
    const rows = await query(
        `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords}   ORDER BY n.id ${sort} LIMIT 10`
    );
    res.send({ rows, count });
});

router.post('/get-news-more', async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        lastId,
        sort,
        countries
    } = req.body;
    let userSources;
    if (sources.length) {
        userSources = await query(
            `SELECT * FROM  source where lang ='${lang}' and id in (${sources})`
        );
    } else {
        userSources = await query(
            `SELECT source.* FROM user_source INNER join source on source.id = user_source.source_id WHERE source.lang ='${lang}' AND user_source.user_id = ${req.user[0].id}`
        );
    }
    if (!userSources.length) {
        userSources = await query(
            `SELECT * FROM  source where lang ='${lang}'`
        );
    }
    const ttemp = [];
    for (const source of userSources) {
        ttemp.push(source.id);
    }
    newTemp = JSON.stringify(ttemp).replace(/\[|\]/g, '');
    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }

    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    const coutry = countries.length
        ? `AND country IN (${JSON.stringify(countries).replace(
            /\[|\]/g,
            ''
        )}) AND n.source_id IN (${newTemp})`
        : ` AND n.source_id IN (${newTemp})`;
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';

    if (sort == 'desc') {
        const rows = await query(
            `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} AND n.id < ${lastId} ORDER BY n.id ${sort} LIMIT 10`
        );
        res.send(rows);
    } else {
        const rows = await query(
            `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.url ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} AND n.id > ${lastId}   ORDER BY n.id ${sort} LIMIT 10`
        );
        res.send(rows);
    }
});

router.post('/get-opinion-sources', async (req, res) => {
    const { countries, lang } = req.body;

    try {
        const rows = await query(
            `SELECT opinion_source.* FROM opinion_source INNER JOIN user_opinion on opinion_source.id = user_opinion.source_id WHERE opinion_source.lang = "${lang}" AND user_opinion.user_id = ${req.user[0].id}`
        );

        if (!rows.length) {
            const rows = await query(
                `SELECT opinion_source.* FROM opinion_source WHERE opinion_source.lang = "${lang}"`
            );
            res.send(rows);
        } else {
            res.send(rows);
        }
    } catch (err) {
        res.send(err);
    }
});

router.post('/get-opinions', async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        sort,
        countries
    } = req.body;

    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }


    let userSources;

    const ttemp = [];
    try {
        if (sources.length) {
            userSources = await query(
                `SELECT * FROM  opinion_source where lang ='${lang}' and id in (${sources})`
            );
        } else {
            userSources = await query(
                `SELECT opinion_source.* from user_opinion inner join opinion_source on opinion_source.id = user_opinion.source_id where opinion_source.lang ='${lang}' AND user_opinion.user_id = ${req.user[0].id}`
            );
        }
        if (!userSources.length) {
            userSources = await query(
                `SELECT * from opinion_source where lang = '${lang}'`
            );
        }

        for (const source of userSources) {
            ttemp.push(source.id);
        }
    } catch (error) {
        console.log(error);
    }

    newTemp = JSON.stringify(ttemp).replace(/\[|\]/g, '');
    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    const coutry = countries.length
        ? `AND country IN (${JSON.stringify(countries).replace(
            /\[|\]/g,
            ''
        )}) AND n.source_id IN (${newTemp})`
        : ` AND n.source_id IN (${newTemp})`;
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';
    let count;

    if (!searchWord || !searchWords)
        count = await query(
            `${countQuery} FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${coutry} ${searchWord} ${searchWords} ORDER BY n.id ${sort}`
        );

    const rows = await query(
        `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} ORDER BY n.id ${sort} LIMIT 10`
    );
    res.send({ rows, count });
});

router.post('/get-opinions-more', async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        sort,
        lastId,
        countries
    } = req.body;

    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }
    let userSources;
    const ttemp = [];

    try {
        if (sources.length) {
            userSources = await query(
                `SELECT * FROM  opinion_source where lang ='${lang}' and id in (${sources})`
            );
        } else {
            userSources = await query(
                `SELECT opinion_source.* from user_opinion inner join opinion_source on opinion_source.id = user_opinion.source_id where opinion_source.lang ='${lang}' AND user_opinion.user_id = ${req.user[0].id}`
            );
        }
        if (!userSources.length) {
            userSources = await query(
                `SELECT * from opinion_source where lang = '${lang}'`
            );
        }

        for (const source of userSources) {
            ttemp.push(source.id);
        }
    } catch (error) {
        console.log(error);
    }
    newTemp = JSON.stringify(ttemp).replace(/\[|\]/g, '');

    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    const coutry = countries.length
        ? `AND country IN (${JSON.stringify(countries).replace(
            /\[|\]/g,
            ''
        )}) AND n.source_id IN (${newTemp})`
        : ` AND n.source_id IN (${newTemp})`;
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';

    if (sort == 'desc') {
        const rows = await query(
            `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} AND n.id < ${lastId} ORDER BY n.id ${sort} LIMIT 10`
        );
        res.send(rows);
    } else {
        const rows = await query(
            `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body ,n.media ,n.link ,n.hot,n.cold,n.pub_date   ,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinions  as n  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${coutry} ${searchWords} AND n.id > ${lastId} ORDER BY n.id ${sort} LIMIT 10`
        );
        res.send(rows);
    }
});

router.post('/get-news-archive', async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        sort
    } = req.body;

    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }

    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';
    const count = [];
    // if (!searchWord || !searchWords)
    //     count = await query(`${countQuery} FROM  opinions  as n  where  ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' AND n.lang = "${lang}" ORDER BY n.id ${sort}`);
    const rows = await query(
        `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body  ,n.media ,n.url ,n.hot,n.cold,n.pub_date,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  news_archives as na INNER JOIN news as n on n.id = na.news_id  where  n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords}   ORDER BY n.id ${sort}`
    );
    res.send({ rows, count });
});

router.post('/add-news-archive', async (req, res) => {
    const { newsId } = req.body;
    try {
        const archiveNews = await query(
            `INSERT INTO news_archives( news_id, user_id) VALUES (${newsId},${req.user[0].id})`
        );
        res.send(true);
    } catch (e) {
        res.send(e);
    }
});

router.post('/delete-news-archive', async (req, res) => {
    const { newsId } = req.body;
    try {
        const archiveNews = await query(
            `DELETE FROM news_archives WHERE news_id = ${newsId} AND user_id = ${req.user[0].id}`
        );
        res.send(true);
    } catch (e) {
        res.send(e);
    }
});

router.post('/add-opinion-archive', async (req, res) => {
    const { opinionId } = req.body;
    try {
        const archiveNews = await query(
            `INSERT INTO opinion_archives( news_id, user_id) VALUES (${opinionId},${req.user[0].id})`
        );
        res.send(true);
    } catch (e) {
        res.send(e);
    }
});

router.post('/delete-opinion-archive', async (req, res) => {
    const { opinionId } = req.body;
    try {
        const archiveNews = await query(
            `DELETE FROM opinion_archives WHERE news_id = ${opinionId} AND user_id = ${req.user[0].id}`
        );
        res.send(true);
    } catch (e) {
        res.send(e);
    }
});

router.post('/get-opinion-archive', async (req, res) => {
    const {
        sources,
        startDate,
        endDate,
        lang,
        searchPhrase,
        searchKeywords,
        sort
    } = req.body;

    let keywordMatch = '';
    if (searchKeywords) {
        for (const keyword of searchKeywords) {
            keywordMatch = `${keywordMatch} AND (n.title LIKE '% ${keyword} %' OR n.body LIKE '% ${keyword} %')`;
        }
    }

    const source = sources.length ? `AND n.source_id in (${sources})` : '';
    const date = startDate
        ? `AND n.pub_date BETWEEN "${startDate}" AND "${endDate}"`
        : '';
    const searchWord = searchPhrase
        ? `AND (n.title LIKE '%${searchPhrase}%' OR n.body LIKE '%${searchPhrase}%')`
        : '';
    const searchWords = keywordMatch ? `${keywordMatch}` : '';
    const count = [];
    // if (!searchWord || !searchWords)
    //     count = await query(`${countQuery} FROM  opinions  as n  where  ${source} ${date} ${searchWord} ${searchWords} AND country = 'pakistan' AND n.lang = "${lang}" ORDER BY n.id ${sort}`);

    const rows = await query(
        `select n.id ,n.title ,n.slug ,n.hot ,n.cold ,n.description ,n.body  ,n.media ,n.link ,n.hot,n.cold,n.pub_date,n.category_id  ,n.source ,n.lang ,n.source_slug  as sources_slug,n.country as country  FROM  opinion_archives as na INNER JOIN opinions as n on n.id = na.news_id  where n.lang = "${lang}" ${source} ${date} ${searchWord} ${searchWords}  ORDER BY n.id ${sort}`
    );
    res.send({ rows, count });
});

router.get('/login', async (req, res) => {
    if (req.user) {
        res.redirect('/international/dashboard/');
    } else {
        res.render('./international/login.html');
    }
});
router.get('/register', async (req, res) => {
    if (req.user) {
        res.redirect('/international/dashboard');
    } else {
        res.render('./international/register.html');
    }
});
router.post('/register', async (req, res) => {
    try {
        const salt = bcrypt.genSaltSync(saltRounds);
        const hash = bcrypt.hashSync(req.body.password, salt);
        if (req.files && req.files.file) {
            const dir = './public/uploads/';
            if (!fs.existsSync(dir)) {
                fs.mkdirSync(dir);
            }
            const sampleFile = req.files.file;
            let imagename = `${dir + new Date() / 1000}${sampleFile.name}`;
            await sampleFile.mv(imagename, function (err) {
                if (err) return res.status(500).send(err);
            });
            imagename = imagename.substring(9);
            const rows = await query(
                `INSERT INTO \`user\`(name,email,password,picture,oauth_provider) VALUES ('${
                    req.body.name
                }','${
                    req.body.email
                }','${
                    hash
                }','${
                    imagename
                }','newshunt.io')`
            );
            res.send({ sucess: 'true' });
        } else {
            const rows = await query(
                `INSERT INTO user (name,email,phone,password,oauth_provider) VALUES ('${req.body.email}','${req.body.email}',${req.body.phone},'${hash}','newshunt.io')`
            );
            res.send({ sucess: 'true' });
        }
    } catch (err) {
        console.log(err);
        res.send({ sucess: 'false' });
    }
});

router.post('/updateUser', isLoggedIn, async (req, res) => {
    try {
        const { regions, sources, countries, opinionSources } = req.body;
        const userId = req.user[0].id;
        await query(`Delete from user_source Where user_id = ${userId}`);
        await query(`Delete from user_regions Where user_id = ${userId}`);
        await query(`Delete from user_opinion Where user_id = ${userId}`);
        await query(`Delete from user_country Where user_id = ${userId}`);

        for (const iterator of regions) {
            const rows = await query(
                `INSERT INTO user_regions (user_id,region_id) VALUES (${userId},${iterator})`
            );
        }
        for (const iterator of countries) {
            const rows = await query(
                `INSERT INTO user_country (user_id,country_id) VALUES (${userId},${iterator})`
            );
        }
        for (const iterator of sources) {
            const rows = await query(
                `INSERT INTO user_source (user_id,source_id) VALUES (${userId},${iterator})`
            );
        }
        for (const iterator of opinionSources) {
            const rows = await query(
                `INSERT INTO user_opinion (user_id,source_id) VALUES (${userId},${iterator})`
            );
        }
        res.send({ sucess: 'true' });
    } catch (err) {
        console.log(err);
        res.send({ sucess: 'false' });
    }
});

router.post('/login', function (req, res, next) {
    passport.authenticate('local-login', function (err, user, info) {
        if (err) {
            console.log('error', `${err}`);
            //req.flash('error', "" + err); // will generate a 500 error
            return res.send(false);
        }
        if (!user) {
            console.log(info);
            return res.send(info);
            //console.log(info.message);
        }
        req.login(user, (loginErr) => {
            if (loginErr) {
                console.log(loginErr);
            }
            res.send(true);
            // res.send({success : true,user : req.user});
        });
    })(req, res, next);
});

router.get('/get-news-archive', async (req, res) => {
    try {
        const rows = await query(
            `SELECT * FROM news_archives where user_id = ${req.user[0].id}`
        );
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }
});

router.get('/get-opinion-archive', async (req, res) => {
    try {
        const rows = await query(
            `SELECT * FROM opinion_archives where user_id = ${req.user[0].id}`
        );
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }
});
router.get('/getRegions', async (req, res) => {
    try {
        const rows = await query('SELECT * FROM regions');
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }
});
router.post('/getcountries', async (req, res) => {
    try {
        const rows = await query(
            `SELECT * FROM country where region_id in (${req.body.regionId})`
        );
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }
});
router.post('/getSourcesReg', async (req, res) => {
    try {
        const rows = await query(
            `SELECT * FROM source where country_id in(${req.body.countryId}) And lang ="${req.body.lang}"`
        );
        res.send({ rows });
    } catch (err) {
        res.send(err);
    }
});
router.get('/logout', function (req, res) {
    req.logout();
    res.redirect('/international/login');
});

module.exports = router;

function isLoggedIn(req, res, next) {
    if (req.isAuthenticated()) return next();
    res.redirect('/international/login');
}

async function getCountries() {
    try {
        const rows = await query(
            `SELECT country.* FROM country INNER JOIN user_country on country.id = user_country.country_id where user_country.user_id = ${req.user[0].id}`
        );
        return rows;
    } catch (error) {
        return error;
    }
}
