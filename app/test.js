require('dotenv').config();
const SequelizeAuto = require('sequelize-auto')

const auto = new SequelizeAuto(process.env.DB_NAME, process.env.DB_USER, process.env.DB_PASSWORD, {
    host: 'localhost',
    port: process.env.DB_PORT,
    dialect: process.env.DB_DIALECT,
    directory: './models' // where to write files
});
auto.run();