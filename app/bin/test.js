require('dotenv').config({ path: './scraper/.env' });
const client = require('../factory/elasticsearch');

const News  = require('../models/news');
const Source  = require('../models/source');
const SourceUrl  = require('../models/source_url');
const SourceDataPath  = require('../models/source_data_path');
const Country  = require('../models/country');
const Sequelize = require('sequelize');
const { Op } = Sequelize;


const axios = require('axios');
const cheerio = require('cheerio');

const timeout = 5000;

(async () => {

    const x = await Source.update({
        updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
    }, {
        where: {
            id: 5
        }
    });
    console.log(x);

})();