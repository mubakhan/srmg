const app = require('../app');
const bodyParser = require('body-parser'),
    cors = require('cors'),
    passport = require('passport'),
    LocalStrategy = require('passport-local'),
    bcrypt = require('bcryptjs'),
    PORT = process.env.PORT || 3000;

const compression = require('compression');
const fileUpload = require('express-fileupload');
const query = require('../factory/query');

const indexRouter = require('../routes/index');
app.use(cors());
app.use(bodyParser.json());

// for parsing application/xwww-
app.use(bodyParser.urlencoded({ extended: true }));
app.use(compression());
app.use(fileUpload());
app.set('view engine', 'ejs');
app.engine('html', require('ejs').renderFile);
app.use(
    require('express-session')({
        secret: 'funtime',
        resave: true,
        saveUninitialized: false
    })
);
app.use(passport.initialize());
app.use(passport.session());

passport.use(
    'local-login',
    new LocalStrategy(
        {
            // by default, local strategy uses username and password, we will override with email
            usernameField: 'email',
            passwordField: 'password',
            passReqToCallback: true // allows us to pass back the entire request to the callback
        }, async function (req, email, password, done) {
            // callback with email and password from our form
            query(
                `SELECT * FROM \`user\` WHERE \`email\` = '${email}'`,
                async function (err, rows) {
                    if (err) return done(err);
                    if (!rows.length) {
                        return done(null, false, 'no user found');
                    }
                    const match = await bcrypt.compare(password, rows[0].password);
                    if (match) {
                        return done(null, rows[0]);
                    } else {
                        return done(null, false, 'wrong password');
                    }
                }
            );
        }));
passport.serializeUser(function (user, done) {
    done(null, user);
});
passport.deserializeUser(async function (user, done) {
    //console.log(user);
    const rows = await query(
        `SELECT * FROM \`user\` WHERE \`email\` = '${user.email}'`
    );
    //res.send(rows);
    done(null, rows);
});
app.use(function (req, res, next) {
    res.set(
        'Cache-Control',
        'no-cache, private, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0'
    );
    res.locals.currentUser = req.user;
    next();
});
app.use(indexRouter);
require('./scrap');
app.listen(PORT, () => {
    console.log('Server is Listening on port :', PORT);
});
