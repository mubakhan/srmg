require('../app');
const migrations = require('../migrations');

(async()=>{
    for (const migration of migrations) {
        await migration.up();
    }
    process.exit(0);
})();
