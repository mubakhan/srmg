require('dotenv').config({ path: './scraper/.env' });
const client = require('../factory/elasticsearch');
const cron = require('node-cron');

const News  = require('../models/news');
const Source  = require('../models/source');
const SourceUrl  = require('../models/source_url');
const SourceDataPath  = require('../models/source_data_path');
const Country  = require('../models/country');
const Sequelize = require('sequelize');
const { Op } = Sequelize;


const axios = require('axios');
const cheerio = require('cheerio');

const timeout = 5000;

const scrap = async (sources)=>{
    return Promise.all(sources.map(async (source)=>{
        const { id } = source;
        const news = await News.findAll({
            where: {
                source_id: id
            },
            limit:200,
            order: [
                ['id', 'DESC']
            ],
            raw: true
        });
        const recentLinks = news.map(news=> news.url);
        const scrapedNewsWithLinks = (await Promise.all(source.source_urls.map(async sourceUrl=> {
            try {
                const { data:html } = await axios({
                    method: 'get',
                    url: sourceUrl.url,
                    timeout // only wait for 2s
                });

                const $ = cheerio.load(html, { xmlMode: true });
                if (sourceUrl.encoding === 'xml') {
                    const dataPaths = sourceUrl.source_data_paths;
                    const arr = dataPaths.reduce((acc, dataPath)=> {
                        const { data_name, data_path } = dataPath;
                        acc[data_name] = data_path;
                        return acc;
                    }, []);

                    if (dataPaths.length && dataPaths[0].data_param === 'html') {
                        const newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            const link = $(elem).find(arr['url']).text();
                            newLinks.push(link);

                        });
                        return {
                            newLinks: newLinks.map(link => {
                                return { link, source, arr, sourceUrl };
                            })
                        };

                    } else {
                        const scrapedNews = [];
                        $(arr['*']).each((i, elem) => {
                            let title = $(elem).find(arr['title']).text();
                            title = title.trim();

                            const slug = title
                                .replace(/[^a-zA-Z ]/g, '')
                                .toLowerCase()
                                .split(' ').join('_');
                            const body = $(elem).find(arr['body']).text();

                            const bodyHtml = cheerio.load(body);
                            const desc = bodyHtml.text().replace(/<[^>]*>?/gm, '');

                            const description = desc.substring(0, 200);

                            const link = $(elem).find(arr['url']).text();
                            if (!recentLinks.includes(link)) {
                                let media = $(elem).find(arr['media']).attr('url');
                                if (!media) {
                                    media = $(elem).find(arr['media']).attr('src');
                                    if (!media) {
                                        const newHtm = cheerio.load(body);
                                        media = newHtm('img').attr('src');

                                    }

                                }
                                if (media) {
                                    if (media.substring(0, 5) === 'http:') {
                                        media = media.replace('http:', 'https:');
                                    }
                                }

                                scrapedNews.push({
                                    title,
                                    slug,
                                    body: body.replace(/[^\x20-\x7E]+/g, ''),
                                    description,
                                    media,
                                    url:link,
                                    category_id: sourceUrl.category_id,
                                    source_id: source.id,
                                    date: new Date(),
                                    desc,
                                    source: source.source,
                                    lang: source.lang,
                                    source_slug: source.image,
                                    country: source.country.name
                                });
                            }
                        });
                        return { scrapedNews };
                    }


                } else {
                    const dataPaths = sourceUrl.source_data_paths;
                    const arr = dataPaths.reduce((acc, dataPath)=> {
                        const { data_name, data_path, data_filter } = dataPath;
                        acc[data_name] = data_path;
                        acc[`${data_name}Filter`] = data_filter;
                        return acc;
                    }, []);

                    if (arr['baseUrl']) {
                        const newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            let link = $(elem).attr('href');
                            if (link && !link.startsWith('http')) {
                                link = arr['baseUrl'] + link;
                                newLinks.push(link);
                            }

                        });

                        return {
                            newLinks: newLinks.map(link => {
                                return { link, source, arr, sourceUrl };
                            })
                        };


                    } else {
                        const newLinks = [];
                        $(arr['*']).each((i, elem) => {
                            const link = $(elem).attr('href');
                            const title = $(elem).text().trim();
                            const slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase().trim().split(' ').join('_');
                            newLinks.push({
                                link,
                                title,
                                slug
                            });

                        });
                        return {
                            newLinks: newLinks.map(link => {
                                return { link, source, arr, sourceUrl };
                            })
                        };
                    }
                }

            } catch (err) {
                return null;
            }
        }))).filter(Boolean);

        const scrapedNews = await Promise.all(scrapedNewsWithLinks.map(async (scrapedNewsWithLink)=> {
            const { newLinks, scrapedNews } = scrapedNewsWithLink;
            if (scrapedNews) {
                return scrapedNews;
            }
            const scrapedNewsFromLinks = await scrapeNewsFromLinks(newLinks);
            return scrapedNewsFromLinks.flatMap(scrapedNewsFromLink=>scrapedNewsFromLink).filter(Boolean);
        }));

        await Promise.all(scrapedNews.map(async (scrapedNews)=> {

            if (scrapedNews.length) {
                try {
                    const news = (await saveNewsInDB(scrapedNews))
                        .map(news=>news.toJSON())
                        .filter(news=>news.id);
                    if (news.length) {
                        await updateSources(news);
                        await saveNewsInES(news);
                    }
                } catch (err) {
                    console.log(err);
                }
            }
        }));
    }));

};

const scrapeNewsFromLinks = async (links) => {
    return await Promise.all(links.map(async ({ link, source, arr, sourceUrl }) => {
        const scrapedNews = [];
        try {
            const { data:html } = await axios({
                method: 'get',
                url: link.link,
                timeout // only wait for 2s
            });
            const $Again = await cheerio.load(html);
            let title = $Again(arr['title']).attr('content');
            let slug;
            if (title) {
                title = title.trim().replace(/[^a-zA-Z ]/g, '').toLowerCase();
                slug = title.trim().split(' ').join('_');
            } else {
                title = $Again(arr['title']).text();
                title = title.trim().replace(/[^a-zA-Z ]/g, '').toLowerCase();
                slug = title.trim().split(' ').join('_');
            }


            let media = $Again(arr['media']).attr('src');
            if (!media) {
                media = $Again(arr['media']).attr('content');
            }
            if (media && media.startsWith('http:')) {
                media = media.replace('http:', 'https:');
            }

            let body = $Again(arr['body']).html();

            if (body !== null) {
                const newCheerio = cheerio.load(body, { decodeEntities: false });
                newCheerio('script').remove();
                newCheerio('style').remove();
                newCheerio('svg').remove();
                newCheerio('button').remove();
                newCheerio('video').remove();

                body = newCheerio('body').html();

                const bodyHtml = cheerio.load(body);
                const desc = bodyHtml.text().replace(/<[^>]*>?/gm, '');

                const description = desc.substring(0, 200);

                scrapedNews.push({
                    title,
                    slug,
                    body: body.replace(/[^\x20-\x7E]+/g, ''),
                    description,
                    media,
                    url: link.link,
                    category_id: sourceUrl.category_id,
                    source_id: source.id,
                    date: new Date(),
                    desc,
                    source: source.source,
                    lang: source.lang,
                    source_slug: source.image,
                    country: source.country.name
                });
            }

        } catch (error) {
            // console.log(error);
        }


        return scrapedNews;
    }));
};

const saveNewsInDB = async (news) => {
    return News.bulkCreate(news, { returning: true, ignoreDuplicates: true });
};

const updateSources = async (news) => {
    return Promise.all(news.map(async (news) => {
        return Source.update({
            updated_at: Sequelize.literal('CURRENT_TIMESTAMP')
        }, {
            where: {
                id: news.source_id
            }
        });
    }));
};

const saveNewsInES = async (news) => {
    if (news.length) {
        const result = await client.bulk({
            body: news.flatMap(news=> getNews(news))
        });
        console.log(result);
    }
};

const getNews = (news)=>{
    const { id, source_id:sourceId, category_id:categoryId, country, title, body, lang, pub_date: pubDate } = news;
    return [
        { index:  { _index: 'news', _id: id } },
        // the document to index
        {
            id,
            sourceId,
            categoryId,
            country,
            title: title.replace(/<[^>]*>/g, '').replace(/\s+/g, ' ').trim(),
            lang,
            body: body.replace(/<[^>]*>/g, '').replace(/\s+/g, ' ').replace(/\uFFFD/g, '').trim().substr(0, 8766),
            pubDate: new Date(pubDate).getTime()
        }];
};

module.exports = cron.schedule('0 */20 * * * *', async() => {
    (async()=>{

        let lastId = 0;
        let sources = [];
        do {
            try {

                sources = await Source.findAll({
                    where: {
                        id: {
                            [Op.gt]: lastId
                        }
                    },
                    include: [
                        { model: Country },
                        { model: SourceUrl, required: true, include: [{ model: SourceDataPath }] }],
                    limit:5
                });

                if (sources.length) {
                    lastId = sources[sources.length - 1].id;
                    await scrap(sources.map(source=> source.toJSON()));

                }
            } catch (err) {
                console.log(err);
            }
        } while (sources.length);

        process.exit(0);
    })();


});

