const cron = require('node-cron');
const axios = require('axios');
const query = require('./query');
const cheerio = require('cheerio');


const Promises = require('bluebird');
const mysql = require('mysql');

module.exports = cron.schedule('*/40 * * * *', async() => {

    const id = await query('SELECT id FROM source ');
    for (const iterator of id) {
        const source_id = iterator.id;
        let news = [];
        try {

            const rows = await query(`SELECT * FROM \`source_url\` WHERE source_id = ${source_id}`);
            const getLinks = await query(`SELECT url FROM news WHERE source_id = ${source_id} ORDER BY id DESC LIMIT 100`);
            const checkLinks = [];
            for (const iterator of getLinks) {

                checkLinks.push(iterator.url);
            }
            for (let index = 0; index < rows.length; index++) {
                const response = await axios.get(rows[index].url);
                const { category_id } = rows[index];
                if (response.status === 200) {
                    const html = response.data;
                    const $ = cheerio.load(html, {
                        xmlMode: true
                    });

                    if (rows[index].encoding === 'xml') {
                        const dataPath = await query(`SELECT * FROM \`source_data_path\` WHERE sourceUrl_id = ${rows[index].id}`);
                        const arr = [];

                        for (let i = 0; i < dataPath.length; i++) {
                            const key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[`${key}Filter`] = dataPath[i].data_filter;

                        }
                        if (dataPath[1].data_param === 'html') {
                            const newLinks = [];
                            $(arr['*']).each((i, elem) => {
                                const link = $(elem).find(arr['url']).text();
                                newLinks.push(link);

                            });
                            for (let n = 0; n < newLinks.length; n++) {
                                const obj = {};
                                const linkChecked = checkLinks.includes(newLinks[n]);
                                console.log(newLinks[n]);
                                if (!linkChecked) {
                                    if (n < 11) {
                                        const responseAgain = await axios.get(newLinks[n]);
                                        if (responseAgain.status === 200) {
                                            const htmlAgain = responseAgain.data;
                                            const $Again = cheerio.load(htmlAgain);
                                            let title = $Again(arr['title']).attr('content');
                                            title = title.trim();
                                            let slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                            slug = slug.trim();
                                            slug = slug.split(' ').join('_');

                                            let media = $Again(arr['media']).attr('src');
                                            if (!media) {
                                                media = $Again(arr['media']).attr('content');
                                            }
                                            if (media) {
                                                if (media.substring(0, 5) === 'http:') {
                                                    media = media.replace('http:', 'https:');
                                                }
                                            }
                                            let body = $Again(arr['body']).html();

                                            if (body !== null) {
                                                const newCheerio = cheerio.load(body, { decodeEntities: false });
                                                const bodyFil = arr['bodyFilter'].split('|');

                                                for (const filter of bodyFil) {
                                                    newCheerio(filter).remove();

                                                }
                                                newCheerio('script').remove();
                                                newCheerio('style').remove();
                                                newCheerio('svg').remove();
                                                newCheerio('button').remove();
                                                newCheerio('video').remove();

                                                body = newCheerio('body').html();

                                                const bodyHtml = cheerio.load(body);
                                                let desc = bodyHtml.text();
                                                desc = desc.replace(/<[^>]*>?/gm, '');

                                                const description = desc.substring(0, 200);

                                                const pub_date = new Date();
                                                let date = mysql.escape(pub_date);
                                                date = date.substring(1, 20);

                                                desc = mysql.escape(desc);
                                                obj.title = mysql.escape(title);
                                                obj.slug = mysql.escape(slug);
                                                obj.body = mysql.escape(body);
                                                obj.description = mysql.escape(description);
                                                obj.media = mysql.escape(media);
                                                obj.link = mysql.escape(newLinks[n]);
                                                obj.category_id = mysql.escape(category_id);
                                                obj.source_id = mysql.escape(source_id);
                                                obj.date = mysql.escape(date);
                                                obj.desc = mysql.escape(desc);
                                                news.push(obj);
                                            }

                                        }
                                    }
                                }


                            }


                        } else {
                            $(arr['*']).each(async(i, elem) => {
                                const obj = {};
                                let title = $(elem).find(arr['title']).text();
                                title = title.trim();

                                let slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                slug = slug.split(' ').join('_');
                                const body = $(elem).find(arr['body']).text();

                                // let body = $(elem).find('content\\:encoded').text();

                                const bodyHtml = cheerio.load(body);
                                let desc = bodyHtml.text();
                                desc = desc.replace(/<[^>]*>?/gm, '');

                                const description = desc.substring(0, 200);

                                const link = $(elem).find(arr['url']).text();
                                const linkChecked = checkLinks.includes(link);
                                if (!linkChecked) {
                                    let media = $(elem).find(arr['media']).attr('url');
                                    if (!media) {
                                        media = $(elem).find(arr['media']).attr('src');
                                        if (!media) {
                                            const newHtm = cheerio.load(body);
                                            media = newHtm('img').attr('src');

                                        }

                                    }
                                    if (media) {
                                        if (media.substring(0, 5) === 'http:') {
                                            media = media.replace('http:', 'https:');
                                        }
                                    }

                                    const pub_date = new Date();
                                    let date = mysql.escape(pub_date);
                                    date = date.substring(1, 20);

                                    desc = mysql.escape(desc);
                                    obj.title = mysql.escape(title);
                                    obj.slug = mysql.escape(slug);
                                    obj.body = mysql.escape(body);
                                    obj.description = mysql.escape(description);
                                    obj.media = mysql.escape(media);
                                    obj.link = mysql.escape(link);
                                    obj.category_id = mysql.escape(category_id);
                                    obj.source_id = mysql.escape(source_id);
                                    obj.date = mysql.escape(date);
                                    obj.desc = mysql.escape(desc);


                                    news.push(obj);
                                }
                            });
                        }


                    } else {

                        const dataPath = await query(`SELECT * FROM \`source_data_path\` WHERE sourceUrl_id = ${rows[index].id}`);
                        const arr = [];

                        for (let i = 0; i < dataPath.length; i++) {
                            const key = dataPath[i].data_name;
                            arr[key] = dataPath[i].data_path;
                            arr[`${key}Filter`] = dataPath[i].data_filter;

                        }
                        if (arr['baseUrl']) {
                            const links = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {
                                // let newObj= {};
                                    let link = $(elem).attr('href');
                                    if (link) {
                                        if (link.substring(0, 4) !== 'http')  {
                                            link = arr['baseUrl'] + link;
                                            links.push(link);
                                        }

                                    }

                                    // let title = $(elem).text();
                                    // title=title.trim();
                                    // let slug = title.replace(/[^a-zA-Z ]/g, "").toLowerCase();
                                    //     slug = slug.trim();
                                    //     slug = slug.split(' ').join('_');

                                    // // newObj.link=link;
                                    // newObj.title=title
                                    // newObj.slug=slug;

                                    // exLinks.push(newObj)

                                }).then(async function() {

                                    for (let indexs = 0; indexs < links.length; indexs++) {
                                        const obj = {};
                                        const linkChecked = checkLinks.includes(links[indexs]);
                                        if (!linkChecked) {
                                            const responseAgain = await axios.get(links[indexs]);
                                            if (responseAgain.status === 200) {
                                                const htmlAgain = responseAgain.data;
                                                const $Again = cheerio.load(htmlAgain);
                                                let title;
                                                let slug;

                                                if (!title) {
                                                    title = $Again(arr['title']).attr('content');
                                                    title = title.trim();
                                                    slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                                    slug = slug.split(' ').join('_');
                                                }

                                                let media = $Again(arr['media']).attr('src');

                                                if (!media) {
                                                    media = $Again(arr['media']).attr('content');
                                                }
                                                if (media) {
                                                    if (media.substring(0, 5) === 'http:') {
                                                        media = media.replace('http:', 'https:');
                                                    }
                                                }
                                                let body = $Again(arr['body']).html();
                                                if (body !== null) {

                                                    const newCheerio = cheerio.load(body, { decodeEntities: false });
                                                    const bodyFil = arr['bodyFilter'].split('|');
                                                    for (const filter of bodyFil) {
                                                        newCheerio(filter).remove();

                                                    }
                                                    newCheerio('script').remove();
                                                    newCheerio('style').remove();
                                                    newCheerio('svg').remove();
                                                    newCheerio('button').remove();
                                                    newCheerio('video').remove();


                                                    body = newCheerio('body').html();
                                                    const bodyHtml = cheerio.load(body);
                                                    let desc = bodyHtml.text();
                                                    desc = desc.replace(/<[^>]*>?/gm, '');

                                                    const description = desc.substring(0, 200);

                                                    const pub_date = new Date();
                                                    let date = mysql.escape(pub_date);
                                                    date = date.substring(1, 20);

                                                    desc = mysql.escape(desc);
                                                    obj.title = mysql.escape(title);
                                                    obj.slug = mysql.escape(slug);
                                                    obj.body = mysql.escape(body);
                                                    obj.description = mysql.escape(description);
                                                    obj.media = mysql.escape(media);
                                                    obj.link = mysql.escape(links[indexs]);
                                                    obj.category_id = mysql.escape(category_id);
                                                    obj.source_id = mysql.escape(source_id);
                                                    obj.date = mysql.escape(date);
                                                    obj.desc = mysql.escape(desc);
                                                    // console.log(body)

                                                    news.push(obj);
                                                }
                                            }
                                        }

                                    }
                                });


                        } else {
                            const links = [];
                            const exLinks = [];
                            await Promises
                            // Use this to iterate serially
                                .each($(arr['*']).get(), async function(elem) {

                                    const newObj = {};
                                    const link = $(elem).attr('href');
                                    links.push(link);
                                    let title = $(elem).text();
                                    title = title.trim();
                                    let slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                    slug = slug.trim();
                                    slug = slug.split(' ').join('_');

                                    newObj.link = link;
                                    newObj.title = title;
                                    newObj.slug = slug;

                                    exLinks.push(newObj);

                                }).then(async function() {
                                    for (const myData of exLinks) {
                                    // console.log(myData)
                                        const obj = {};
                                        const linkChecked = checkLinks.includes(myData.link);
                                        if (!linkChecked) {
                                            const responseAgain = await axios.get(myData.link);
                                            if (responseAgain.status === 200) {
                                                const htmlAgain = responseAgain.data;
                                                const $Again = cheerio.load(htmlAgain);
                                                let title;
                                                let slug;
                                                if (myData.title.length > 0 && myData.title !== undefined) {
                                                    title = myData.title;
                                                    slug = myData.slug;
                                                } else {
                                                    title = $Again(arr['title']).attr('content');
                                                    title = title.trim();
                                                    slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                                    slug = slug.split(' ').join('_');
                                                }

                                                let media = $Again(arr['media']).attr('src');

                                                if (!media) {
                                                    media = $Again(arr['media']).attr('content');
                                                }
                                                if (media) {
                                                    if (media.substring(0, 5) === 'http:') {
                                                        media = media.replace('http:', 'https:');
                                                    }
                                                }
                                                if (source_id === 84) {
                                                    title = $Again(arr['title']).attr('content');
                                                    title = title.trim();
                                                    slug = title.replace(/[^a-zA-Z ]/g, '').toLowerCase();
                                                    slug = slug.split(' ').join('_');
                                                }
                                                let body = $Again(arr['body']).html();
                                                if (body !== null) {

                                                    const newCheerio = cheerio.load(body, { decodeEntities: false });
                                                    const bodyFil = arr['bodyFilter'].split('|');
                                                    for (const filter of bodyFil) {
                                                        newCheerio(filter).remove();

                                                    }
                                                    newCheerio('script').remove();
                                                    newCheerio('style').remove();

                                                    newCheerio('svg').remove();
                                                    newCheerio('button').remove();
                                                    newCheerio('video').remove();


                                                    body = newCheerio('body').html();
                                                    const bodyHtml = cheerio.load(body);
                                                    let desc = bodyHtml.text();
                                                    desc = desc.replace(/<[^>]*>?/gm, '');

                                                    const description = desc.substring(0, 200);

                                                    const pub_date = new Date();
                                                    let date = mysql.escape(pub_date);
                                                    date = date.substring(1, 20);

                                                    desc = mysql.escape(desc);
                                                    obj.title = mysql.escape(title);
                                                    obj.slug = mysql.escape(slug);
                                                    obj.body = mysql.escape(body);
                                                    obj.description = mysql.escape(description);
                                                    obj.media = mysql.escape(media);
                                                    obj.link = mysql.escape(myData.link);
                                                    obj.category_id = mysql.escape(category_id);
                                                    obj.source_id = mysql.escape(source_id);
                                                    obj.date = mysql.escape(date);
                                                    obj.desc = mysql.escape(desc);
                                                    news.push(obj);
                                                }
                                            }
                                        }

                                    }

                                });


                        }

                    }

                }
            }
        } catch (err) {
            console.log(err);
        }
        news = [...new Set(news)];
        for (let index = 0; index < news.length; index++) {
            const element = news[index];
            // let sentidata;
            // let deepData;
            // try {
            //     let senti = await axios.get('http://192.64.113.8:5000/newshunt_senti?text=' + encodeURIComponent(element.desc));
            //     sentidata = senti;
            //     let newSenti = senti.data;
            //     if (newSenti != undefined && newSenti != null) {
            //         element.senti = mysql.escape(newSenti.sentences);
            //         element.sentiStatus = mysql.escape(newSenti.status);
            //         element.sentiNeg = mysql.escape(newSenti.Neg);
            //         element.sentiNeu = mysql.escape(newSenti.Neu);
            //         element.sentiPos = mysql.escape(newSenti.Pos);
            //     }
            // } catch (err) {


            // }
            // if (!sentidata) {
            //     element.senti = null;
            //     element.sentiStatus = null;
            //     element.sentiNeg = null;
            //     element.sentiNeu = null;
            //     element.sentiPos = null;
            // }
            // let sumaryData;

            // try {
            //     let sumary = await axios.get('http://192.64.113.8:5000/newshunt_summary?text=' + encodeURIComponent(element.desc));
            //     sumaryData =sumary.data;
            //     let summary = sumary.data;
            //     if (summary != undefined && summary != null) {
            //         element.summary = mysql.escape(summary);

            //     }


            // } catch (err) {
            //     // element.summary = null;

            //     console.log(err);
            // }
            // if (!sumaryData) {
            //     element.summary = null;

            // }
            // try {
            //     let deepAnalysis = await axios.get('http://192.64.113.8:5000/newshunt_deep?text=' + encodeURIComponent(element.desc));
            //     deepData=deepAnalysis;
            //     let deep = deepAnalysis.data;
            //     if (deep != undefined && deep != null) {

            //         element.deepSentences = mysql.escape(deep.sentences);
            //         element.deepStatus = mysql.escape(deep.status);
            //         element.deepInsult = mysql.escape(deep.insult);
            //         element.deepToxicity = mysql.escape(deep.toxicity);
            //         element.deepProfanity = mysql.escape(deep.profanity);
            //         element.deepexual = mysql.escape(deep.sexual);
            //         element.deeoIdentity = mysql.escape(deep.identity);
            //         element.deepFlirt = mysql.escape(deep.flirt);
            //         element.deepThreat = mysql.escape(deep.threat);
            //         element.DeepSevere = mysql.escape(deep.severe);


            //     }
            // } catch (err) {


            // }
            // if(!deepData){
            //     element.deepSentences = '';
            //     element.deepStatus = '';
            //     element.deepInsult = '';
            //     element.deepToxicity = '';
            //     element.deepProfanity = '';
            //     element.deepexual = '';
            //     element.deeoIdentity = '';
            //     element.deepFlirt = '';
            //     element.deepThreat = '';
            //     element.DeepSevere = '';
            // }

            // try {
            //     const rows = await query('INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date,sentiment,sentiment_state,senti_neg,senti_neu,senti_pos,summary) VALUES (' + element.title + ',' + element.slug + ', ' + element.body + ', ' + element.description + ', ' + element.link + ', ' + element.media + ', ' + element.category_id + ', ' + element.source_id + ', ' + element.date + ', ' + element.senti + ', ' + element.sentiStatus + ', ' + element.sentiNeg + ', ' + element.sentiNeu + ', ' + element.sentiPos + ', ' + element.summary + ')');

            // } catch (err) {
            //     console.log(err)
            // }
            try {
                await query(`INSERT INTO news (title, slug, body, description,url,media,category_id,source_id,pub_date) VALUES (${element.title},${element.slug}, ${element.body}, ${element.description}, ${element.link}, ${element.media}, ${element.category_id}, ${element.source_id}, ${element.date})`);

            } catch (err) {
                console.log(err);
            }

        }

    }


});
