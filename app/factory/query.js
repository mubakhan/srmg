const mysql = require('mysql');
const util = require('util');

const connection = mysql.createConnection({
    host: process.env.DB_HOST,
    port: process.env.DB_PORT,
    user: process.env.DB_USER,
    password: process.env.DB_PASSWORD,
    database:process.env.DB_NAME
});
const query = util.promisify(connection.query).bind(connection);
module.exports = query;
