let cron = require('node-cron'),
    query = require('./query');


module.exports = cron.schedule("*/3 * * * *", async() => {
    try {
        const keywords = await query(`SELECT * FROM keywords`);
        let newsWithkeyWord = [];
        const rows = await query(`SELECT * FROM news WHERE keyStatus = 0 ORDER BY id DESC LIMIT 500`);
        for (const news of rows) {
            for (const keyword of keywords) {
                let title = news.title.toLowerCase(); 
                let body = news.body.toLowerCase();
                let newsId = news.id;
                let keywordID = keyword.id;
                if (title.includes(keyword.word) || body.includes(keyword.word)) {
                    let insertIN = await query(`INSERT INTO keyword_news(news_id, keyword_id) VALUES (${newsId},${keywordID})`)

                }
            }

        }

        const updateNews = await query(`UPDATE news SET keyStatus=1 WHERE keyStatus = 0 ORDER BY id DESC LIMIT 500`)


    } catch (err) {
        console.log(err);
    }

});