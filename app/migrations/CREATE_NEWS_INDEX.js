const client = require('../factory/elasticsearch');

const up = async ()=>{
    try {
        const result = await client.indices.create({
            index: 'news',
            body: {
                settings: {
                    analysis: {
                        normalizer: {
                            custom_normalizer: {
                                type: 'custom',
                                filter: ['lowercase']
                            }
                        }
                    },
                    number_of_shards: 1,
                    number_of_replicas: 0
                },
                mappings: {
                    properties: {
                        id: { type: 'long' },
                        sourceId: { type: 'long' },
                        categoryId: { type: 'long' },
                        country: { type: 'keyword' },
                        lang: { type: 'keyword' },
                        title: { type: 'text',
                            fields: {
                                keyword:{
                                    type: 'keyword',
                                    ignore_above: 8000,
                                    normalizer: 'custom_normalizer'
                                }
                            }
                        },
                        body: {
                            type: 'text',
                            fields: {
                                keyword:{
                                    type: 'keyword',
                                    ignore_above: 8000,
                                    normalizer: 'custom_normalizer'
                                }
                            }
                        },
                        pubDate: { type: 'date' }
                    }
                }
            }
        });
        console.log(result.body);
    } catch (err) {
        console.log(err);
        console.log(err.body.error.reason);
    }
};

module.exports = {
    up
};
