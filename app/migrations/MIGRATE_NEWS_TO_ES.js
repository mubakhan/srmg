const client = require('../factory/elasticsearch');

const News  = require('../models/news');
const { Op } = require('sequelize');
const up = async ()=>{
    let lastId = 0;
    let news = [];
    do {
        try {

            news = await News.findAll({
                where: {
                    id: {
                        [Op.gt]: lastId
                    }
                },
                limit:1000,
                raw:true
            });
            if (news.length) {
                lastId = news[news.length - 1].id;
                console.log(lastId);
                const result = await client.bulk({
                    body: news.flatMap(news=> getNews(news))
                });
                if (result.body.errors) {

                    console.log(result.body.items.map(item=>item.index.error).filter(Boolean));
                    console.log(result.body.items.filter(item=>item.index.error).map(item=>item));
                }
            }
        } catch (err) {
            console.log(err.meta.body);
        }
    } while (news.length);

};


const getNews = (news)=>{
    const { id, source_id:sourceId, category_id:categoryId, country, title, body, lang, pub_date: pubDate } = news;
    return [
        { index:  { _index: 'news', _id: id } },
        // the document to index
        {
            id,
            sourceId,
            categoryId,
            country,
            title: title.replace(/<[^>]*>/g, '').replace(/\s+/g, ' ').trim(),
            lang,
            body: body.replace(/<[^>]*>/g, '').replace(/\s+/g, ' ').replace(/\uFFFD/g, '').trim().substr(0, 8766),
            pubDate: new Date(pubDate).getTime()
        }];
};

module.exports = {
    up
};
